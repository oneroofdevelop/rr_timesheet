import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatToolbarModule } from '@angular/material/toolbar';

import { FuseSearchBarModule, FuseShortcutsModule } from '@fuse/components';
import { FuseSharedModule } from '@fuse/shared.module';

import { ToolbarComponent } from 'app/layout/components/toolbar/toolbar.component';
import { MatBadgeModule } from '@angular/material/badge';
import { MatChipsModule } from '@angular/material/chips';
import { MatRippleModule } from '@angular/material/core';
import { DxPopoverModule, DxTemplateModule } from 'devextreme-angular';
import { MatDividerModule } from '@angular/material/divider';
import { UtilShareModule } from '@utilies/util-share.module';
import { RrIserviceModule } from '@rr-iservice/rr-iservice.module';

@NgModule({
    declarations: [
        ToolbarComponent
    ],
    imports: [
        // MatBadgeModule,
        RouterModule,
        // MatButtonModule,
        // MatIconModule,
        // MatMenuModule,
        // MatToolbarModule,
        // MatChipsModule,
        // MatRippleModule,
        // MatDividerModule,
        RrIserviceModule,
        UtilShareModule,


        FuseSharedModule,
        FuseSearchBarModule,
        FuseShortcutsModule
    ],
    exports: [
        ToolbarComponent
    ]
})
export class ToolbarModule {
}
