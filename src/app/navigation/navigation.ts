import { FuseNavigation } from '@fuse/types';

export const navigation: FuseNavigation[] = [
    {
        id: 'timesheet',
        title: 'Timesheet',
        type: 'item',
        icon: 'timer',
        url: '/pages/timesheet',
    },
    {
        id: 'jobs',
        title: 'Jobs',
        type: 'item',
        icon: 'assignment',
        url: '/pages/jobs',
    },
    // {
    //     id: 'report',
    //     title: 'Task',
    //     type: 'item',
    //     icon: 'assignment_ind',
    //     url: '/pages/report',
    // },
    // {
    //     id: 'report',
    //     title: 'Weekly',
    //     type: 'item',
    //     icon: 'assignment_ind',
    //     url: '/pages/report/weekly',
    // },
    {
        id: '111',
        title: 'report',
        type: 'collapsable',
        icon: 'show_chart',
        children: [
            {
                id: 'task',
                title: 'Task',
                type: 'item',
                icon: 'table_chart',
                url: '/pages/report/task',
            },
            {
                id: 'weekly',
                title: 'Weekly',
                type: 'item',
                icon: 'table_chart',
                url: '/pages/report/weekly',
            },
            {
                id: 'waitData',
                title: 'Wait Data',
                type: 'item',
                icon: 'table_chart',
                url: '/pages/report/report',
            }
        ]
    },
    {
        id: 'notification',
        title: 'Notifications',
        type: 'item',
        icon: 'notifications',
        url: '/pages/noti',
    },
];
