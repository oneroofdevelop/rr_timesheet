import { Routes, Route } from '@angular/router';
import { LoginComponent } from './pages/login/login.component';
import { AppAuthGuardService } from '@rr-iservice/service/app/app-auth-guard.service';


export const appRoutes: Routes = [
    {
        path: 'pages',
        loadChildren: () => import('./pages/pages.module').then(m => m.PagesModule),
        canActivateChild: [AppAuthGuardService]
    },
    {
        path: 'login',
        component: LoginComponent,
    },
    {
        path: '**',
        redirectTo: 'pages/timesheet'
    } 
];
