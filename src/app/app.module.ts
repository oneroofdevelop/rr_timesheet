import { NgModule, LOCALE_ID, NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RouterModule, Routes } from '@angular/router';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { TranslateModule } from '@ngx-translate/core';
import 'hammerjs';

import { FuseModule } from '@fuse/fuse.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseProgressBarModule, FuseSidebarModule, FuseThemeOptionsModule } from '@fuse/components';

import { fuseConfig } from 'app/fuse-config';

import { AppComponent } from 'app/app.component';
import { LayoutModule } from 'app/layout/layout.module';
import { appRoutes } from './app.routing';
import { LoginModule } from './pages/login/login.module';
import { DecimalPipe, DatePipe } from '@angular/common';
import { NgxLoadingModule } from 'ngx-loading';
import { ApiTokenAuthInterceptor } from '@utilies/interceptor/api-token-auth.interceptor';
import { UtilShareModule } from '@utilies/util-share.module';
import localeTh from '@angular/common/locales/th.js';
import { registerLocaleData } from '@angular/common';
import { DateTimePipe } from '@utilies/pipe/date-time.pipe';
import { MatNativeDateModule } from '@angular/material/core';
import { CoreModule } from './@core/core.module';
import { FormsModule } from '@angular/forms';

registerLocaleData(localeTh);
// const appRoutes: Routes = [
//     {
//         path      : '**',
//         redirectTo: '/pages/dashboard'
//     }
// ];

@NgModule({
    declarations: [
        AppComponent,

    ],
    imports: [
        BrowserModule,
        FormsModule,
        BrowserAnimationsModule,
        HttpClientModule,
        RouterModule.forRoot(appRoutes,
            {
                useHash: true
            }),

        TranslateModule.forRoot(),

        // Material moment date module
        MatMomentDateModule,
        MatNativeDateModule,

        // Material
        MatButtonModule,
        MatIconModule,

        // Fuse modules
        FuseModule.forRoot(fuseConfig),
        FuseProgressBarModule,
        FuseSharedModule,
        FuseSidebarModule,
        FuseThemeOptionsModule,

        // App modules
        LayoutModule,
        LoginModule,

        CoreModule,
    ],
    providers: [

        DecimalPipe,
        DatePipe,
        // config
        {
            provide: HTTP_INTERCEPTORS,
            useClass: ApiTokenAuthInterceptor,
            multi: true,
        },
        // {
        //     provide: LOCALE_ID,
        //     useValue: 'us-US'
        // }

    ],
    exports: [],
    bootstrap: [
        AppComponent
    ],
    schemas: [NO_ERRORS_SCHEMA, CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule {
}
