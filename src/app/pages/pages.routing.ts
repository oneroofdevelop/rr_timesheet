import { Routes, RouterModule, Route } from '@angular/router';
const routes: Routes = [
    {
        path: 'timesheet',
        loadChildren: () => import('./timesheet/timesheet.module').then(m => m.TimesheetModule)
    }, {
        path: 'jobs',
        loadChildren: () => import('./jobs/jobs.module').then(m => m.JobsModule)
    },
    {
        path: 'report',
        loadChildren: () => import('./report/report.module').then(m => m.ReportModule)
    },
    {
        path: 'noti',
        loadChildren: () => import('./noti/noti.module').then(m => m.NotiModule)
    }
];
export const PagesRoutes = RouterModule.forChild(routes);
