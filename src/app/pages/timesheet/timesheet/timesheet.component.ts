import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import * as moment from 'moment';
import { TaskFromModalComponent } from '@rr-iservice/components/task/task-from-modal/task-from-modal.component';
import { AppTimerService } from '@rr-iservice/service/app/app-timer.service';
import { List } from 'linqts';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppClientService } from '@rr-iservice/service/app/app-client.service';
import { AppProjectService } from '@rr-iservice/service/app/app-project.service';
import { AppEmployeeService } from '@rr-iservice/service/app/app-employee.service';
import { AlertService } from '@utilies/service/alert.service';
import { AppUserService } from '@rr-iservice/service/app/app-user.service';

@Component({
  selector: 'app-timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: ['./timesheet.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class TimesheetComponent implements OnInit {
  searchForm: FormGroup;
  clientList = [];
  projectList = [];
  employeeList = [];
  constructor(
    private dialog: MatDialog,
    private appTimerService: AppTimerService,
    private appClientService: AppClientService,
    private appProjectService: AppProjectService,
    private appEmployeeService: AppEmployeeService,
    private formBuilder: FormBuilder,
    private appUserService: AppUserService,
    private alertService: AlertService
  ) {
    this.loadData();
    this.setSubscribe();
    this.setSearchFormValidate();

  }

  ngOnInit(): void {
    // this.appTimerService.refreshRecords().then(res => {
    // this.setSearchPanel();
    // });

    this.appUserService.userProfile.subscribe(res => {
      this.searchForm.get('empNoList').setValue([res?.empNo]);
      this.onSearch();
    });

  }

  setSearchFormValidate(): void {
    const start = moment().isoWeekday(1);
    const end = moment().isoWeekday(7);

    this.searchForm = this.formBuilder.group({
      compCode: [],
      projectCode: [],
      empNoList: [],
      startDate: [start, Validators.required],
      endDate: [end, Validators.required],
    });

    this.searchForm.get('empNoList').setValue([]);

    this.setSearchFormEvent();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(TaskFromModalComponent, {
      width: '800px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(() => {

    });
  }

  onSearch(): void {
    if (this.searchForm.invalid) {
      this.alertService.danger({ title: 'Alert', message: 'กรุณาไส่ข้อมูลที่ต้องการค้นหา' });
      return;
    }
    const formData = this.searchForm.value;
    const requsetData = {
      compCodeList: formData.compCode === null ? [] : [formData.compCode],
      projectCodeList: formData.projectCode === null ? [] : [formData.projectCode],
      empNoList: formData.empNoList,
      startDate: formData.startDate,
      endDate: formData.endDate,
    };
    localStorage.setItem('rrTimesheetReq', JSON.stringify(requsetData));

    this.appTimerService.getRecords(requsetData);
  }

  loadData(): void {
    this.appClientService.getClientList({});
    this.appEmployeeService.getEmployeeList({});
  }
  setSubscribe(): void {
    this.appProjectService.projectList.subscribe(res => {
      this.projectList = res;
    });

    this.appClientService.clientList.subscribe(res => {
      this.clientList = res;
    });

    this.appEmployeeService.employeeList.subscribe(res => {
      this.employeeList = res;
    });
  }

  setSearchFormEvent(): void {
    this.searchForm.get('compCode').valueChanges.subscribe(res => {
      this.appProjectService.getProjectList({
        compCodeList: [res]
      });
    });

  }
  timeManualTrigger(): void {
    this.onSearch();
  }
}
