import { Routes, RouterModule } from '@angular/router';
import { TimesheetComponent } from './timesheet/timesheet.component';
export const timesheetComps = [
  TimesheetComponent
];
const routes: Routes = [
  {
    path: 'main',
    component: TimesheetComponent,
  },
  {
    path: '**',
    redirectTo: 'main'
  }
];
export const TimesheetRoutes = RouterModule.forChild(routes);
