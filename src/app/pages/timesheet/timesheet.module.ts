import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TimesheetRoutes, timesheetComps } from './timesheet.routing';
import { UtilShareModule } from '@utilies/util-share.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule, FuseWidgetModule } from '@fuse/components';
import { DxDataGridModule, DxTemplateModule, DxBulletModule } from 'devextreme-angular';
import { RrIserviceModule } from '@rr-iservice/rr-iservice.module';

@NgModule({
  imports: [
    CommonModule,
    TimesheetRoutes,

    UtilShareModule,
    RrIserviceModule,

    FuseSharedModule,
    FuseSidebarModule,
    FuseWidgetModule,
  ],
  declarations: [timesheetComps],
  providers: [],
  entryComponents: []
})
export class TimesheetModule { }
