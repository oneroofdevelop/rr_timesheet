import { Routes, RouterModule } from '@angular/router';
import { NotiComponent } from './noti.component';
export const notiComps = [
  NotiComponent
];
const routes: Routes = [
  {
    path: 'main',
    component: NotiComponent,
  },
  {
    path: '**',
    redirectTo: 'main'
  }
];

export const NotiRoutes = RouterModule.forChild(routes);
