import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotiComponent } from './noti.component';
import { notiComps, NotiRoutes } from './noti.routing';
import { UtilShareModule } from '@utilies/util-share.module';
import { RrIserviceModule } from '@rr-iservice/rr-iservice.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule, FuseWidgetModule } from '@fuse/components';

@NgModule({
  imports: [
    CommonModule,
    NotiRoutes, 
    UtilShareModule,
    RrIserviceModule,

    FuseSharedModule,
    FuseSidebarModule,
    FuseWidgetModule,
  ],
  declarations: [notiComps]
})
export class NotiModule { }
