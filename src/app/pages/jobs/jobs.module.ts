import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { jobsComps, JobsRoutes } from './jobs.routing';
import { UtilShareModule } from '@utilies/util-share.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule, FuseWidgetModule } from '@fuse/components';
import { DxDataGridModule, DxTemplateModule, DxBulletModule } from 'devextreme-angular';
import { JobRespository } from 'app/@core/respositorys/job.respository';
import { JobService } from 'app/@core/services/job.service';
import { RrIserviceModule } from '@rr-iservice/rr-iservice.module';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    JobsRoutes,
    FormsModule,

    UtilShareModule,
    RrIserviceModule,

    FuseSharedModule,
    FuseSidebarModule,
    FuseWidgetModule,


    DxDataGridModule,
    DxTemplateModule,
    DxBulletModule
  ],
  declarations: [jobsComps],
  providers: [

  ],

})
export class JobsModule { }
