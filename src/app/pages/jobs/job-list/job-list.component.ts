import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { AppJobsService } from '@rr-iservice/service/app/app-jobs.service';
import { List } from 'linqts';
import { JobListService } from './job-list.service';
import { AppClientService } from '@rr-iservice/service/app/app-client.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppProjectService } from '@rr-iservice/service/app/app-project.service';
import { AppEmployeeService } from '@rr-iservice/service/app/app-employee.service';
import { AppMasterService } from '@rr-iservice/service/app/master/api-master.service';
import { AppMasterConfigService } from '@rr-iservice/service/app/master/app-master-config.service';
import { AppUserService } from '@rr-iservice/service/app/app-user.service';
import { AlertService } from '@utilies/service/alert.service';

@Component({
  selector: 'app-job-list',
  templateUrl: './job-list.component.html',
  styleUrls: ['./job-list.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class JobListComponent implements OnInit {
  searchForm: FormGroup;
  projectList = [];
  clientList = [];
  jobStatusList = [];
  specialDepartmentList = [];
  userProfile: any;
  pushYear: any;
  selectYearList = [];
  today = new Date();
  year: number = this.today.getFullYear();
  constructor(
    private appJobsService: AppJobsService,
    public dialog: MatDialog,
    private jobListService: JobListService,
    private appClientService: AppClientService,
    private formBuilder: FormBuilder,
    private appProjectService: AppProjectService,
    private appMasterService: AppMasterService,
    private appUserService: AppUserService,
    private appMasterConfigService: AppMasterConfigService,
    private alertService: AlertService

  ) {
    this.loadData();
    this.setSubscribe();
    this.setFormValidate();
    this.appUserService.userProfile.subscribe(res => {
      this.userProfile = res;
    });
  }

  ngOnInit(): void {
    this.getSelectYear();
  }
  onSearch(): void {
    const formData = this.searchForm.getRawValue();
    if (this.searchForm.invalid) {
      this.alertService.danger({ title: 'Job', message: 'ตรวจสอบข้อมูลให้ครบถ้วน' });
      return;
    }    
    const req = {
      compCodeList: !formData.compCode || formData.compCode === null ? [] : [formData.compCode],
      projectCodeList: !formData.projectCode || formData.projectCode === null ? [] : [formData.projectCode],
      jobNo: formData.jobNo,
      jobStatusCodeList: formData.jobStatusCode || [],
      briefDateFrom: formData.briefStartDate,
      briefDateTo: formData.briefEndDate,
      jobYear : formData.jobYear.toString()
    };
    this.appJobsService.getJobsList(req);
  }

  setFormValidate(): void {
    this.searchForm = this.formBuilder.group({
      compCode: [],
      projectCode: [],
      jobNo: [''],
      jobStatusCode: ['', Validators.required],
      briefStartDate: [''],
      briefEndDate: [''],
      jobYear: ['']
    });

    this.setFormEvent();
    this.searchForm.get('compCode').setValue(this.jobListService.compCode);
    this.searchForm.get('projectCode').setValue(this.jobListService.projectCode);
    this.searchForm.get('jobNo').setValue(this.jobListService.jobNo);
    if (this.jobListService.jobStatusCode) {
      this.searchForm.get('jobStatusCode').setValue([this.jobListService.jobStatusCode]);
    } else {
      this.searchForm.get('jobStatusCode').setValue(['OP', 'IP', 'AP']);
    }
    this.searchForm.get('jobYear').setValue(this.year);
    this.onSearch();
  }
  resetForm(): void {
    this.searchForm = this.formBuilder.group({
      compCode: [],
      projectCode: [],
      jobNo: [''],
      jobStatusCode: [['OP', 'IP', 'AP']],
      briefStartDate: [''],
      briefEndDate: [''],
      jobYear: this.year
    });
    // this.searchForm.get('jobNo').setValue(this.jobListService.jobNo);
    this.setFormEvent();
  }
  loadData(): void {
    this.appClientService.getClientList({});
    this.appMasterConfigService.getJobStatusList().then(res => {
      this.jobStatusList = res;
    });
    this.appMasterConfigService.getSpecialDepartmentList().then(res => {
      this.specialDepartmentList = res;
    });

  }
  setSubscribe(): void {
    this.appProjectService.projectList.subscribe(res => {
      this.projectList = res;
    });

    this.appClientService.clientList.subscribe(res => {
      this.clientList = res;
    });

  }
  setFormEvent(): void {
    this.searchForm.get('compCode').valueChanges.subscribe(res => {
      if (res) {
        this.appProjectService.getProjectList({
          compCodeList: [res]
        });
      }
    });

  }

  getSDPermission(): boolean {
    if (this.userProfile?.positionList.length > 0 && this.specialDepartmentList.length > 0) {
      const positionList = new List<any>(this.userProfile?.positionList).ToList();
      const specialDepartmentList = new List<any>(this.specialDepartmentList).Select(a => a.specialDeptCode).ToList();
      return positionList.Where(a => specialDepartmentList.Contains(a.deptCode)).Any();
    } else {
      return false;
    }

  }

  getSelectYear(): void {
    this.pushYear = this.year;
    do {
      this.selectYearList.push(this.pushYear);
      this.pushYear = this.pushYear - 1;
    } while (this.pushYear >= 2016);
  }
}
