import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AppJobsService } from '@rr-iservice/service/app/app-jobs.service';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class JobListService implements Resolve<any> {
  jobNo: string;
  compCode: string;
  projectCode: string;
  jobStatusCode: string;
  constructor(
    private appJobsService: AppJobsService,

  ) {

  }
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    this.jobNo = route.params.jobNo;
    this.compCode = route.params.compCode;
    this.projectCode = route.params.projectCode;
    this.jobStatusCode = route.params.jobStatusCode;
    const req = {
      compCodeList: [route.params.compCode],
      projectCodeList: [route.params.projectCode],
      jobNo: route.params.jobNo,
      jobStatusCodeList: [route.params.jobStatusCode],
      briefDateFrom: '',
      briefDateTo: '',
    };

    return new Promise((resolve, reject) => {
      Promise.all([
        this.appJobsService.getJobsList(req)
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }
}
