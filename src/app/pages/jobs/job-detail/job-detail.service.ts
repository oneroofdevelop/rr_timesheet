import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Observable, BehaviorSubject } from 'rxjs';
import { AppDepartmentService } from '@rr-iservice/service/app/master/app-department.service';
import { AppJobsService } from '@rr-iservice/service/app/app-jobs.service';

@Injectable({
  providedIn: 'root'
})
export class JobDetailService implements Resolve<any> {
  taskFromList: BehaviorSubject<any[]>;
  constructor(
    private appJobsService: AppJobsService
  ) {
    this.taskFromList = new BehaviorSubject(new Array<any>());
  }
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<any> | Promise<any> | any {
    if (route.params.jobNo === 0) {
      return;
    }
    return new Promise((resolve, reject) => {
      Promise.all([
        this.appJobsService.getJobDetail(route.params.jobNo)
      ]).then(
        () => {
          resolve();
        },
        reject
      );
    });
  }
}
