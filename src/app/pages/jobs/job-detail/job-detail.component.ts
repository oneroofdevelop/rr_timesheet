import { element } from 'protractor';
import { Component, OnInit, ViewEncapsulation, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { JobsTaskModalComponent } from '@rr-iservice/components/jobs/jobs-task-modal/jobs-task-modal.component';
import { AppJobsService } from '@rr-iservice/service/app/app-jobs.service';
import { AppClientService } from '@rr-iservice/service/app/app-client.service';
import { AppMasterService } from '@rr-iservice/service/app/master/api-master.service';
import { JobDetailService } from './job-detail.service';
import { AlertService } from '@utilies/service/alert.service';
import * as _ from 'underscore';
import { AppProjectService } from '@rr-iservice/service/app/app-project.service';
import { List } from 'linqts';
import { Observable, Observer, BehaviorSubject } from 'rxjs';
import { Router } from '@angular/router';
import * as moment from 'moment';
import { AppUserService } from '@rr-iservice/service/app/app-user.service';
import { FormHelperService } from '@utilies/service/form-helper.service';
import { environment } from 'environments/environment';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { HubService } from '@rr-iservice/service/app/hub.service';
import { AppMasterConfigService } from '@rr-iservice/service/app/master/app-master-config.service';
import { log } from 'util';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DialogService } from '@utilies/service/dialog.service';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AppEmployeeService } from '@rr-iservice/service/app/app-employee.service';

export interface PeriodicElement {
  oldFileName: string;
  fileType: string;
  filePath: string;
  key: number;
  isActive: string;
}

const ELEMENT_DATA: PeriodicElement[] = [];

@Component({
  selector: 'app-job-detail',
  templateUrl: './job-detail.component.html',
  styleUrls: ['./job-detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class JobDetailComponent implements OnInit {
  dataSource = ELEMENT_DATA;
  uploadSucced: boolean;
  uploadUrl: any;
  key = 1;
  jobForm: FormGroup;
  clientForm: FormGroup;
  projectForm: FormGroup;
  issueBy;
  userProfile: any;
  userProfileEmpNo: any;
  files: File;
  previewLoaded: boolean = false;
  private env = environment;
  jobStatusCode;
  choosefile;
  clientList = [];
  projectList = [];
  serviceList = [];
  subServiceList = [];
  contactPersonSourceList = [];
  contactPersonList = [];
  jobsTaskList = [];
  jobStatusList: any[] = [];
  selectYearList = [];
  jobDetail: any;
  customerList: any;
  projecSourceList: any;
  filteredContactPerson: Observable<any[]>;
  dueDate: any;
  specialDept: any[];
  clientCare: BehaviorSubject<any[]>;
  jobNo: any;
  constructor(
    public dialog: MatDialog,
    private alertService: AlertService,
    private appJobsService: AppJobsService,
    private appClientService: AppClientService,
    private appMasterService: AppMasterService,
    private appMasterConfigService: AppMasterConfigService,
    private jobDetailService: JobDetailService,
    private appProjectService: AppProjectService,
    private appUserService: AppUserService,
    private route: Router,
    private formHelperService: FormHelperService,
    private hubService: HubService,
    private httpClient: HttpClient,
    private dialogService: DialogService,
    private appEmployeeService: AppEmployeeService,
  ) {
    this.hubService.start();
    this.setFormValidate();
    this.clientCare = new BehaviorSubject([]);
    this.appClientService.getContactList({ compCodeList: [] });
    this.appMasterService.getServiceList({});
    this.appMasterConfigService.getSpecialDepartmentList();
    this.appMasterService.getSubServiceList({});
    this.appClientService.getClientList({});
    this.appEmployeeService.getEmployeeAllList({});
    this.appProjectService.getProjectList({
      compCodeList: []
    }).then(res => {
      this.projecSourceList = res;
      this.projectList = res;
    });

    this.appMasterConfigService.specialDept.subscribe(res => {
      this.specialDept = res;
      this.isShowButtonSave();
      this.onFormDisable();
    });
    this.clientCare.subscribe(res => {
      this.isShowButtonSave();
      this.onFormDisable();
    });
  }
  fetchClientCareList(): any {
    if (!this.userProfile) {
      return;
    }
    const { positionList } = this.userProfile;
    const payload = {
      deptCodeList: _.pluck(positionList, 'deptCode'),
    };
    this.appClientService.getClientCareList(payload).then(res => {
      this.clientCare.next(res);
    });

  }
  isClientStaff(): boolean {
    if (!this.userProfile) {
      return false;
    }
    const { empNo } = this.userProfile;
    const dataClientCare = _.findWhere(this.clientCare.value, { empNo: empNo });
    if (!dataClientCare) {
      return false;
    }
    const { companyList } = dataClientCare;
    return _.contains(_.pluck(companyList, 'compCode'), this.jobDetail?.compCode);
  }
  isShowButtonSave(): boolean {
    if (this.userProfile?.empNo === this.issueBy && this.jobDetail) {
      return this.issueBy !== undefined;
    }
    return this.checkSpecialDept() || this.isClientStaff();
  }
  checkSpecialDept(): boolean {
    const data = _.filter(this.specialDept, item => {
      return _.contains(_.pluck(this.userProfile?.positionList, 'deptCode'), item.specialDeptCode);
    });
    if (data.length > 0) {
      return true;
    }
    return false;
  }

  isShowBudget(): boolean {
    if (this.userProfile?.empNo === this.issueBy) {
      return true;
    }
    return _.contains(_.pluck(this.userProfile?.positionList, 'positionCode'), 'DH') || this.checkSpecialDept();
  }

  isDisableInput(): boolean {
    if (this.userProfile?.empNo === this.issueBy) {
      return true;
    }
    return this.checkSpecialDept();
  }

  ngOnInit(): void {

    this.appMasterConfigService.getJobStatusList().then(res => {
      this.jobStatusList = res;
    });
    this.appClientService.clientList.subscribe(res => {
      this.customerList = res;
    });
    this.appJobsService.jobDetail.subscribe(res => {
      this.jobDetail = res;
      if (this.jobDetail) {
        this.setFormData();
        this.jobDetailService.taskFromList.next(this.jobDetail.trJobTask);
      } else {
        this.jobDetailService.taskFromList.next([]);
      }
    });
    this.setFormData();
    this.fetchClientCareList();
    this.appClientService.contactList.subscribe(res => {
      this.contactPersonSourceList = res;
      this.contactPersonList = res.map(item => {
        item.fullName = item.preNameName + item.firstName + ' ' + item.lastName;
        return item;
      });
    });
    this.appMasterService.serviceList.subscribe(res => {
      this.serviceList = res;
    });
    this.appMasterService.subServiceList.subscribe(res => {
      this.subServiceList = res;
    });

    this.appMasterService.statusJobList.subscribe(res => {
      this.jobStatusList = res;
    });
    this.jobDetailService.taskFromList.subscribe(res => {
      this.jobsTaskList = res;
    });
    this.appUserService.userProfile.subscribe(res => {
      this.userProfile = res;
      this.fetchClientCareList();
      this.isShowButtonSave();
      this.onFormDisable();
      this.userProfileEmpNo = this.userProfile?.empNo;
      if (this.jobDetail === null) {
        this.issueBy = res?.empNo;
        if (this.userProfile) {
          this.jobForm.get('issueBy').setValue(res?.name);
        }
        return;
      }
    });


    this.jobForm.get('srvcCode').valueChanges.subscribe(res => {
      const req = {
        srvcCode: res
      };
      this.appMasterService.getSubServiceList(req).then();
    });
    this.jobForm.get('compCode').valueChanges.subscribe(res => {
      this.filterByCustomer();
      this.appClientService.getContactList({ compCodeList: [res] });
    });
    const jobFormData = this.jobForm.getRawValue();
  }

  private _filter(value: string): string[] {
    const filterValue = value.toLowerCase();
    return this.contactPersonList.filter(option => option.fullName.toLowerCase().includes(filterValue) && this.jobForm.get('compCode').value === option.compCode);
  }
  filterContactByCompCode(event): void {
    this.contactPersonList = this.contactPersonSourceList.filter(option => this.jobForm.get('compCode').value === option.compCode);
    this.filteredContactPerson = new Observable((observer: Observer<any>) => {
      observer.next(this.contactPersonList);
      observer.complete();
    });
  }
  trackClientForm(event: any): void {
    this.clientForm = event;
  }
  trackProjectForm(event: any): void {
    this.projectForm = event;

  }
  setFormValidate(): void {
    const start = moment().isoWeekday(1);
    const end = moment().isoWeekday(7);
    this.jobForm = new FormGroup({
      compCode: new FormControl('', Validators.required),
      projectCode: new FormControl('', Validators.required),
      jobNo: new FormControl({ value: '', disabled: true }),
      personCode: new FormControl('', Validators.required),
      srvcCode: new FormControl('', Validators.required),
      subSrvcCode: new FormControl('', Validators.required),
      jobName: new FormControl('', [Validators.required, Validators.maxLength(100)]),
      jobDesc: new FormControl(''),
      jobStatusCode: new FormControl({ value: '' }, Validators.required),
      workBudget: new FormControl(''),
      briefDate: new FormControl(start, Validators.required),
      dueDate: new FormControl(end, Validators.required),
      issueBy: new FormControl({ value: '', disabled: true }, Validators.required),
      issueDate: new FormControl({ value: '', disabled: true }, Validators.required)
    });
    this.jobForm.controls['jobStatusCode'].disable();
    this.jobForm.get('jobStatusCode').setValue('OP');
    this.jobForm.get('briefDate').setValue(moment());
    this.jobForm.get('dueDate').setValue(moment());
    this.jobForm.get('issueDate').setValue(moment());

    this.appUserService.userProfile.subscribe(res => {

      if (this.jobDetail === null) {
        this.issueBy = this.userProfile?.empNo;
        if (this.userProfile) {
          this.jobForm.get('issueBy').setValue(this.userProfile?.name);
        }
        return;
      }
    });
    this.setFormEvent();
  }

  setFormData(): void {
    if (this.jobDetail === null) {
      this.issueBy = this.userProfile?.empNo;
      if (this.userProfile) {
        this.jobForm.get('issueBy').setValue(this.userProfile?.name);
      }
      return;
    }

    this.jobForm.get('compCode').setValue(this.jobDetail.compCode);
    this.jobForm.get('projectCode').setValue(this.jobDetail.projectCode);
    this.jobForm.get('jobNo').setValue(this.jobDetail.jobNo);
    this.jobForm.get('personCode').setValue(this.jobDetail.personCode);
    this.jobForm.get('srvcCode').setValue(this.jobDetail.srvcCode);
    this.jobForm.get('subSrvcCode').setValue(this.jobDetail.subSrvcCode);
    this.jobForm.get('jobName').setValue(this.jobDetail.jobName);
    this.jobForm.get('jobDesc').setValue(this.jobDetail.jobDesc);
    this.jobForm.get('jobStatusCode').setValue(this.jobDetail.jobStatusCode);
    this.jobStatusCode = this.jobDetail.jobStatusCode;
    this.jobForm.get('workBudget').setValue(this.jobDetail.workBudget);
    this.jobForm.get('briefDate').setValue(this.jobDetail.briefDate);
    this.jobForm.get('dueDate').setValue(this.jobDetail.dueDate);
    this.issueBy = this.jobDetail.issueBy;
    if (this.jobDetail.jobNo !== '') {
      this.jobForm.get('issueBy').setValue(this.jobDetail.issueByName);
    }
    this.jobForm.get('issueDate').setValue(this.jobDetail.issueDate);
    this.jobForm.controls['compCode'].disable();
    this.jobForm.controls['projectCode'].disable();
    this.jobForm.controls['srvcCode'].disable();
    this.jobForm.controls['subSrvcCode'].disable();

    this.filterByCustomer();
    if (this.jobDetail.jobStatusCode === 'CL') {
      this.formHelperService.disableForm(this.jobForm);
    }
    if (this.jobDetail.jobNo !== '') {
      this.dataSource = [];
      if (this.jobDetail.jobAttactList && this.jobDetail.jobAttactList.length > 0) {
        this.dataSource = this.pushItem(this.jobDetail.jobAttactList);
        this.previewLoaded = false;
        this.uploadSucced = true;
      } else {
        this.previewLoaded = false;
        this.uploadSucced = false;
      }

    }





  }
  onFormDisable(): void {
    if (!this.isDisableInput() && !this.isShowButtonSave()) {
      this.jobForm.controls['jobStatusCode'].disable();
      this.jobForm.get('briefDate').disable();
      this.jobForm.get('dueDate').disable();
      this.jobForm.get('jobName').disable();
      this.jobForm.get('jobDesc').disable();
      this.jobForm.get('personCode').disable();
      this.jobForm.get('workBudget').disable();
    } else {
      this.jobForm.controls['jobStatusCode'].enable();
      this.jobForm.get('briefDate').enable();
      this.jobForm.get('dueDate').enable();
      this.jobForm.get('jobName').enable();
      this.jobForm.get('jobDesc').enable();
      this.jobForm.get('workBudget').enable();
      this.jobForm.get('personCode').enable();
    }
  }

  onSave(e): void {
    e.preventDefault();
    if (this.jobForm.invalid) {
      this.alertService.info({ title: 'Job', message: 'ตรวจสอบข้อมูลให้ครบถ้วนก่อนการบันทึก' });
      return;
    }

    const jobFormData = this.jobForm.getRawValue();
    const data =
    {
      compCode: jobFormData.compCode,
      projectCode: jobFormData.projectCode,
      jobNo: jobFormData.jobNo || '',
      jobName: jobFormData.jobName,
      personCode: jobFormData.personCode,
      srvcCode: jobFormData.srvcCode,
      subSrvcCode: jobFormData.subSrvcCode,
      jobDesc: jobFormData.jobDesc,
      jobStatusCode: jobFormData.jobStatusCode,
      workBudget: jobFormData.workBudget || 0,
      briefDate: jobFormData.briefDate,
      dueDate: jobFormData.dueDate,
      issueBy: this.userProfile?.empNo,
      issueDate: jobFormData.issueDate,
      isActive: 'Y',
      jobTaskList: this.jobsTaskList,
      jobAttactList: this.dataSource
    };
    if (jobFormData.jobNo !== '') {

      data.issueBy = this.jobDetail.issueBy;
    }
    this.appJobsService.saveJob(data).then(res1 => {
      if (res1 !== 'error') {
        this.hubService.hubConnection.invoke('sendNotify').catch(err => console.error(err.toString()));
        this.alertService.info({ title: 'Job', message: 'บันทึกข้อมูลเรียบร้อย' }).then(res2 => {
          this.route.navigate([`/pages/jobs/list/${res1.compCode}/${res1.projectCode}/${res1.jobNo}/${res1.jobStatusCode}`]);
        });
      }
    });
  }
  openJobTaskModelEdit(): void {

  }
  openJobTaskModal(): void {
    const dialogRef = this.dialog.open(JobsTaskModalComponent, {
      width: '800px',
      data: {
        task: null,
        dueDate: this.jobForm.get('dueDate').value,
        briefDate: this.jobForm.get('briefDate').value,
        actionType: 'add',
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result && !this.jobDetail) {
        result.taskJobId = 0;
        this.jobsTaskList = this.jobsTaskList.concat(result);
        this.jobDetailService.taskFromList.next(this.jobsTaskList);
      }
    });
  }
  filterByCustomer(): void {
    this.projectList = new List<any>(this.projecSourceList).Where(a => a.compCode === this.jobForm.get('compCode').value).ToArray();
  }
  setFormEvent(): void {
    this.jobForm.get('srvcCode').valueChanges.subscribe(res => {
      const req = {
        srvcCode: res
      };
      this.appMasterService.getSubServiceList(req).then();
    });
    this.jobForm.get('compCode').valueChanges.subscribe(res => {
      if (res) {
        this.appClientService.getContactList({
          compCodeList: [res]
        });
      }

    });

    this.jobForm.get('dueDate').valueChanges.subscribe(res => {
      if (res) {
        const jobFormData = this.jobForm.getRawValue();
        this.dueDate = jobFormData.dueDate;
      }
    });

  }

  afterSaveContact(event): void {
    if (event) {
      this.appClientService.getContactList({ compCodeList: [this.jobForm.get('compCode').value] });
    }
  }

  incomingfile(event): void {

    this.files = event.target.files[0];

    if (typeof (this.files) === 'undefined') {
      this.previewLoaded = false;
    } else {
      this.previewLoaded = true;
      this.choosefile = this.files.name;
    }

  }
  delete(key): void {

    this.dialogService.confirm({
      dialogTitile: 'ยืนยันการลบไฟล์',
      btnConfirmText: '',
      btnCancelText: ''
    }).then(res => {
      if (res) {
        let resultActive = this.dataSource.filter((dataSource) => {
          return dataSource.key !== key
        });
        // key==key
        const itemFile = _.findWhere(this.dataSource, { key: key });
        resultActive = resultActive.concat({
          ...itemFile,
          isActive: 'N'
        });
        this.dataSource = [];
        this.dataSource = resultActive;
        if (!this.dataSource && this.dataSource.length < 0) {
          this.uploadSucced = false;
        }
        this.alertService.info({ title: 'ลบข้อมูล', message: 'ลบข้อมูลเรียบร้อย' });
      }

    });


  }

  upload(): void {
    const formData = new FormData();
    formData.append('image', this.files, this.files.name);
    const url = this.env.iService.apiUrl + '/uploadFile';
    this.httpClient.post(url, formData).subscribe(res1 => {
      this.uploadSucced = true;
      this.uploadUrl = res1;
      const { items } = this.uploadUrl;

      this.dataSource = this.add(items);

      if (res1 !== 'error') {
        this.hubService.hubConnection.invoke('sendNotify').catch(err => console.error(err.toString()));
        // this.alertService.info({ title: 'Upload', message: 'บันทึกข้อมูลเรียบร้อย' })
      }
      this.previewLoaded = false;

    });


  }

  pushItem(items: any[]): any[] {
    const { jobNo } = this.jobDetail;
    if (!jobNo) {
      return [];
    }

    // item เช็คค่า undefine ถ้ามีค่าถึวจะทำ

    const data = items && (items as any[]).map((value, index) => {
      return {
        seq: value.seq,
        jobNo: jobNo,
        fileName: value.fileName,
        oldFileName: value.oldFileName,
        fileType: value.fileType,
        filePath: value.filePath,
        isActive: value.isActive,
        key: index,
        jobAttachId: value.jobAttachId
      };
    });
    this.jobNo = jobNo;
    return this.dataSource.concat(data);
  }
  add(items: any[]): any[] {
    if (!this.jobDetail) {
      this.jobNo = null;
    }
    const data = items && (items as any[]).map((value, index) => {
      return {
        seq: this.dataSource.length + 1,
        jobNo: this.jobNo,
        fileName: value.fileName,
        oldFileName: value.oldFileName,
        fileType: value.fileType,
        filePath: value.filePath,
        isActive: 'Y',
        key: this.dataSource.length,
        jobAttachId: 0
      };
    });

    return this.dataSource.concat(data);
  }
  location(link): void {
    window.open(link);
  }

}
