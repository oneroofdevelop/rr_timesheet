import { Routes, RouterModule } from '@angular/router';
import { JobListComponent } from './job-list/job-list.component';
import { JobDetailComponent } from './job-detail/job-detail.component';
import { JobDetailService } from './job-detail/job-detail.service';
import { JobListService } from './job-list/job-list.service';
export const jobsComps = [
  JobListComponent,
  JobDetailComponent
];
const routes: Routes = [
  {
    path: 'list',
    component: JobListComponent,
  }, {
    path: 'list/:compCode/:projectCode/:jobNo/:jobStatusCode',
    component: JobListComponent,
    resolve: {
      data: JobListService
    }
  }, {
    path: 'detail/:jobNo',
    component: JobDetailComponent,
    resolve: {
      data: JobDetailService
    }
  }, {
    path: '**',
    redirectTo: 'list'
  }
];
export const JobsRoutes = RouterModule.forChild(routes);
