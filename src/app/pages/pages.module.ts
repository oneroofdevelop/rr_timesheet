import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesRoutes } from './pages.routing';

@NgModule({
  imports: [
    CommonModule,
    PagesRoutes
  ],
  exports: [],

  declarations: [],
  providers: []
})
export class PagesModule { }
