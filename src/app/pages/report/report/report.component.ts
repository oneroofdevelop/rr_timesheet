import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import * as moment from 'moment';
import { TaskFromModalComponent } from '@rr-iservice/components/task/task-from-modal/task-from-modal.component';
import { AppTimerService } from '@rr-iservice/service/app/app-timer.service';
import { List } from 'linqts';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AppClientService } from '@rr-iservice/service/app/app-client.service';
import { AppProjectService } from '@rr-iservice/service/app/app-project.service';
import { AppEmployeeService } from '@rr-iservice/service/app/app-employee.service';
import { AlertService } from '@utilies/service/alert.service';
import { AppJobsService } from '@rr-iservice/service/app/app-jobs.service';
import { AppMasterConfigService } from '@rr-iservice/service/app/master/app-master-config.service';
import { JobListService } from 'app/pages/jobs/job-list/job-list.service';
import { AppMasterService } from '@rr-iservice/service/app/master/api-master.service';
import { log } from 'util';
import { CamelCaseToDashPipe } from '@fuse/pipes/camelCaseToDash.pipe';

export interface PeriodicElement {
  client: string;
  project: string;
  year: string;
  jobno: string;
  task: string;
  status: string;
  start: string;
  end: string;
  estmated: string;
  useemp: string;

}


const ELEMENT_DATA: PeriodicElement[] = [
  { client: 'Hydrogen', project: 'Hydrogen', year: 'phichakorn', jobno: 'H', task: 'H', status: 'H', start: '11.38', end: '11.38', estmated: "2.2", useemp: "2" },
  { client: 'Helium', project: 'Helium', year: 'pornpicha', jobno: 'H', task: 'He', status: 'H', start: '11.38', end: '11.38', estmated: "2.2", useemp: "2" },
  { client: 'Lithium', project: 'Lithium', year: 'udorn', jobno: 'H', task: 'Li', status: 'H', start: '11.38', end: '11.38', estmated: "2.2", useemp: "2" },
  { client: 'Beryllium', project: 'Beryllium', year: 'planstart', jobno: 'H', task: 'Be', status: 'H', start: '11.38', end: '11.38', estmated: "2.2", useemp: "2" },
  { client: 'Boron', project: 'Boron', year: 'patchara', jobno: 'H', task: 'B', status: 'H', start: '11.38', end: '11.38', estmated: "2.2", useemp: "2" },

];
@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.css'],
  encapsulation: ViewEncapsulation.None,

})
export class ReportComponent implements OnInit {

  searchForm: FormGroup;
  projectList = [];
  clientList = [];
  jobStatusList = [];
  taskStatusList = [];
  employeeList = [];
  jobNoList = [];
  selectYearList = [];
  today = new Date();
  year: number = this.today.getFullYear();
  pushYear: any;

  dataSource = [];

  constructor(
    private appJobsService: AppJobsService,
    public dialog: MatDialog,
    private jobListService: JobListService,
    private appClientService: AppClientService,
    private formBuilder: FormBuilder,
    private appProjectService: AppProjectService,
    private appMasterService: AppMasterService,
    private appMasterConfigService: AppMasterConfigService,
    private appEmployeeService: AppEmployeeService,
    private alertService: AlertService


  ) {
    this.setFormValidate();
    this.loadData();
    this.setSubscribe();



  }
  ngOnInit(): void {
    this.pushYear = this.year;

    do {
      this.selectYearList.push(this.pushYear);
      this.pushYear = this.pushYear - 1;
    } while (this.pushYear >= 2016)
  }
  loadData(): void {
    this.appClientService.getClientList({});
    this.appMasterConfigService.getJobStatusList().then(res => {
      this.jobStatusList = res;
    });
  }

  resetForm(): void {
    this.searchForm = this.formBuilder.group({
      compCode: [''],
      projectCode: [''],
      jobNo: [''],
      jobStatusCode: [['OP', 'IP', 'AP']],
      year: [this.year]

    });
    this.setFormEvent();
  }

  setFormValidate(): void {
    this.searchForm = this.formBuilder.group({
      compCode: [''],
      projectCode: [''],
      year: ['', Validators.required],
      jobNo: [''],
      jobStatusCode: ['', Validators.required],
    });

    this.setFormEvent();
    this.searchForm.get('jobStatusCode').setValue(['OP', 'IP', 'AP']);
    this.searchForm.get('year').setValue(this.year);

  }

  setFormEvent(): void {
    this.searchForm.get('compCode').valueChanges.subscribe(res => {
      if (res) {
        this.appProjectService.getProjectList({
          compCodeList: [res]
        });
      }
      this.searchForm.get('projectCode').setValue('');
    });

    this.searchForm.get('projectCode').valueChanges.subscribe(res => {
      if (res) {
        const formData = this.searchForm.getRawValue();

        const req = {
          compCodeList: [formData.compCode],
          projectCodeList: formData.projectCode === undefined ? [] : [formData.projectCode],
          jobNo: formData.jobNo,
          jobStatusCodeList: formData.jobStatusCode || [],

        };
        this.appJobsService.getJobsList(req);
        this.appJobsService.jobsList.subscribe(res => this.jobNoList = res);
      }
    });



  }

  onSearch(): void {
    if (this.searchForm.invalid) {
      this.alertService.danger({ title: 'Alert', message: 'Complete from and try again.' });
      return;
    }
    const formData = this.searchForm.getRawValue();
    const req = {
      compCode: formData.compCode,
      projectCode: formData.projectCode,
      jobNo: formData.jobNo,
      jobStatusCodeList: formData.jobStatusCode,
      year: formData.year.toString(),


    };

    this.appJobsService.getSummaryTaskList(req);

    this.appJobsService.summaryTaskList.subscribe(res => {
      // console.log("summaryTaskList",res);
      if (res && res.length > 0 ) {
        this.dataSource = res.map(item => {
          const minutes = item.totalWorkingTime % 60;
          const hours = (item.totalWorkingTime - minutes) / 60;
          const total = hours + ':' + minutes;
          return {
            ...item,
            totalWorkingTime: total
          }
        });
      }
    })
  }

    setSubscribe(): void {
      this.appProjectService.projectList.subscribe(res => {
        this.projectList = res;
      });

      this.appClientService.clientList.subscribe(res => {
        this.clientList = res;
      });

      this.appMasterConfigService.getTaskStatusList().then(res => {
        this.taskStatusList = res;
      });
    }
  }
