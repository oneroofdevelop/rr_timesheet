import { WeeklyComponent } from './weekly/weekly.component';
import { Routes, RouterModule } from '@angular/router';
import { ReportComponent } from './report/report.component';
import {ReportWaitDataComponent } from './report-wait-data/report-wait-data.component';
export const reportComps = [
  ReportComponent,
  WeeklyComponent,
  ReportWaitDataComponent
];
export const routes: Routes = [
  {
    path: 'task',
    component: ReportComponent,
  },
  {
    path: 'weekly',
    component: WeeklyComponent,
  },
  {
    path: 'report',
    component: ReportWaitDataComponent,
  },
];

export const ReportRoutes = RouterModule.forChild(routes);
