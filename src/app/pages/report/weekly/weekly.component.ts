import { filter } from 'rxjs/operators';
import { from } from 'rxjs';
import { DialogConfirmComponent } from './../../../../@utilies/component/dialog/dialog-confirm/dialog-confirm.component';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { WeeklyService } from './weekly.service';
import { exportDataGrid } from 'devextreme/excel_exporter';
import * as ExcelJS from 'exceljs';
import * as FileSaver from 'file-saver';
import * as moment from 'moment';
import { List } from 'linqts';
import { AppDepartmentService } from '@rr-iservice/service/app/master/app-department.service';
import { MatDialog } from '@angular/material/dialog';
import { ReportWorkDetailModalComponent } from '@rr-iservice/components/report/report-work-detail-modal/report-work-detail-modal.component';
import _ from 'underscore'
@Component({
  selector: 'app-weekly',
  templateUrl: './weekly.component.html',
  styleUrls: ['./weekly.component.scss']
})
export class WeeklyComponent implements OnInit {
  searchForm: FormGroup;
  departmentList: any[];
  dataList: any[];
  dateList = [];
  constructor(
    private weeklyService: WeeklyService,
    private formBuilder: FormBuilder,
    private appDepartmentService: AppDepartmentService,
    public dialog: MatDialog,
  ) {

  }

  ngOnInit(): void {
    this.searchFormInit();
    this.appDepartmentService.getDepartmentList().then(res => {
      this.departmentList = res;
    });
  }

  searchFormInit(): void {
    const start = moment().isoWeekday(1);
    const end = moment().isoWeekday(7);

    this.searchForm = this.formBuilder.group({
      startDate: [start],
      endDate: [end],
      deptCodeList: [[]]
    });
    this.generateColumns(this.searchForm.getRawValue());
    this.searchForm.valueChanges.subscribe(res => {
      this.generateColumns(res);
    });
  }

  searchFormSubmit(): void {
    const data = this.searchForm.getRawValue();
    data.deptCodeList = data.deptCodeList || [];
    this.weeklyService.reportWeekly(data).then(res => {
      this.dataList = res;
    });
  }
  generateColumns(res): void {
    this.dataList = [];
    if (!res.startDate || !res.endDate) {
      return;
    }
    if (moment(res.startDate).isAfter(moment(res.endDate))) {
      return;
    }

    this.dateList = [];
    const dateFirst = res.startDate.clone();
    const dateLast = res.endDate.clone();
    this.addDate(dateFirst);
    while (dateFirst.add(1, 'days').diff(dateLast) < 0) {
      this.addDate(dateFirst);
    }
    this.addDate(dateLast);

  }
  addDate(date: any): void {
    const isExist = new List(this.dateList).Where(a => moment(a.value).format('L') === moment(date).format('L')).Any();
    if (!isExist) {
      console.log('isExist', isExist);
      this.dateList.push({
        name: moment(date.clone().toDate()).format('dd-DD'),
        value: date.clone().toDate()
      });
    }
  }

  onExporting(e): void {
    const workbook = new ExcelJS.Workbook();
    const worksheet = workbook.addWorksheet('report');

    exportDataGrid({
      component: e.component,
      worksheet: worksheet,
      autoFilterEnabled: true,
      customizeCell: (options) => {
        const { gridCell, excelCell } = options;
        if (gridCell.rowType === 'data') {
          if (moment(gridCell.column.dataField).isValid()) {
            excelCell.value = this.getDuration(gridCell.data.worktimeList, gridCell);
            excelCell.alignment = { horizontal: 'center' };
          }
        }
        if (gridCell.rowType === 'group') {
          excelCell.fill = { type: 'pattern', pattern: 'solid', fgColor: { argb: 'BEDFE6' } };
        }
      }
    }).then(res => {
      workbook.xlsx.writeBuffer().then((buffer) => {
        const blob = new Blob([buffer], { type: 'application/octet-stream' });
        FileSaver.saveAs(blob, `timesheet_weekly_${moment().format('DD_MM_YYYY_HH_mm')}.xlsx`);
      });
    });
    e.cancel = true;
  }

  convertDuration(summaryTimeList: any): string {
    let house = 0;
    let min = 0;
    const data = summaryTimeList.worktimeList.map(item => {
      const dataItem = item.workTime.toString().split('.');
      if (dataItem[0]){
         // tslint:disable-next-line: radix
         house = house + parseInt(dataItem[0]);
      }
      if (dataItem[1]){
         // tslint:disable-next-line: radix
         min = min + parseInt(dataItem[1]);
      }
    });
    return `${house + parseInt((min / 60).toString(), 10)}.${(min % 60).toString().padStart(2, '0')}`;
  }
  getDuration(summaryTimeList: any[], cell: any): string {
    const data = new List(summaryTimeList).Where(a => moment(a.Date).format('L') === moment(cell.column.dataField).format('L')).FirstOrDefault();
    if (data != null) {
      return data.workTime;
    } else {
      return '0.00';
    }

  }

  openWorkDeatail(cell): void{
   let tempData = [];
   let dataTaskList = [];
   const {data} = cell;
   const {worktimeList} = data;
   if(worktimeList.length > 0 ){
    dataTaskList = _.filter(worktimeList,item=>{ 
      return moment(item.Date).format('L') === moment(cell.column.dataField).format('L')
   });
  }
  if(dataTaskList.length > 0 ){
    tempData = dataTaskList[dataTaskList.length-1].dataTaskList;
  }
// return ;
    const dialogRef = this.dialog.open(ReportWorkDetailModalComponent, {
      width: '70%',
      data: {
        empNo:data.empNo,
        empName:data.empName,
        dataTaskList:dataTaskList[dataTaskList.length-1].dataTaskList
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      // alert(result);
    });
  }
}
