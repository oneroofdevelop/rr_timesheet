import { Injectable } from '@angular/core';
import { ApiReportService } from '@rr-iservice/service/api/api-report.service';


@Injectable({
  providedIn: 'root'
})
export class WeeklyService {

  constructor(private apiReportService: ApiReportService) { }

  reportWeekly(data): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiReportService.weekly(data).subscribe(res => {
        resolve(res);
      });
    });
  }
  reportRawData(data): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiReportService.rawData(data).subscribe(res => {
        resolve(res);
      });
    });
  }  
}
