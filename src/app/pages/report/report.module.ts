import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { reportComps, ReportRoutes } from './report.routing';
import { UtilShareModule } from '@utilies/util-share.module';
import { RrIserviceModule } from '@rr-iservice/rr-iservice.module';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseWidgetModule, FuseSidebarModule } from '@fuse/components';

@NgModule({
  imports: [
    CommonModule,
    ReportRoutes,

    UtilShareModule,
    RrIserviceModule,

    FuseSharedModule,
    FuseSidebarModule,
    FuseWidgetModule,
  ],
  declarations: [reportComps],
  providers: [],
  entryComponents: []})
export class ReportModule { }
