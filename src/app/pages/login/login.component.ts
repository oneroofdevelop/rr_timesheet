import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { fuseAnimations } from '@fuse/animations';
import { FuseConfigService } from '@fuse/services/config.service';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { AppUserService } from '@rr-iservice/service/app/app-user.service';
import { IApiLoginRequest } from '@rr-iservice/interface/api/api-user.interface';
import { MatDialog } from '@angular/material/dialog';
import { ResetPasswordModalComponent } from '@rr-iservice/components/reset-password/reset-password-modal/reset-password-modal.component';

@Component({
  selector: 'login-2',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: fuseAnimations
})
export class LoginComponent implements OnInit {
  env = environment;
  loginForm: FormGroup;
  loginErrorMsg: string;
  loginFail: boolean;
  constructor(
    public dialog: MatDialog,
    private _fuseConfigService: FuseConfigService,
    private formBuilder: FormBuilder,
    private appUserService: AppUserService,
    private router: Router
  ) {
    this.loginFail = false;
    this._fuseConfigService.config = {
      layout: {
        navbar: {
          hidden: true
        },
        toolbar: {
          hidden: true
        },
        footer: {
          hidden: true
        },
        sidepanel: {
          hidden: true
        }
      }
    };
  }

  ngOnInit(): void {
    if (this.env.iService.reCaptchaKey) {
      this.loginForm = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required],
        recaptcha: ['', Validators.required]
      });
    } else {
      this.loginForm = this.formBuilder.group({
        username: ['', Validators.required],
        password: ['', Validators.required],
      });
    }

  }
  resetPassword(): void {
    const dialogRef = this.dialog.open(ResetPasswordModalComponent, {
      width: '800px',
      data: {

      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {

      }
    });
  }
  
  submitLogin(): void {
    this.loginFail = false;
    if (this.loginForm.valid) {
      const data: IApiLoginRequest = {
        userName: this.loginForm.value.username,
        passWord: this.loginForm.value.password,
        userGroupId: 1
      };

      this.appUserService.login(data).then(res => {
        if (res) {
          this.router.navigate(['pages/customer/list']);
        }
      });
    }
  }

}
