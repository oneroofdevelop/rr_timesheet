export abstract class JobRespository {
    abstract getItemAll();
    abstract getItemById();
    abstract addItem(jobItem);
    abstract deleteItem(jobItem);
    abstract editItem(jobItem);
}