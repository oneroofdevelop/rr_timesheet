export abstract class TaskRespository {
    abstract getItemAll();
    abstract getItemById();
    abstract addItem(taskItem);
    abstract deleteItem(taskItem);
    abstract editItem(taskItem);
}