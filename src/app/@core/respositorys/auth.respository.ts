export abstract class AuthRespository {
    abstract login(user, password);
    abstract logout();
    abstract checkLogin();
    abstract getProfileServer(_id: string);
    abstract getProfileLocal();
    abstract saveLoginLocal(params: any);
}