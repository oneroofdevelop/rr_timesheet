export abstract class JobTaskRespository {
    abstract getItemAll();
    abstract getItemById();
    abstract addItem(item);
    abstract deleteItem(item);
    abstract editItem(item);
}