import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { JobService } from 'app/@core/services/job.service';
import { JobRespository } from 'app/@core/respositorys/job.respository';
import { ClientRespository } from 'app/@core/respositorys/client.repository';
import { ClientService } from 'app/@core/services/client.service';
import { ProjectService } from 'app/@core/services/project.service';
import { ProjectRespository } from 'app/@core/respositorys/project.respository';
import { ServiceMasterService } from 'app/@core/services/service.service';
import { ServiceRespository } from 'app/@core/respositorys/service.repositoty';
import { SubServiceRespository } from 'app/@core/respositorys/sub-service.respository';
import { SubServiceMasterService } from 'app/@core/services/sub-service.service';
import { DepartmentRespository } from 'app/@core//respositorys/department.respository';
import { DepartmentService } from 'app/@core/services/department.service';


@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
        {
            provide: JobRespository, useClass: JobService
        },
        ,
        {
            provide: ClientRespository, useClass: ClientService
        },
        {
            provide: ProjectRespository, useClass: ProjectService
        },
        {
            provide: ServiceRespository, useClass: ServiceMasterService
        },
        {
            provide: SubServiceRespository, useClass: SubServiceMasterService
        },
        {
            provide: DepartmentRespository, useClass: DepartmentService
        }
  ]
})
export class CoreModule { }
