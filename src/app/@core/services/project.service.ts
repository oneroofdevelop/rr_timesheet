import { Injectable } from '@angular/core';
import { SharedService } from './shared.service';
import { ProjectRespository } from 'app/@core/respositorys/project.respository';
@Injectable({
    providedIn: 'root'
  })
export class ProjectService extends ProjectRespository {
    constructor(
        private _sharedService: SharedService
    ) {
        super();
    }
    getItemAll(param: any = null) {
        return this._sharedService.httpPost('getProjectList', param);
    }
}