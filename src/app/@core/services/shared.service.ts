import { ClientRespository } from 'app/@core/respositorys/client.repository';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from 'environments/environment';
@Injectable({
    providedIn: 'root'
})
export class SharedService {
    constructor(
        private http: HttpClient
    ) {

    }
    httpGet(actionUrl: string) {
        return this.http.get(`${environment.iService.apiUrl}/${actionUrl}`);
    }
    httpPost(actionUrl: string, param: any) {

        const headerParam = new HttpHeaders({ Authorization: 'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiIxIiwidXNlck5hbWUiOiJhZG1pbiIsInVzZXJHcm91cElkIjoiMSIsInVzZXJUeXBlSWQiOiIxIiwiZW1wTm8iOm51bGwsImVtYWlsIjoiYW5hbkByZWxhdGlvbnNoaXByZXB1YmxpYy5jb20iLCJ1c2VySWRMaXN0IjpbXSwiaXNBY3RpdmUiOiJZIiwidG9rZW5EYXRlVGltZSI6MTU4ODE0MTcxMn0.WwbeVZEv_nLxTbxx_QLmAwQSFPitXhJnQGV1YFbrIss' });
        return this.http.post(`${environment.iService.apiUrl}/${actionUrl}`, param, {
            headers: headerParam
        });
    }
}