import { ClientRespository } from 'app/@core/respositorys/client.repository';
import { Injectable } from '@angular/core';
import { SharedService } from './shared.service';
@Injectable({
    providedIn: 'root'
  })
export class ClientService extends ClientRespository {
    constructor(
        private _sharedService: SharedService
    ) {
        super();
    }
    getItemAll() {
        return this._sharedService.httpPost('getClientList', {});
    }
}