import { ClientRespository } from 'app/@core/respositorys/client.repository';
import { Injectable } from '@angular/core';
import { SharedService } from './shared.service';
import { TaskRespository } from '../respositorys/task.respository';
@Injectable({
    providedIn: 'root'
})
export class TaskService extends TaskRespository {
    constructor(
        private _sharedService: SharedService
    ) {
        super();
    }
    getItemById() {
        throw new Error("Method not implemented.");
    }
    addItem(taskItem: any) {
        throw new Error("Method not implemented.");
    }
    deleteItem(taskItem: any) {
        throw new Error("Method not implemented.");
    }
    editItem(taskItem: any) {
        throw new Error("Method not implemented.");
    }

    getItemAll() {
        return this._sharedService.httpGet('getTaskList');
    }
}