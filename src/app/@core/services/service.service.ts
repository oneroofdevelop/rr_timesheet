import { Injectable } from '@angular/core';
import { SharedService } from './shared.service';
import { ServiceRespository } from '../respositorys/service.repositoty';

@Injectable({
    providedIn: 'root'
  })
export class ServiceMasterService extends ServiceRespository {
    constructor(
        private _sharedService: SharedService
    ) {
        super();
    }
    getItemAll(param: any = null) {
        return this._sharedService.httpPost('getServiceList', param);
    }
}