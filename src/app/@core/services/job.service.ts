import { Injectable } from '@angular/core';
import { SharedService } from './shared.service';
import { JobRespository } from 'app/@core/respositorys/job.respository';

@Injectable({
    providedIn: 'root'
})
export class JobService extends JobRespository {
    jobDetail;
    jobTaskList;
    constructor(
        private _sharedService: SharedService
    ) {
        super();
    }
    getItemAll() {
        return this._sharedService.httpPost(`getJobList`, {

        });
    }
    getItemById() {
        return this._sharedService.httpPost(`getJob`, {});
    }
    addItem(jobItem: any) {
        return this._sharedService.httpPost(`saveJob`, jobItem);
    }
    deleteItem(jobItem: any) {
        return this._sharedService.httpPost(`saveJob`, jobItem);
    }
    editItem(jobItem: any) {
        return this._sharedService.httpPost(`saveJob`, jobItem);
    }


}

export class JobService2 extends JobRespository {
    jobDetail;
    jobTaskList;
    constructor(
        private _sharedService: SharedService
    ) {
        super();
    }
    getItemAll() {
        return this._sharedService.httpPost(`getJobList`, {

        });
    }
    getItemById() {
        return this._sharedService.httpPost(`getJob`, {});
    }
    addItem(jobItem: any) {
        return this._sharedService.httpPost(`saveJob`, jobItem);
    }
    deleteItem(jobItem: any) {
        return this._sharedService.httpPost(`saveJob`, jobItem);
    }
    editItem(jobItem: any) {
        return this._sharedService.httpPost(`saveJob`, jobItem);
    }


}