import { Injectable } from '@angular/core';
import { SharedService } from './shared.service';
import { DepartmentRespository } from '../respositorys/department.respository';
@Injectable({
    providedIn: 'root'
  })
export class DepartmentService extends DepartmentRespository {
    constructor(
        private _sharedService: SharedService
    ) {
        super();
    }
    getItemAll(param: any = null) {
        return this._sharedService.httpPost('getDepartmentList', param);
    }
}