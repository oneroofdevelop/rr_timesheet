import { Injectable } from '@angular/core';
import { SharedService } from './shared.service';
import { SubServiceRespository } from '../respositorys/sub-service.respository';

@Injectable({
    providedIn: 'root'
  })
export class SubServiceMasterService extends SubServiceRespository {
    constructor(
        private _sharedService: SharedService
    ) {
        super();
    }
    getItemAll(param: any = null) {
        return this._sharedService.httpPost('getSubServiceList', param);
    }
}