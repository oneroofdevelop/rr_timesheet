import { IUseCase } from '../usecase.interface';
import { Injectable } from '@angular/core';
import { ProjectRespository } from 'app/@core/respositorys/project.respository';

@Injectable({
    providedIn: 'root'
})
export class GetProjectList implements IUseCase<any, any> {
    response: any;
    constructor(private _projectRespository: ProjectRespository) { }
    excute(params: any) {
        try {
            this.response = this._projectRespository.getItemAll(params);
        } catch (e) {
            console.log(e);
        }
    }
}