import { IUseCase } from '../usecase.interface';
import { Injectable } from '@angular/core';
import { ClientRespository } from 'app/@core/respositorys/client.repository';
@Injectable({
    providedIn: 'root'
  })
export class GetClientList implements IUseCase<any, any> {
    response: any;
    constructor(private _clientRespository: ClientRespository) {}
    excute(params: any) {
        try {
           this.response = this._clientRespository.getItemAll();
        } catch (e) {
            console.log(e);
        }
    }
}