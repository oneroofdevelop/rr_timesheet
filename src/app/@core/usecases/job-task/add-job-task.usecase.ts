import { IUseCase } from '../usecase.interface';
import { Injectable } from '@angular/core';
import { JobTaskRespository } from 'app/@core/respositorys/job-task.respository';
@Injectable({
    providedIn: 'root'
  })
export class AddJobTask implements IUseCase<any, any> {
    response: any;
    constructor(private _jobTaskRepository: JobTaskRespository) {}
    excute(params: any) {
        try {
           this.response =  this._jobTaskRepository.addItem(params);
        } catch (e) {
            console.log(e);
        }
    }

}