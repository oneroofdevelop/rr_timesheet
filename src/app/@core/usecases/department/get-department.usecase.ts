import { IUseCase } from '../usecase.interface';
import { Injectable } from '@angular/core';
import { DepartmentRespository } from 'app/@core/respositorys/department.respository';
@Injectable({
    providedIn: 'root'
  })
export class GetDepartmentList implements IUseCase<any, any> {
    response: any;
    constructor(private _departmentRespository: DepartmentRespository) {}
    excute(params: any) {
        try {
            this.response = this._departmentRespository.getItemAll(params);
        } catch (e) {
            console.log(e);
        }
    }
}