export interface IUseCase<TInput, TOutput> {
    response: TOutput;
    excute(params: TInput);
}