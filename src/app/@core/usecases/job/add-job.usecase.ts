import { IUseCase } from '../usecase.interface';
import { JobRespository } from 'app/@core/respositorys/job.respository';
import { Injectable } from '@angular/core';
@Injectable({
    providedIn: 'root'
})
export class AddJob implements IUseCase<any, any> {
    response: any;
    constructor(private _jobRepository: JobRespository) { }
    excute(params: any) {
        try {
            this.response = this._jobRepository.addItem(params);
        } catch (e) {
            console.log(e);
        }
    }

}