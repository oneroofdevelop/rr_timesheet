import { IUseCase } from '../usecase.interface';
import { JobRespository } from 'app/@core/respositorys/job.respository';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
  })
export class GetJobList implements IUseCase<any, any> {
    response: any;
    constructor(private _jobRepository: JobRespository) {}
    excute(params: any | null) {
        try {
            this.response = this._jobRepository.getItemAll();
        } catch (e) {
            console.log(e);
        }
    }
}