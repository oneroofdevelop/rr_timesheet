import { IUseCase } from '../usecase.interface';
import { Injectable } from '@angular/core';
import { SubServiceRespository } from 'app/@core/respositorys/sub-service.respository';

@Injectable({
    providedIn: 'root'
})
export class GetSubServiceList implements IUseCase<any, any> {
    response: any;
    constructor(private _subServiceRespository: SubServiceRespository) { }
    excute(params: any) {
        try {
            this.response = this._subServiceRespository.getItemAll();
        } catch (e) {
            console.log(e);
        }
    }
}