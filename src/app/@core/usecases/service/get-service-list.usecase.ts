import { IUseCase } from '../usecase.interface';
import { Injectable } from '@angular/core';
import { SubServiceRespository } from 'app/@core/respositorys/sub-service.respository';

@Injectable({
    providedIn: 'root'
})
export class GetServiceList implements IUseCase<any, any> {
    response: any;
    constructor(private _serviceRespository: SubServiceRespository) { }
    excute(params: any) {
        try {
            this.response = this._serviceRespository.getItemAll();
        } catch (e) {
            console.log(e);
        }
    }
}