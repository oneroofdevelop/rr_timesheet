export class Client {
    compCode: string;
    compName: string;
    businessTypeId: number;
    businessTypeName: string;
    subBusinessTypeId: number;
    subBusinessTypeName: string;
    startDate: string;
    endDate: string;
    taxId: string;
    telNo: string;
    faxNo: string;
    termOfPayment: string;
    agencyFree: string;
    remark: string;
    fackbookId: string;
    website: string;
    isCompany: string;
    isActive: string;
}