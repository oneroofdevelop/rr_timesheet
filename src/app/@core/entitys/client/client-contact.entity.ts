export class ClientContact {
    personId: number;
    titleName: string;
    firstName: string;
    lastName: string;
    emailEmail: string;
    mobile: string;
    telephone: string;
    citizenId: string;
    passportPassport: string;
    compCode: string;
    departmentName: string;
    jobPositionName: string;
    contactTypeCfcode: number;
    personSourceCfcode: number;
    isMain: string;
    lineId: string;
    isActive: string;
}