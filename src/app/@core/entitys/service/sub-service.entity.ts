export class SubService {
    srvcId: number;
    srvcName: string;
    srvcSubId: number;
    srvcSubName: string;
    isActive: string;
}