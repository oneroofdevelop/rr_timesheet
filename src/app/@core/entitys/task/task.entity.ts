export class Task {
    taskId: string;
    taskCode: string;
    jobNumber: string;
    projectHId: string;
    parentTaskId: string;
    taskGroupId: string;
    priorityId: string;
    actualStartDate: Date;
    actualEndDate: Date;
    planStartDate: Date;
    planEndDate: Date;
    dueDate: Date;
    note: string;
    progressStatusId: string;
    hashtag: string;
    isActive: string;
}