import { Task } from '../task/task.entity';

export class Job {
    projectDId: number;
    projectHId: number;
    jobNo: string;
    jobDate: Date;
    jobStatus: string;
    statusDate: string;
    srvcId: string;
    subSrvcId: string;
    prodTypeCode: string;
    contactPerson: string;
    jobDesc: string;
    cancelReason: string;
    peRemk: string;
    peVatPct: string;
    peVatFlag: string;
    workBudget: number;
    startDate: Date;
    finishDate: Date;
    actlStartDate: Date;
    actlFinishDate: Date;
    issueBy: string;
    issueDate: Date;
    ackBy: string;
    ackDate: string;
    appvBy: string;
    appvDate: string;
    assignTo: string;
    assignDate: Date;
    freezeFlag: string;
    freezeYymm: string;
    clientAppvBy: string;
    clientAppvDate: Date;
    peAgcyFeePct: string;
    lastVerNo: string;
    lastVerDate: Date;
    verLock: string;
    peAgcyFeeAmt: string;
    remark: string;
    isActive: string;
    trTask: Task[];
    constructor() {
        this.jobDate = new Date();
        this.jobDesc = '';
        this.workBudget = 0;
        this.trTask = [];
        this.isActive = 'Y'
    }
}