export class Department {
    deptId: number;
    deptCode: string;
    deptName: string;
    jobHNDLFlag: string;
    isActive: string;
}