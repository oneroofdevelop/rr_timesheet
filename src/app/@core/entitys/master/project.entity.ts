export class Project {
    projectHId: string;
    projectCode: string;
    compCode: string;
    projectName: string;
    projectDesc: string;
    budgetValue: number;
    budgetUnitTypeId: number;
    budgetUnitTypeName: string;
    isActive: string;
    otherItems: string;
}