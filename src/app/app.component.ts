import { Component, Inject, OnDestroy, OnInit, LOCALE_ID } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Platform } from '@angular/cdk/platform';
import { TranslateService } from '@ngx-translate/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { FuseConfigService } from '@fuse/services/config.service';
import { FuseNavigationService } from '@fuse/components/navigation/navigation.service';
import { FuseSidebarService } from '@fuse/components/sidebar/sidebar.service';
import { FuseSplashScreenService } from '@fuse/services/splash-screen.service';
import { FuseTranslationLoaderService } from '@fuse/services/translation-loader.service';

import { MomentDateAdapter } from '@angular/material-moment-adapter';
import { Moment } from 'moment';
import * as moment from 'moment';
import { MAT_DATE_LOCALE, DateAdapter, MAT_DATE_FORMATS } from '@angular/material/core';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
moment.locale('us');
moment.fn.toJSON = function (): any {
    return this.format();
};

// tslint:disable-next-line: quotemark
import { formatMessage, loadMessages, locale, formatDate } from "devextreme/localization";
import * as Globalize from 'globalize/dist/globalize';
import { navigation } from './navigation/navigation';
import { AppUserService } from '@rr-iservice/service/app/app-user.service';
// formatDate = function (date: Moment): string {
//     return `${date.format('DD')} ${date.locale('th-TH').format('MMM')} ${date.year() + 543} `;
// };
export class MyDateAdapter extends MomentDateAdapter {

    format(date: any, displayFormat: string): string {
        date = moment(date);
        return `${date.format('DD')}/${date.format('MM')}/${date.year()}`;
    }

    getFirstDayOfWeek(): number {
        return 1;
    }
}
@Component({
    selector: 'app',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss'],
    providers: [
        { provide: LOCALE_ID, useValue: 'th-TH' },
        { provide: MAT_DATE_LOCALE, useValue: 'th-TH' },
        {
            provide: DateAdapter,
            useClass: MyDateAdapter,
            deps: [MAT_DATE_LOCALE]
        },
        {
            provide: MAT_DATE_FORMATS,
            useValue: {
                parse: {
                    // dateInput: 'DD/MM/YYYY',
                    dateInput: ['', 'DD/MM/YYYY'],
                    // dateInput: { month: 'numeric', year: 'numeric', day: 'numeric' },
                },
                display: {
                    dateInput: ['', 'DD/MM/YYYY'],
                    monthYearLabel: 'MMMM YYYY',
                    dateA11yLabel: 'LL',
                    monthYearA11yLabel: 'MMMM YYYY',
                }
            }
        }
    ],
})
export class AppComponent implements OnInit, OnDestroy {
    fuseConfig: any;
    navigation: any;

    // Private
    private _unsubscribeAll: Subject<any>;

    /**
     * Constructor
     *
     * @param {DOCUMENT} document
     * @param {FuseConfigService} _fuseConfigService
     * @param {FuseNavigationService} _fuseNavigationService
     * @param {FuseSidebarService} _fuseSidebarService
     * @param {FuseSplashScreenService} _fuseSplashScreenService
     * @param {FuseTranslationLoaderService} _fuseTranslationLoaderService
     * @param {Platform} _platform
     * @param {TranslateService} _translateService
     */
    constructor(
        @Inject(DOCUMENT) private document: any,
        private _fuseConfigService: FuseConfigService,
        private _fuseNavigationService: FuseNavigationService,
        private _fuseSidebarService: FuseSidebarService,
        private _fuseSplashScreenService: FuseSplashScreenService,
        private _fuseTranslationLoaderService: FuseTranslationLoaderService,
        private _translateService: TranslateService,
        private _platform: Platform,
        private appUserService: AppUserService,
        private router: Router,
        private route: ActivatedRoute,
        private dateAdapter: DateAdapter<any>
    ) {
        loadMessages({
            th: {
                // 'Search': 'กรองจากการค้นหา',
                // 'dxDataGrid-searchPanelPlaceholder': 'กรองจากการค้นหา',
                // 'dxDataGrid-filterRowOperationContains': 'เกี่ยวกับ',
                // 'dxNumberBox-noDataText': 'ไม่มีข้อมูล',
            }
        });
        // locale('th');
        // Globalize('es').formatDate( (val, format) =>{

        // });


        this.dateAdapter.setLocale('us-US');

        // Get default navigation
        this.navigation = navigation;
        // this.appUserService.navigation.subscribe(res => {
        //     this.navigation = res;

        //     this._fuseNavigationService.unregister('main');
        //     this._fuseNavigationService.register('main', this.navigation);

        //     // Set the main navigation as our current navigation
        //     this._fuseNavigationService.setCurrentNavigation('main');
        // });
        // Register the navigation to the service
        this._fuseNavigationService.register('main', this.navigation);

        // // Set the main navigation as our current navigation
        this._fuseNavigationService.setCurrentNavigation('main');

        // // Add languages
        // this._translateService.addLangs(['en', 'tr']);

        // // Set the default language
        // this._translateService.setDefaultLang('en');

        // // Set the navigation translations
        // this._fuseTranslationLoaderService.loadTranslations(navigationEnglish, navigationTurkish);

        // // Use a language
        // this._translateService.use('en');

        /**
         * ----------------------------------------------------------------------------------------------------
         * ngxTranslate Fix Start
         * ----------------------------------------------------------------------------------------------------
         */

        /**
         * If you are using a language other than the default one, i.e. Turkish in this case,
         * you may encounter an issue where some of the components are not actually being
         * translated when your app first initialized.
         *
         * This is related to ngxTranslate module and below there is a temporary fix while we
         * are moving the multi language implementation over to the Angular's core language
         * service.
         **/

        // Set the default language to 'en' and then back to 'tr'.
        // '.use' cannot be used here as ngxTranslate won't switch to a language that's already
        // been selected and there is no way to force it, so we overcome the issue by switching
        // the default language back and forth.
        /**
         setTimeout(() => {
            this._translateService.setDefaultLang('en');
            this._translateService.setDefaultLang('tr');
         });
         */

        /**
         * ----------------------------------------------------------------------------------------------------
         * ngxTranslate Fix End
         * ----------------------------------------------------------------------------------------------------
         */

        // Add is-mobile class to the body if the platform is mobile
        if (this._platform.ANDROID || this._platform.IOS) {
            this.document.body.classList.add('is-mobile');
        }

        // Set the private defaults
        this._unsubscribeAll = new Subject();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Lifecycle hooks
    // -----------------------------------------------------------------------------------------------------

    /**
     * On init
     */
    ngOnInit(): void {

        this.router.events.subscribe((val) => {
            if (val instanceof NavigationEnd) {
                const rrToken = sessionStorage.getItem('rrToken');
                if (rrToken && rrToken !== '') {
                    if (val.url.indexOf('pages') !== -1) {
                        this.appUserService.tokenChange.next(true);
                    }
                }
            }
        });

        // Subscribe to config changes
        this._fuseConfigService.config
            .pipe(takeUntil(this._unsubscribeAll))
            .subscribe((config) => {

                this.fuseConfig = config;

                // Boxed
                if (this.fuseConfig.layout.width === 'boxed') {
                    this.document.body.classList.add('boxed');
                }
                else {
                    this.document.body.classList.remove('boxed');
                }

                // Color theme - Use normal for loop for IE11 compatibility
                for (let i = 0; i < this.document.body.classList.length; i++) {
                    const className = this.document.body.classList[i];

                    if (className.startsWith('theme-')) {
                        this.document.body.classList.remove(className);
                    }
                }

                this.document.body.classList.add(this.fuseConfig.colorTheme);
            });
    }

    /**
     * On destroy
     */
    ngOnDestroy(): void {
        // Unsubscribe from all subscriptions
        this._unsubscribeAll.next();
        this._unsubscribeAll.complete();
    }

    // -----------------------------------------------------------------------------------------------------
    // @ Public methods
    // -----------------------------------------------------------------------------------------------------

    /**
     * Toggle sidebar open
     *
     * @param key
     */
    toggleSidebarOpen(key): void {
        this._fuseSidebarService.getSidebar(key).toggleOpen();
    }
}
