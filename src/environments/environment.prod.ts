export const environment = {
    production: true,
    hmr: false,
    iService: {
        apiUrl: 'http://202.183.221.130/iservice_api/api',
        hubUrl: 'http://202.183.221.130/api_core/notifyhub',
        reCaptchaKey: ''
    },
    webConfig: {

    },
    version: 'v.7.0.0 update Friday, Dec 04, 2020'
};
