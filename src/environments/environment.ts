// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
        production: false,
        hmr: false,
        iService: {
                apiUrl: 'http://localhost:28880/api',
                // apiUrl: 'http://202.183.221.130/iservice_api_dev/api',
                hubUrl: 'http://202.183.221.130/api_core/notifyhub',
                reCaptchaKey: ''
        },
        webConfig: {

        },
        version: 'v.7.0.0 update Friday, Dec 04, 2020'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
