import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({
  name: 'datetime'
})
export class DateTimePipe implements PipeTransform {

  transform(value: any, format?: string): any {
    let dateStr = "";
    let date: any;

    date = moment(value);
    if (date.isValid()) {
      date.add(543, 'years');
      if (format) {
        dateStr = date.format(format);
      } else {
        dateStr = date.format('DD/MM/YYYY');
      }
    }

    return dateStr;
  }

}
