export interface IAPIResultResponse {
    success: boolean;
    code: string;
    message: string;
    description: string;
    transactionId: string;
    transactionDateTime: string;
    items: any;
    otherItems: any;
    key: string;
}
