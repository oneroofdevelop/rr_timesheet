export interface IHttpClient {
    url: string;
    action: string;
    data: any;
    params?: any;
 }