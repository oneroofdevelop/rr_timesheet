export interface IViewResponse {
    status: boolean;
    data: any;
    msg: string;
}
