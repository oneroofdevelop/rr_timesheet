import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { IHttpClient } from '@utilies/model/IHttpClient';
import { IAPIResultResponse } from '@utilies/model/IAPIResultResponse';
import { IViewResponse } from '@utilies/model/IViewResponse';
import { AlertService } from '@utilies/service/alert.service';

@Injectable({
  providedIn: 'root'
})
export class HttpCoreService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
      'Accept': 'application/json',
    }),
    params: null
  };
  constructor(
    private alertService: AlertService,
    private httpClient: HttpClient,
  ) { }

  get(config: IHttpClient): Observable<any> {
    this.httpOptions.params = config.params;
    const uri = `${config.url}/${config.action}`;
    const apiResponse = this.httpClient.get<IAPIResultResponse>(uri, this.httpOptions);
    return this.readResponse(apiResponse);
  }
  post(config: IHttpClient): Observable<any> {
    this.httpOptions.params = config.params;
    const uri = `${config.url}/${config.action}`;
    const data = JSON.stringify(config.data);
    const apiResponse = this.httpClient.post<IAPIResultResponse>(uri, data, this.httpOptions);
    return this.readResponse(apiResponse);
  }
  private readResponse(apiResponse: Observable<IAPIResultResponse>): Observable<any> {
    return apiResponse.pipe(
      map((next: IAPIResultResponse) => {
        if (!next.success) {
          const res = {
            description: next.description,
            message: next.message
          };
          this.alertService.httpError(res);
          console.error(next);
          return 'error';
        } else {
          sessionStorage.setItem('rrToken', next.key);
          return next.items;
        }
      }, (error) => {
        this.alertService.httpError({
          description: 'error',
          message: 'การเชื่อมต่อไม่สำเร็จ'
        });
        const res = {
          status: false,
          msg: 'Http error.',
          data: error
        };
        console.error(res);
        // return res;
        return 'error';
        // }), catchError(err => {
        //   // return throwError(err);
        //   return 'error';

      }), catchError(err => {
        const res = {
          status: false,
          msg: `${err.statusText} : ${err.url}`,
          data: err
        };

        const httpError = {
          description: err.url,
          message: err.statusText
        };
        this.alertService.httpError(httpError);

        console.error('catchError', httpError);
        // return of(res);
        return throwError('error');
      }));
  }
}
