import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { IAuthority } from '@utilies/model/IAuthority';
import { AppUserService } from '@rr-iservice/service/app/app-user.service';

@Injectable()
export class ApiTokenAuthInterceptor implements HttpInterceptor {
    authority: IAuthority;
    constructor(private userService: AppUserService) {
        this.authority = {
            token: ""
        };
    }
    intercept(httpRequest: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

        this.authority.token = this.userService.loadToken();
        if (httpRequest.body instanceof FormData) {
            httpRequest = httpRequest.clone({
                setHeaders: {
                    'Authorization': `Bearer ${this.authority.token}`
                },
            });
        } else {
            httpRequest = httpRequest.clone({
                setHeaders: {
                    'Authorization': `Bearer ${this.authority.token}`
                },
            });
        }
        return next.handle(httpRequest);
    }
}
