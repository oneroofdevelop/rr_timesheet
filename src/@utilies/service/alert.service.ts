import { Injectable } from '@angular/core';
import { IAlertHttpErrorConfig, IAlertDangerConfig } from '@utilies/interface/alert.interface';
import { AlertHttpErrorComponent } from '@utilies/component/alert/alert-http-error/alert-http-error.component';
import { MatDialog } from '@angular/material/dialog';
import { AlertMessageComponent } from '@utilies/component/alert/alert-message/alert-message.component';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(
    public dialog: MatDialog,
) { }
  httpError(config: IAlertHttpErrorConfig): Promise<boolean> {
    return new Promise((resolve, reject) => {
      const dialogRef = this.dialog.open(AlertHttpErrorComponent, {
        data: config,
        width: '400px',
      });

      dialogRef.afterClosed().subscribe(res => {
        resolve(res);
      });
    });
  }
  danger(config: IAlertDangerConfig): Promise<void> {
    return new Promise((resolve, reject) => {
      const dialogRef = this.dialog.open(AlertMessageComponent, {
        data: config,
        width: '400px',
      });

      dialogRef.afterClosed().subscribe(res => {
        resolve(res);
      });
    });
  }
  info(config: IAlertDangerConfig): Promise<void> {
    return new Promise((resolve, reject) => {
      const dialogRef = this.dialog.open(AlertMessageComponent, {
        data: config,
        width: '400px',
      });

      dialogRef.afterClosed().subscribe(res => {
        resolve(res);
      });
    });
  }
}
