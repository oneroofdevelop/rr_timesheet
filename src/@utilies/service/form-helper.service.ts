import { Injectable } from '@angular/core';
import { FormGroup, FormControl, ValidationErrors } from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormHelperService {

  constructor() { }
  validateForm(formGroup: FormGroup): void {
    Object.keys(formGroup.controls).forEach(field => {
      const control = formGroup.get(field);
      if (control instanceof FormControl) {
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {
        this.validateForm(control);
      }
    });
  }
  resetValidateForm(form: FormGroup): void {
    Object.keys(form.controls).forEach(key => {
      form.get(key).setErrors(null);
      // form.reset();
    });
  }
  disableForm(form: FormGroup): void {
    Object.keys(form.controls).forEach(key => {
      form.get(key).disable();
    });
  }
  getValidationErrors(form: FormGroup): void {
    Object.keys(form.controls).forEach(key => {

      const controlErrors: ValidationErrors = form.get(key).errors;
      if (controlErrors != null) {
        Object.keys(controlErrors).forEach(keyError => {
          console.log('Key control: ' + key + ', keyError: ' + keyError + ', err value: ', controlErrors[keyError]);
        });
      }
    });
  }

}
