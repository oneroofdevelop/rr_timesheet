import { Injectable } from '@angular/core';
import { IDialogConfig } from '@utilies/interface/dialog.interface';
import { DialogConfirmComponent } from '@utilies/component/dialog/dialog-confirm/dialog-confirm.component';
import { MatDialog } from '@angular/material/dialog';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(
    public dialog: MatDialog,
  ) { }

  confirm(config: IDialogConfig): Promise<boolean> {
    return new Promise((resolve, reject) => {
      const dialogRef = this.dialog.open(DialogConfirmComponent, { 
        data: config,
        width: '400px',
      });

      dialogRef.afterClosed().subscribe(res => {
        resolve(res);
      });
    });
  }

}
