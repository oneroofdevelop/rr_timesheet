export interface IAlertHttpErrorConfig {
    message: string;
    description: string;
}
export interface IAlertDangerConfig {
    title: string;
    message: string;
 }