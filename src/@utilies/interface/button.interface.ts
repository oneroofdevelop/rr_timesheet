export interface IButtonConfirmConfig {
    dialogTitile?: string;
    dialogContent?: string;
    dialogBtnConfirmText?: string;
    dialogBtnCancelText?: string;

    btnText?: string;
    isIcon?: boolean;

}
