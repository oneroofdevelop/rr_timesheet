export interface IDialogConfig {
    dialogTitile?: string;
    dialogContent?: string;
    btnConfirmText?: string;
    btnCancelText?: string;
}
