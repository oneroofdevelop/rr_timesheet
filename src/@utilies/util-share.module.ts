import { NgModule } from '@angular/core';
import { MaterialDesignModule } from './material-design/material-design.module';
import { CommonModule } from '@angular/common';
import { NgxLoadingModule, ngxLoadingAnimationTypes } from 'ngx-loading';
import { DialogConfirmComponent } from './component/dialog/dialog-confirm/dialog-confirm.component';
import { ButtonConfirmComponent } from './component/button/button-confirm/button-confirm.component';
import { AlertHttpErrorComponent } from './component/alert/alert-http-error/alert-http-error.component';
import { AlertMessageComponent } from './component/alert/alert-message/alert-message.component';
import { DateTimePipe } from './pipe/date-time.pipe';

import { NgxCurrencyModule } from 'ngx-currency';
import { DatePickerRangeComponent } from './component/date-picker-range/date-picker-range.component';
import { FormsModule } from '@angular/forms';
import { DevExtremeModule } from './dev-extreme/dev-extreme.module';
import { MomentModule } from 'angular2-moment';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgOptionHighlightModule } from '@ng-select/ng-option-highlight';
import { DxDropDownBoxModule, DxTreeViewModule } from 'devextreme-angular';

const shareCompos = [
  ButtonConfirmComponent,
  DialogConfirmComponent,
  AlertHttpErrorComponent,
  AlertMessageComponent,
  DateTimePipe,
  DatePickerRangeComponent,

];

const ngxLoadingConfig = {
  animationType: ngxLoadingAnimationTypes.circleSwish,
  backdropBackgroundColour: 'rgba(0,0,0,0.1)',
  backdropBorderRadius: '4px',
  primaryColour: '#ffffff',
  secondaryColour: '#ffffff',
  tertiaryColour: '#ffffff'
};

export const customCurrencyMaskConfig = {
  align: 'right',
  allowNegative: false,
  allowZero: true,
  decimal: '.',
  precision: 0,
  prefix: '',
  suffix: '',
  thousands: ',',
  nullable: true
};

@NgModule({
  imports: [
    MaterialDesignModule,
    DevExtremeModule,
    DxDropDownBoxModule,
    DxTreeViewModule,
    CommonModule,
    NgxLoadingModule.forRoot(ngxLoadingConfig),
    NgxCurrencyModule.forRoot(customCurrencyMaskConfig),
    FormsModule,
    MomentModule,
    NgSelectModule,
    NgOptionHighlightModule
  ],
  exports: [
    MaterialDesignModule,
    DevExtremeModule,
    DxDropDownBoxModule,
    DxTreeViewModule,
    shareCompos,
    NgxLoadingModule,
    NgxCurrencyModule,
    MomentModule,
    NgSelectModule,
    NgOptionHighlightModule
  ],
  declarations: [shareCompos],
  providers: [
    DateTimePipe
  ],
  entryComponents: [
    DialogConfirmComponent,
    AlertHttpErrorComponent,
    AlertMessageComponent
  ]
})
export class UtilShareModule { }
