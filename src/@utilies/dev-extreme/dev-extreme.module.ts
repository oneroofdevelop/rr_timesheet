import { NgModule } from '@angular/core';
import {
  DxDataGridModule,
  DxTemplateModule,
  DxBulletModule,
  DxPopoverModule,
  DxHtmlEditorModule,
  DxTooltipModule
} from 'devextreme-angular';

export const dxModule = [
  DxDataGridModule,
  DxTemplateModule,
  DxBulletModule,
  DxPopoverModule,
  DxHtmlEditorModule,
  DxTooltipModule
];

@NgModule({
  imports: [dxModule],
  exports: [dxModule],
})
export class DevExtremeModule { }
