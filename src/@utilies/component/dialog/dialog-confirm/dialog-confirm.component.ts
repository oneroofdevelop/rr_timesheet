import { Component, OnInit, Inject } from '@angular/core';
import { IDialogConfig } from '@utilies/interface/dialog.interface';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'dialog-confirm',
  templateUrl: './dialog-confirm.component.html',
  styleUrls: ['./dialog-confirm.component.css']
})
export class DialogConfirmComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<DialogConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IDialogConfig
  ) { }

  ngOnInit(): void {
    this.data = {
      btnCancelText: this.data.btnCancelText || 'Cancel',
      btnConfirmText: this.data.btnConfirmText || 'Confirm',
      dialogContent: this.data.dialogContent || 'Are you sure you want to do this?',
      dialogTitile: this.data.dialogTitile || 'Confirm action.',
    }
  }

  confirm(): void {
    this.dialogRef.close(true);
  }
  close(): void {
    this.dialogRef.close(false);
  }
}
