import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { IButtonConfirmConfig } from '@utilies/interface/button.interface';
import { DialogService } from '@utilies/service/dialog.service';

@Component({
  selector: 'button-confirm',
  templateUrl: './button-confirm.component.html',
  styleUrls: ['./button-confirm.component.css']
})
export class ButtonConfirmComponent implements OnInit {
  @Input() config: IButtonConfirmConfig;
  @Output() anwser: EventEmitter<boolean>;
  constructor(private dialogService: DialogService) {
    this.anwser = new EventEmitter(false);
    this.config = {

    };
  }

  ngOnInit(): void {
    this.config = {
      btnText: this.config.btnText || 'Confirm',
      isIcon: this.config.isIcon || false,
      dialogContent: this.config.dialogContent,
      dialogTitile: this.config.dialogTitile
    };
  }
  openDialog(): void {
    this.dialogService.confirm({
      btnCancelText: this.config.dialogBtnCancelText,
      btnConfirmText: this.config.dialogBtnConfirmText,
      dialogContent: this.config.dialogContent,
      dialogTitile: this.config.dialogTitile
    }).then(res => {
      this.anwser.emit(res);
    });
  }

  closeDialog(): void {

  }
}
