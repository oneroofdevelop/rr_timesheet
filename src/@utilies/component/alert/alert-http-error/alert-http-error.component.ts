import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { DialogConfirmComponent } from '@utilies/component/dialog/dialog-confirm/dialog-confirm.component';
import { IAlertHttpErrorConfig } from '@utilies/interface/alert.interface';

@Component({
  selector: 'app-alert-http-error',
  templateUrl: './alert-http-error.component.html',
  styleUrls: ['./alert-http-error.component.css']
})
export class AlertHttpErrorComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<DialogConfirmComponent>,
    @Inject(MAT_DIALOG_DATA) public data: IAlertHttpErrorConfig
  ) { }

  ngOnInit() {
  }
   
}
