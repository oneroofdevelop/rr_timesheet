import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import * as moment from 'moment';
moment.fn.toJSON = function (): any {
  return this.format();
};
export interface IRangeDate {
  startDate: Date;
  endDate: Date;
}
@Component({
  selector: 'date-picker-range',
  templateUrl: './date-picker-range.component.html',
  styleUrls: ['./date-picker-range.component.css']
})
export class DatePickerRangeComponent implements OnInit, OnChanges {
  startDate = new FormControl(null);
  endDate = new FormControl(null);
  @Input() start: any;
  @Input() end: any;
  @Output() rangeDate: EventEmitter<IRangeDate> = new EventEmitter<IRangeDate>();
  constructor(

  ) {
    this.startDate.valueChanges.subscribe(res => {
      this.rangeDate.emit({
        startDate: res,
        endDate: this.endDate.value
      });
    });

    this.endDate.valueChanges.subscribe(res => {
      this.rangeDate.emit({
        startDate: this.startDate.value,
        endDate: res
      });
    });
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (changes.start) {
      this.startDate.setValue(changes.start.currentValue);
    }
    if (changes.end) {
      this.endDate.setValue(changes.end.currentValue);
    }
  }

  ngOnInit(): void {

  }

}
