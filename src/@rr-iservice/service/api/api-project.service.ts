import { Injectable } from '@angular/core';
import { HttpCoreService } from '@utilies/http/http-core.service';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { IHttpClient } from '@utilies/model/IHttpClient';

@Injectable({
    providedIn: 'root'
})
export class ApiProjectService {
    private env = environment;
    constructor(private httpCoreService: HttpCoreService) { }
    saveProject(data: any): Observable<any> {
      return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'saveProject', data } as IHttpClient);
    }
    getProjectList(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getProjectList', data } as IHttpClient);
    }  
}
