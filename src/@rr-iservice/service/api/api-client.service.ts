import { Injectable } from '@angular/core';
import { HttpCoreService } from '@utilies/http/http-core.service';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { IHttpClient } from '@utilies/model/IHttpClient';

@Injectable({
    providedIn: 'root'
})
export class ApiClientService {
    private env = environment;
    constructor(private httpCoreService: HttpCoreService) { }
    saveClient(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'saveClient', data } as IHttpClient);
    }
    getClientList(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getClientList', data } as IHttpClient);
    }  
    getContactList(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getContactList', data } as IHttpClient);
    }      
   
    saveContact(data: any): Observable<any[]>{
        return this.httpCoreService.post({url: this.env.iService.apiUrl, action: 'saveContact', data} as IHttpClient);
    }

    getClientCareList(data: any): Observable<any[]>{
        return this.httpCoreService.post({url: this.env.iService.apiUrl, action: 'getClientCareList', data} as IHttpClient);
    }    
    
}
