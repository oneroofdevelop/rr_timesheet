import { Injectable } from '@angular/core';
import { HttpCoreService } from '@utilies/http/http-core.service';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { IHttpClient } from '@utilies/model/IHttpClient';
import { IApiGetNotiListResponse } from '@rr-iservice/interface/api/api-noti.interface';
@Injectable({
    providedIn: 'root'
})

export class ApiNotiService {
    private env = environment;
    constructor(private httpCoreService: HttpCoreService) { }

    getNotiList(): Observable<IApiGetNotiListResponse[]> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getNotifyList' } as IHttpClient);
    }
    readNotify(data): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'readNotify', data } as IHttpClient);
    }
    readNotifyList(data): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'readNotifyList', data } as IHttpClient);
    }
}
