import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { HttpCoreService } from '@utilies/http/http-core.service';
import { IViewResponse } from '@utilies/model/IViewResponse';
import { IHttpClient } from '@utilies/model/IHttpClient';
import { IApiLoginRequest, IApiGetMenuResponse, IApiGetUserProfileResponse, IApiLoginResponse } from '@rr-iservice/interface/api/api-user.interface';


@Injectable({
  providedIn: 'root'
})
export class ApiUserService {
  private env = environment;
  constructor(private httpCoreService: HttpCoreService) { }

  login(data: IApiLoginRequest): Observable<IApiLoginResponse> {
    data.userName += '@relationshiprepublic.com';
    return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'login', data } as IHttpClient) as Observable<IApiLoginResponse>;
  }

  logout(): Observable<IApiLoginResponse> {
    return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'logout', data: {} } as IHttpClient) as Observable<IApiLoginResponse>;
  }
  savePassword(data: any): Observable<any> {
    return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'savePassword', data } as IHttpClient) as Observable<IApiLoginResponse>;
  }
  getMenu(): Observable<Array<IApiGetMenuResponse>> {
    return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getMenu', data: {} } as IHttpClient) as Observable<Array<IApiGetMenuResponse>>;
  }

  getUserProfile(): Observable<Array<IApiGetUserProfileResponse>> {
    return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getUserProfile', data: {} } as IHttpClient) as Observable<Array<IApiGetUserProfileResponse>>;
  }
}
