import { Injectable } from '@angular/core';
import { HttpCoreService } from '@utilies/http/http-core.service';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { IHttpClient } from '@utilies/model/IHttpClient';

@Injectable({
    providedIn: 'root'
})
export class ApiEmployeeService {
    private env = environment;
    constructor(private httpCoreService: HttpCoreService) { }
    saveEmployee(data: any): Observable<any> {
      return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'saveEmployee', data } as IHttpClient);
    }
    getEmployeeList(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getEmployeeList', data } as IHttpClient);
    }  
    getEmployeeAllList(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getEmployeeAllList', data } as IHttpClient);
    } 
}
