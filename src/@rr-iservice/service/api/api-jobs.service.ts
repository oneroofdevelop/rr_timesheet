import { Injectable } from '@angular/core';
import { HttpCoreService } from '@utilies/http/http-core.service';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { IHttpClient } from '@utilies/model/IHttpClient';

@Injectable({
    providedIn: 'root'
})
export class ApiJobsService {
    private env = environment;
    constructor(private httpCoreService: HttpCoreService) { }

    getJobsList(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getJobList', data } as IHttpClient);
    }
    getJobDetail(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getJobDetail', data } as IHttpClient);
    }
    getTaskGroupList(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getTaskGroupList', data } as IHttpClient);
    }
    getSummaryTaskList(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getSummaryTaskList', data } as IHttpClient);
    }
    saveJob(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'saveJob', data } as IHttpClient);
    }  
    saveJobTask(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'saveJobTark', data } as IHttpClient);
    }  
    saveAcceptJob(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'acceptJobTask', data } as IHttpClient);
    }    
    
    
}
