import { Injectable } from '@angular/core';
import { HttpCoreService } from '@utilies/http/http-core.service';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { IHttpClient } from '@utilies/model/IHttpClient';

@Injectable({
    providedIn: 'root'
})
export class ApiTaskService {
    private env = environment;
    constructor(private httpCoreService: HttpCoreService) { }
    saveTask(data: any): Observable<any> {
      return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'saveTask', data } as IHttpClient);
    }
    getTask(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getTask', data } as IHttpClient);
    }  
    getTaskDetail(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getTaskDetails', data } as IHttpClient);
    }  
    getMasterTaskList(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getMasterTaskList', data } as IHttpClient);
    }      
    getMasterTaskStatusList(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getMasterTaskStatusList', data } as IHttpClient);
    }          
    saveTaskList(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'saveTaskList', data } as IHttpClient);
    }          

}
