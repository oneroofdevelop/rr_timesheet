import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpCoreService } from '@utilies/http/http-core.service';
import { Observable } from 'rxjs';
import { IHttpClient } from '@utilies/model/IHttpClient';

@Injectable({
  providedIn: 'root'
})
export class ApiTimerService {
  private env = environment;
  constructor(private httpCoreService: HttpCoreService) { }
  getDataTimeSheet(data: any): Observable<any> {
    return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getDataTimeSheet', data } as IHttpClient);
  }
  getTaskCerrentTimeSheet(): Observable<any> {
    return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getTaskCerrentTimeSheet', data: {} } as IHttpClient);
  }
  saveTimeSheet(data: any): Observable<any> {
    return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'saveTimeSheet', data } as IHttpClient);
  }
  getTimeSheetHistoryByTaskCode(data: any): Observable<any> {
    return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getTimeSheetHistoryByTaskCode', data } as IHttpClient);
}
}
