import { Injectable } from '@angular/core';
import { HttpCoreService } from '@utilies/http/http-core.service';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { IHttpClient } from '@utilies/model/IHttpClient';

@Injectable({
    providedIn: 'root'
})
export class ApiMasterTimeService {
    private env = environment;
    constructor(private httpCoreService: HttpCoreService) { }
    getEstimationTimeList(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getEstimationTimeList', data } as IHttpClient);
    }  
}
