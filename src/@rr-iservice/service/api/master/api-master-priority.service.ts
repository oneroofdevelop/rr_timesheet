import { Injectable } from '@angular/core';
import { HttpCoreService } from '@utilies/http/http-core.service';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';
import { IHttpClient } from '@utilies/model/IHttpClient';

@Injectable({
    providedIn: 'root'
})
export class ApiMasterPriorityService {
    private env = environment;
    constructor(private httpCoreService: HttpCoreService) { }
    getPriorityList(data: any): Observable<any> {
        return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'getPriorityList', data } as IHttpClient);
    }  
}
