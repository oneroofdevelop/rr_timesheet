import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpCoreService } from '@utilies/http/http-core.service';
import { Observable } from 'rxjs';
import { IHttpClient } from '@utilies/model/IHttpClient';

@Injectable({
  providedIn: 'root'
})
export class ApiDepartmentService {
  private env = environment;

  constructor(private httpCoreService: HttpCoreService) { }

  getDepartmentList(): Observable<any> {
    return this.httpCoreService.post({
      url: this.env.iService.apiUrl,
      action: 'getDepartmentList',
      data: {}
    } as IHttpClient);
  }
}
