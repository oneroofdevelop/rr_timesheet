import { IHttpClient } from '@utilies/model/IHttpClient';
import { Observable } from 'rxjs';
import { HttpCoreService } from '@utilies/http/http-core.service';
import { environment } from 'environments/environment';
import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiMasterConfigService {
  private env = environment;

  constructor(private httpCoreService: HttpCoreService) { }

  getMasterConfigList(headerCode: string): Observable<any> {
    return this.httpCoreService.post({
      url: this.env.iService.apiUrl,
      action: 'getMasterConfigList',
      data: {
        configHCode: headerCode
      }
    } as IHttpClient).pipe(map(value => {
      const words = headerCode.toLowerCase().split('_');
      let keyName = '';
      words.forEach((word, index) => {
        if (index === 0) {
          keyName += word;
        } else {
          keyName += word.charAt(0).toUpperCase() + word.slice(1);
        }
      });
      let lang = 'E';
      if (headerCode === 'PRE_NAME') {
        lang = 'T';
      }
      value.forEach(item => {
        item[keyName + 'Name'] = item[keyName + `${lang}Name`];
      });
      return value;
    }));
  }
}
