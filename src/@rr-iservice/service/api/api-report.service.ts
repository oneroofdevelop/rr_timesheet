import { Injectable } from '@angular/core';
import { HttpCoreService } from '@utilies/http/http-core.service';
import { IHttpClient } from '@utilies/model/IHttpClient';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiReportService {
  private env = environment;
  constructor(private httpCoreService: HttpCoreService) { }
  weekly(data: any): Observable<any> {
    return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'reportSummaryWorktime', data } as IHttpClient);
  }
  rawData(data: any): Observable<any> {
    return this.httpCoreService.post({ url: this.env.iService.apiUrl, action: 'reportRawDataWorktime', data } as IHttpClient);
  }
}
