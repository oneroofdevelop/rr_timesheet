import { Injectable } from '@angular/core';
import { ApiClientService } from '../api/api-client.service';
import { ApiEmployeeService } from '../api/api-employee.service';
import { BehaviorSubject } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class AppEmployeeService {
    employeeList: BehaviorSubject<any[]>;
    employeeAllList: BehaviorSubject<any[]>;
    constructor(
      private apiEmployeeService: ApiEmployeeService,
     ) {
      this.employeeList = new BehaviorSubject(new Array<any>());
      this.employeeAllList = new BehaviorSubject(new Array<any>());
    }
    getEmployeeList(requestData: any): Promise<any[]> {
        return new Promise((resolve, reject) => {
          this.apiEmployeeService.getEmployeeList(requestData).subscribe(res => {
            this.employeeList.next(res);
            resolve();
          });
        });
    }
    getEmployeeAllList(requestData: any): Promise<any[]> {
      return new Promise((resolve, reject) => {
        this.apiEmployeeService.getEmployeeAllList(requestData).subscribe(res => {
          this.employeeAllList.next(res);
          resolve();
        });
      });
  }    
}
