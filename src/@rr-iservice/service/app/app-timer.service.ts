import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApiTimerService } from '../api/api-timer.service';
import * as moment from 'moment';
import { AppUserService } from './app-user.service';
import { log } from 'util';

@Injectable({
  providedIn: 'root'
})
export class AppTimerService {
  records: BehaviorSubject<any[]>;
  timing: BehaviorSubject<any>;
  userProfile: any;
  timeSheetHistory: BehaviorSubject<any[]>;

  constructor(
    private apiTimerService: ApiTimerService,
    private appUserService: AppUserService,
  ) {
    this.records = new BehaviorSubject([]);
    this.timing = new BehaviorSubject({});
    this.timeSheetHistory = new BehaviorSubject([]);
    this.appUserService.userProfile.subscribe(res => {
      this.userProfile = res;
    });
  }

  getRecords(req): Promise<any> {
    return new Promise((resolve, reject) => {
      this.apiTimerService.getDataTimeSheet(req).subscribe(res => {
        this.records.next(res);
        resolve(res);
      });
    });
  }

  getTimeSheetHistoryByTaskCode(requestData: any): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiTimerService.getTimeSheetHistoryByTaskCode(requestData).subscribe(res => {
        console.log('TimeSheetHistory', res);
        this.timeSheetHistory.next(res);
        resolve(res);
      });
    });
  }
  
  refreshRecords(): Promise<any> {
    return new Promise((resolve, reject) => {
      const req = localStorage.getItem('rrTimesheetReq');
      if (!req) {
        resolve();
      }
      const reStr = JSON.parse(req);
      this.getRecords(reStr).then(res => resolve(res));
    });
  }
  getTimingInfo(): Promise<any> {

    if (!this.userProfile) {
      return;
    }
    return new Promise((resolve, reject) => {
      this.apiTimerService.getTaskCerrentTimeSheet().subscribe(res => {
        this.timing.next(res);
        resolve(res);
      });
    });
  }

  saveTiming(timeSheetId: number, workingTime: number, startDate: any, taskCode: string, timeSheetActive: string): Promise<any> {
    const req = {
      timeSheetId: timeSheetId,
      taskCode: taskCode,
      startDate: startDate,
      endDate: moment(),
      workTime: workingTime,
      timeSheetActive: timeSheetActive
    };
    return new Promise((resolve, reject) => {
      this.apiTimerService.saveTimeSheet(req).subscribe(res => {
        this.getTimingInfo();
        this.refreshRecords();
        resolve(res);
      });
    });
  }

  createTiming(taskCode: string): Promise<any> {
    const req = {
      timeSheetId: 0,
      taskCode: taskCode,
      startDate: moment(),
      endDate: null,
      workTime: 0,
      timeSheetActive: 'A'
    };
    return new Promise((resolve, reject) => {
      this.apiTimerService.saveTimeSheet(req).subscribe(res => {
        this.getTimingInfo();
        resolve(res);
      });
    });
  }

  getWorkEstimation(startDate: Date, endDate: Date): any {
    const start = moment(moment(startDate).format('YYYY-MM-DD'));
    const end = moment(moment(endDate).format('YYYY-MM-DD'));

    if (start > end) {
      return 0;
    }
    if (!start || !end) {
      return 0;
    }
    let count = end.diff(start, 'days');
    if (end === start) {
      count = 1;
    } else {
      count += 1;
    }

    return count * 8;
  }

  getWorkTime(startDate: Date): any {
    const start = moment(startDate);
    const end = moment();

    if (start > end) {
      return 0;
    }
    if (!start || !end) {
      return 0;
    }
    let count = end.diff(start, 'minutes');
    if (count === 0) {
      count = 1;
    }

    return count;
  }

  saveTimeManual(resquestData: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.apiTimerService.saveTimeSheet(resquestData).subscribe(res => {
        this.getTimingInfo();
        this.refreshRecords();
        resolve(res);
      });
    });
  }


} 
