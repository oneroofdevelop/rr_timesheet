import { Injectable } from '@angular/core';
import { ApiClientService } from '../api/api-client.service';
import { BehaviorSubject } from 'rxjs';
import { ApiNotiService } from '../api/api-noti.service';
import { IAppGetNotiListResponse } from '@rr-iservice/interface/app/app-noti.interface';


@Injectable({
  providedIn: 'root'
})
export class AppNotiService {
  notiList: BehaviorSubject<any[]>;
  constructor(
    private apiNotiService: ApiNotiService,
  ) {
    this.notiList = new BehaviorSubject([]);
  }
  getNotiList(): Promise<Array<any>> {
    return new Promise((resolve, reject) => {
      this.apiNotiService.getNotiList().subscribe(res => {
        this.notiList.next(res);
        resolve(res);
      });
    });

    // return new Promise((resolve, reject) => {
    //     const data = [
    //                  {notifyHeader:"การแจ้งเตือน1",notifyDesc:"detail noti 1",readStatus:"read"}
    //                 ,{notifyHeader:"การแจ้งเตือน2",notifyDesc:"detail noti 2",readStatus:"unread"}
    //             ] as Array<IAppGetNotiListResponse>;
    //     this.notiList.next(data);
    //     resolve(data);
    //   });
  }
  readNotify(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.apiNotiService.readNotify(data).subscribe(res => {
        this.getNotiList();
        resolve(res);
      });
    });
  }

  readNotifyList(data): Promise<any> {
    return new Promise((resolve, reject) => {
      this.apiNotiService.readNotifyList(data).subscribe(res => {
        this.getNotiList();
        resolve(res);
      });
    });
  }
}