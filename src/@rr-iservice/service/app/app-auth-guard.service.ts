import { Injectable } from '@angular/core';
import { Router, CanActivate, CanActivateChild } from '@angular/router';
import { AppUserService } from './app-user.service';
 
@Injectable({
  providedIn: 'root'
})
export class AppAuthGuardService implements CanActivate, CanActivateChild {

  constructor(private appUserService: AppUserService, public router: Router) { }
  canActivate(): boolean {
    return true;
  }

  canActivateChild(): boolean {
    if (!this.appUserService.loadToken()) {
      this.appUserService.logout();
      this.router.navigate(['login']);
      return false;
    }
    return true;
  }
}
