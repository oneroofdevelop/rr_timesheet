import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApiMasterService } from '@rr-iservice/service/api/master/api-master.service';
import { ApiMasterConfigService } from '@rr-iservice/service/api/master/api-master-config.service';

@Injectable({
  providedIn: 'root'
})
export class AppMasterService {
  [x: string]: any;
  serviceList: BehaviorSubject<any[]>;
  subServiceList: BehaviorSubject<any[]>;
  statusJobList: BehaviorSubject<any[]>;
  constructor(
    private apiMasterService: ApiMasterService,
    private apiMasterConfugService: ApiMasterConfigService,
  ) {
    this.serviceList = new BehaviorSubject(new Array<any>());
    this.subServiceList = new BehaviorSubject(new Array<any>());
    this.statusJobList = new BehaviorSubject(new Array<any>());
  }
  getServiceList(data: any): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiMasterService.getServiceList(data).subscribe(res => {
        this.serviceList.next(res);
        resolve(res);
      });
    });
  }
  getSubServiceList(data: any): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiMasterService.getSubServiceList(data).subscribe(res => {
        this.subServiceList.next(res);
        resolve(res);
      });
    });
  }


}
