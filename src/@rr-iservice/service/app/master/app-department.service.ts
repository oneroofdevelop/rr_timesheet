import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApiMasterPriorityService } from '@rr-iservice/service/api/master/api-master-priority.service';
import { ApiDepartmentService } from '@rr-iservice/service/api/master/api-department.service';

@Injectable({
  providedIn: 'root'
})
export class AppDepartmentService {
  constructor(
    private apiDepartmentService: ApiDepartmentService,
  ) {

  }
  getDepartmentList(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiDepartmentService.getDepartmentList().subscribe(res => {
        resolve(res);
      });
    });
  }
}
