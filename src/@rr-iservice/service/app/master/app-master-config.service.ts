import { ApiMasterConfigService } from './../../api/master/api-master-config.service';
import { BehaviorSubject } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AppMasterConfigService {
  specialDept: BehaviorSubject<any[]>;
  constructor(
    private apiMasterConfugService: ApiMasterConfigService,
  ) {
    this.specialDept = new BehaviorSubject(new Array<any>());
  }
  getPreNameList(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiMasterConfugService.getMasterConfigList('PRE_NAME').subscribe(res => {
        resolve(res);
      });
    });
  }
  getJobStatusList(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiMasterConfugService.getMasterConfigList('JOB_STATUS').subscribe(res => {
        resolve(res);
      });
    });
  }
  getJobTaskStatusList(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiMasterConfugService.getMasterConfigList('JOB_TASK_STATUS').subscribe(res => {
        resolve(res);
      });
    });
  }
  getTaskStatusList(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiMasterConfugService.getMasterConfigList('TASK_STATUS').subscribe(res => {
        resolve(res);
      });
    });
  }
  getPriorityList(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiMasterConfugService.getMasterConfigList('PRIORITY').subscribe(res => {
        resolve(res);
      });
    });
  }

  getSpecialDepartmentList(): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiMasterConfugService.getMasterConfigList('SPECIAL_DEPT').subscribe(res => {
        this.specialDept.next(res);
        resolve(res);
      });
    });
  }
} 
