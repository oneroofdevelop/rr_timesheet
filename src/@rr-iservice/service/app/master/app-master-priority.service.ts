import { Injectable } from '@angular/core';
import { ApiMasterTimeService } from '@rr-iservice/service/api/master/api-master-time.service';
import { BehaviorSubject } from 'rxjs';
import { ApiMasterPriorityService } from '@rr-iservice/service/api/master/api-master-priority.service';


@Injectable({
  providedIn: 'root'
})
export class AppMasterPriorityService {
  priorityList: BehaviorSubject<any[]>;
  constructor(
    private apiMasterPriorityService: ApiMasterPriorityService,
  ) {
    this.priorityList = new BehaviorSubject(new Array<any>());
  }
  getPriorityList(requestData: any): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiMasterPriorityService.getPriorityList(requestData).subscribe(res => {
        this.priorityList.next(res);
        resolve(res);
      });
    });
  }
}
