import { Injectable } from '@angular/core';
import { ApiMasterTimeService } from '@rr-iservice/service/api/master/api-master-time.service';
import { BehaviorSubject } from 'rxjs';


@Injectable({
    providedIn: 'root'
})
export class AppMasterTimeService {
    estimationTimeList: BehaviorSubject<any[]>;
    constructor(
      private apiMasterTimeService: ApiMasterTimeService,
     ) {
      this.estimationTimeList = new BehaviorSubject(new Array<any>());
    }
    getEstimationTimeList(requestData: any): Promise<any[]> {
        return new Promise((resolve, reject) => {
          this.apiMasterTimeService.getEstimationTimeList(requestData).subscribe(res => {
            this.estimationTimeList.next(res);
            resolve();
          });
        });
    }
}
