import { Injectable } from '@angular/core';
import { ApiClientService } from '../api/api-client.service';
import { ApiJobsService } from '../api/api-jobs.service';
import { BehaviorSubject } from 'rxjs';
import { log } from 'util';


@Injectable({
  providedIn: 'root'
})
export class AppJobsService {
  jobsList: BehaviorSubject<any[]>;
  jobDetail: BehaviorSubject<any>;
  jobsGroupList: BehaviorSubject<any[]>;
  summaryTaskList: BehaviorSubject<any[]>;
  constructor(
    private apiJobsService: ApiJobsService,
  ) {
    this.jobsList = new BehaviorSubject(new Array<any>());
    this.jobDetail = new BehaviorSubject(null);
    this.jobsGroupList = new BehaviorSubject(new Array<any>());
    this.summaryTaskList = new BehaviorSubject(new Array<any>());

  }

  getJobsList(requestData: any): Promise<any[]> {
    requestData.deptCodeList = ['AC', 'MM', 'TM'];
    return new Promise((resolve, reject) => {
      this.apiJobsService.getJobsList(requestData).subscribe(res => {
        this.jobsList.next(res);
        resolve(res);
      });
    });
  }
  getJobDetail(jobNo: string): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiJobsService.getJobDetail({ jobNo: jobNo }).subscribe(res => {
        this.jobDetail.next(res);
        resolve(res);
      });
    });
  }
  getTaskGroupList(requestData: any): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiJobsService.getTaskGroupList(requestData).subscribe(res => {
        this.jobsGroupList.next(res);
        resolve(res);
      });
    });
  }
  getSummaryTaskList(requestData: any): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiJobsService.getSummaryTaskList(requestData).subscribe(res => {
        this.summaryTaskList.next(res);
        resolve(res);
      });
    });
  }
  saveJob(requestData: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.apiJobsService.saveJob(requestData).subscribe(res => {
        resolve(res);
      });
    });
  }
  saveJobTask(requestData: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.apiJobsService.saveJobTask(requestData).subscribe(res => {
        resolve(res);
      });
    });
  }  
  
  saveAcceptJob(requestData: any): Promise<any> {
    return new Promise((resolve, reject) => {
      this.apiJobsService.saveAcceptJob(requestData).subscribe(res => {
        resolve(res);
      });
    });
  }

}
