import { Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { environment } from 'environments/environment';

@Injectable({
  providedIn: 'root'
})
export class HubService {
  private env = environment;
  hubConnection: HubConnection;

  constructor() {

  }
  start(): void {
    this.hubConnection = new HubConnectionBuilder()
      .withUrl(this.env.iService.hubUrl)
      .withAutomaticReconnect()
      .build();

    this.hubConnection.serverTimeoutInMilliseconds = 100000; // 100 second
    this.hubConnection.keepAliveIntervalInMilliseconds = 100000; // 100 second

    this.hubConnection
      .start()
      .then(() => console.log('Connection started'))
      .catch(err => console.log('Error while starting connection: ' + err));
  }
}
