import { Injectable } from '@angular/core';
import { ApiTaskService } from '../api/api-task.service';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AppTaskService {
  statusList: BehaviorSubject<any[]>;
  dataTaskFrom: BehaviorSubject<any[]>;
  summaryTaskList: BehaviorSubject<any[]>;
  dataJobDetail: BehaviorSubject<any>;
  dataJobTask: BehaviorSubject<any>;
  constructor(
    private apiTaskService: ApiTaskService,
  ) {
    this.statusList = new BehaviorSubject(new Array<any>());
    this.dataTaskFrom = new BehaviorSubject(new Array<any>());
    this.dataJobDetail = new BehaviorSubject(null);
    this.dataJobTask = new BehaviorSubject(null);
  }
  saveTask(requestData: any): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiTaskService.saveTask(requestData).subscribe(res => {
        resolve();
      });
    });
  }
  getTaskDetail(requestData: any): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiTaskService.getTaskDetail(requestData).subscribe(res => {
        this.dataTaskFrom.next(res);
        resolve(res);
      });
    });
  }
  getMasterTaskList(requestData: any): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiTaskService.getMasterTaskList(requestData).subscribe(res => {
        resolve();
      });
    });
  }

  getMasterTaskStatusList(requestData: any): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiTaskService.getMasterTaskStatusList(requestData).subscribe(res => {
        this.statusList.next(res);
        resolve();
      });
    });
  }

  getSummaryTaskList(requestData: any): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiTaskService.getMasterTaskStatusList(requestData).subscribe(res => {
        this.summaryTaskList.next(res);
        resolve();
      });
    });
  }
  saveTaskList(requestData: any): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiTaskService.saveTaskList(requestData).subscribe(res => {
        resolve();
      });
    });
  }
}
