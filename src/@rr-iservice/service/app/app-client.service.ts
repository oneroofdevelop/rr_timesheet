import { ISaveContactRequest } from './../../interface/api/api-contact.interface';
import { Injectable } from '@angular/core';
import { ApiClientService } from '../api/api-client.service';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AppClientService {
  clientList: BehaviorSubject<any[]>;
  clientSelected: BehaviorSubject<any>;
  contactList: BehaviorSubject<any[]>;
  constructor(
    private apiClientService: ApiClientService,
  ) {
    this.clientList = new BehaviorSubject([]);
    this.clientSelected = new BehaviorSubject([]);
    this.contactList = new BehaviorSubject([]);

  }
  getClientList(requestData: any): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiClientService.getClientList(requestData).subscribe(res => {
        this.clientList.next(res);
        resolve();
      });
    });
  }
  getContactList(requestData: any): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiClientService.getContactList(requestData).subscribe(res => {
        this.contactList.next(res);
        resolve();
      });
    });
  }
  saveContact(requestData: any): Promise<any>{
    return new Promise((resolve, reject) => {
      this.apiClientService.saveContact(requestData).subscribe(res => {
        this.getContactList({compCodeList: [requestData.contact.compCode]});
        resolve();
      });
    });
  }
  getClientCareList(requestData: any): Promise<any>{
    return new Promise((resolve, reject) => {
      this.apiClientService.getClientCareList(requestData).subscribe(res => {
        resolve(res);
      });
    });
  }
  
}
