import { Injectable } from '@angular/core';
import { ApiTaskService } from '../api/api-task.service';
import { ApiProjectService } from '../api/api-project.service';
import { BehaviorSubject } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AppProjectService {
  projectList: BehaviorSubject<any[]>;
  constructor(
    private apiProjectService: ApiProjectService,
  ) {
    this.projectList = new BehaviorSubject(new Array<any>());
  }
  getProjectList(requestData: any): Promise<any[]> {
    return new Promise((resolve, reject) => {
      this.apiProjectService.getProjectList(requestData).subscribe(res => {
        this.projectList.next(res);
        resolve(res);
      });
    });
  }
}
