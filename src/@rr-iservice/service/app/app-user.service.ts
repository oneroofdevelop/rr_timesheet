import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApiUserService } from '../api/api-user.service';
import { FuseNavigation } from '@fuse/types';
import { navigation } from 'app/navigation/navigation';
import { Router, ActivatedRoute } from '@angular/router';
import { IAppGetUserProfileResponse } from '@rr-iservice/interface/app/app-user.interface';
import { IApiLoginRequest, IApiGetMenuResponse, IApiLoginResponse } from '@rr-iservice/interface/api/api-user.interface';
import { AppProjectService } from './app-project.service';
import { AppEmployeeService } from './app-employee.service';

@Injectable({
  providedIn: 'root'
})
export class AppUserService {
  tokenChange: BehaviorSubject<boolean>;
  navigation: BehaviorSubject<FuseNavigation[]>;
  testNavigation: FuseNavigation[] = navigation;
  userProfile: BehaviorSubject<any>;

  constructor(
    private router: Router,
    private apiUserService: ApiUserService,
    private appProjectService: AppProjectService,
    private appEmployeeService: AppEmployeeService,
  ) {
    this.tokenChange = new BehaviorSubject(false);
    this.navigation = new BehaviorSubject(new Array<FuseNavigation>());
    this.userProfile = new BehaviorSubject(null);

    this.tokenChange.subscribe(res1 => {
      if (res1) {
        this.apiUserService.getMenu().subscribe(res2 => {
          if (res2) {
            const newMenu = this.getFuseMenu(res2);
            this.navigation.next(newMenu);
          }
        });

        this.getUserProfile();
      }
    });
  }
  resetPassword(requestData: any): Promise<any> {
    return new Promise((resolve) => {
      this.apiUserService.savePassword(requestData).subscribe(res => {
        resolve(res);
      });
    });
  }
  login(requestData: IApiLoginRequest): Promise<IApiLoginResponse> {
    return new Promise((resolve) => {
      this.apiUserService.login(requestData).subscribe(res => {
        resolve(res);
      });
    });
  }
  // tslint:disable-next-line:typedef
  logout() {
    // this.apiUserService.logout().subscribe(res => {
    this.clearToken().then(() => {
      this.router.navigate(['/login']);
    });
    // }) 
  }
  loadToken(): string {
    let token = '';
    const session = sessionStorage.getItem('rrToken');
    if (session) {
      token = session;
    }
    return token;
  }
  // tslint:disable-next-line:typedef
  clearToken() {
    return new Promise((resolve) => {
      sessionStorage.clear();
      resolve();
    });
  }

  getUserProfile(): Promise<Array<IAppGetUserProfileResponse>> {
    return new Promise((resolve) => {
      this.apiUserService.getUserProfile().subscribe(res => {
        this.userProfile.next(res);
        this.appEmployeeService.getEmployeeList({});
        resolve(res);
      });
    });
  }

  private getFuseMenu(data: Array<IApiGetMenuResponse>): FuseNavigation[] {
    let result = [];
    result = data.map(parent => {
      let parentItem = null;
      parentItem = this.convertToFuseMenu(parent);
      parentItem.children = parent.children.map(this.convertToFuseMenu);
      parentItem.type = parentItem.children.length > 0 ? 'collapsable' : 'item';
      return parentItem;
    });
    return result;
    // return this.testNavigation;
  }
  private convertToFuseMenu(data: IApiGetMenuResponse): FuseNavigation {
    return {
      id: data.id.toString(),
      title: data.name,
      icon: data.icon,
      type: 'item',
      url: `pages/${data.path}`
    } as FuseNavigation;
  }
  getUserToken(): any {
    return JSON.parse(window.atob(sessionStorage.getItem('rrToken').split('.')[1]));
  }
}
