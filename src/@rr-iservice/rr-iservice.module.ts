import { ReportWorkDetailModalComponent } from './components/report/report-work-detail-modal/report-work-detail-modal.component';
import { NgModule } from '@angular/core';
import { TaskFromComponent } from './components/task/task-from/task-from.component';
import { RouterModule } from '@angular/router';
import { UtilShareModule } from '@utilies/util-share.module';
import { TaskFromModalComponent } from './components/task/task-from-modal/task-from-modal.component';
import { FuseSharedModule } from '@fuse/shared.module';
import { FuseSidebarModule, FuseWidgetModule } from '@fuse/components';
import { EmployeeFilterComponent } from './components/master/employee-filter/employee-filter.component';
import { ProjectFilterComponent } from './components/master/project-filter/project-filter.component';
import { TimerGridComponent } from './components/timer/timer-grid/timer-grid.component';
import { TimerPanelComponent } from './components/timer/timer-panel/timer-panel.component';
import { TimerButtonComponent } from './components/timer/timer-button/timer-button.component';
import { CustomerFilterComponent } from './components/master/customer-filter/customer-filter.component';
import { JobsGridComponent } from './components/jobs/jobs-grid/jobs-grid.component';
import { DateAdapter, MAT_DATE_LOCALE } from '@angular/material/core';
import { MyDateAdapter } from 'app/app.component';
import { ClientFilterComponent } from './components/master/client-filter/client-filter.component';
import { TaskCommentComponent } from './components/task/task-comment/task-comment.component';
import { JobsTaskGridComponent } from './components/jobs/jobs-task-grid/jobs-task-grid.component';
import { JobsTaskModalComponent } from './components/jobs/jobs-task-modal/jobs-task-modal.component';
import { JobsTaskFormComponent } from './components/jobs/jobs-task-form/jobs-task-form.component';
import { CustomerSelectedItemComponent } from './components/master/client-selected-item/client-selected-item.component';
import { ProjectSelectedItemComponent } from './components/master/project-selected-item/project-selected-item.component';
import { NotiComponent } from './components/noti/noti.component';
import { ContactFormModalComponent } from './components/contact/contact-form-modal/contact-form-modal.component';
import { ContactFormComponent } from './components/contact/contact-form/contact-form.component';
import { TimerManualFormComponent } from './components/timer/timer-manual-form/timer-manual-form.component';
import { TimerManualModalComponent } from './components/timer/timer-manual-modal/timer-manual-modal.component';
import { ContactAddButtonComponent } from './components/contact/contact-add-button/contact-add-button.component';
import { JobsAssignModalComponent } from './components/jobs/jobs-assign-modal/jobs-assign-modal.component';
import { JobsAssignFormComponent } from './components/jobs/jobs-assign-form/jobs-assign-form.component';
import { JopsTaskListComponent } from './components/jobs/jops-task-list/jops-task-list.component';
import { ResetPasswordModalComponent } from './components/reset-password/reset-password-modal/reset-password-modal.component';
import { TimerHistoryModalComponent } from './components/timer/timer-history-modal/timer-history-modal.component';
import { TimerHistoryGridComponent } from './components/timer/timer-history-grid/timer-history-grid.component';
import { SummaryWorktimeGridComponent } from './components/report/summary-worktime-grid/summary-worktime-grid.component';
import { NotiGridComponent } from './components/noti/noti-grid/noti-grid/noti-grid.component';
import { JobsDialogTaskGroupComponent } from './components/jobs/jobs-dialog-task-group/jobs-dialog-task-group.component';
import { ReportWorkFormComponent } from './components/report/report-work-form/report-work-form.component';

const rrIserviceComps = [
   TaskFromComponent,
   TaskFromModalComponent,
   EmployeeFilterComponent,
   ProjectFilterComponent,
   TimerGridComponent,
   TimerPanelComponent,
   TimerButtonComponent,
   CustomerFilterComponent,
   JobsGridComponent,
   JobsTaskGridComponent,
   JobsTaskModalComponent,
   JobsTaskFormComponent,
   ClientFilterComponent,
   TaskCommentComponent,
   CustomerSelectedItemComponent,
   ProjectSelectedItemComponent,
   NotiComponent,
   ContactFormComponent,
   ContactFormModalComponent,
   TimerManualFormComponent,
   TimerManualModalComponent,
   ContactAddButtonComponent,
   JobsAssignModalComponent,
   JobsAssignFormComponent,
   JopsTaskListComponent,
   ResetPasswordModalComponent,
   TimerHistoryModalComponent,
   TimerHistoryGridComponent,
   SummaryWorktimeGridComponent,
   NotiGridComponent,
   JobsDialogTaskGroupComponent,
   ReportWorkDetailModalComponent,
   ReportWorkFormComponent
];

@NgModule({
   imports: [
      RouterModule,
      UtilShareModule,
      FuseSharedModule,
      FuseSidebarModule,
      FuseWidgetModule
   ],
   exports: [
      rrIserviceComps
   ],
   declarations: [
      rrIserviceComps,
   ],
   entryComponents: [
      TaskFromModalComponent,
      JobsTaskModalComponent,
      ContactFormModalComponent,
      TimerManualModalComponent,
      JobsAssignModalComponent,
      ResetPasswordModalComponent,
      TimerHistoryModalComponent,
      JobsDialogTaskGroupComponent,
   ],
   providers: [
      // { provide: DateAdapter, useClass: CustomDateAdapter },
      { provide: DateAdapter, useClass: MyDateAdapter, deps: [MAT_DATE_LOCALE] },
   ]
})
export class RrIserviceModule { }
