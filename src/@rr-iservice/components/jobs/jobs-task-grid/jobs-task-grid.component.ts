import { Component, OnInit, Input } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { JobsTaskModalComponent } from '../jobs-task-modal/jobs-task-modal.component';
import { JobsAssignModalComponent } from '../jobs-assign-modal/jobs-assign-modal.component';
import { AppJobsService } from '@rr-iservice/service/app/app-jobs.service';
import { TaskFromModalComponent } from '@rr-iservice/components/task/task-from-modal/task-from-modal.component';

@Component({
  selector: 'rr-jobs-task-grid',
  templateUrl: './jobs-task-grid.component.html',
  styleUrls: ['./jobs-task-grid.component.css']
})
export class JobsTaskGridComponent implements OnInit {
  jobDetail: any;
  @Input() isShowButton: boolean;
  @Input() dataSource: any[];
  @Input() dueDate: any[];
  @Input() jobStatusCode: any;
  constructor(
    private dialog: MatDialog,
    private appJobService: AppJobsService) {
    this.appJobService.jobDetail.subscribe(res => {
      this.jobDetail = res;
    });
  }

  ngOnInit(): void {
  }
  openDialogTaskFrom(cell: any): void {
    const dialogRef = this.dialog.open(TaskFromModalComponent, {
      width: '800px',
      data: {
        actionType: 'formJobs',
        task: cell.data,
        jobsDetail: this.jobDetail,
      }
    });

    dialogRef.afterClosed().subscribe(() => {

    });
  }
  openJobTaskModal(cell: any): void {
    const dialogRef = this.dialog.open(JobsTaskModalComponent, {
      width: '800px',
      data: {
        task: cell.data,
        actionType: 'edit',
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.dataSource[cell.rowIndex] = result;
      }

      if (this.jobDetail) {
        this.appJobService.getJobDetail(this.jobDetail.jobNo);
      }

    });
  }
  // openJobAssignModal(data: any): void {
  //   const dialogRef = this.dialog.open(JobsAssignModalComponent, {
  //     width: '400px',
  //     data: data
  //   });

  //   dialogRef.afterClosed().subscribe(result => {
  //     if (this.jobDetail) {
  //       this.appJobService.getJobDetail(this.jobDetail.jobNo);
  //     }
  //   });
  // }

  deleteJobTask(event: any, cell: any): void {
    if (event) {
      this.dataSource.splice(cell.rowIndex, 1);
    }
  }
}
