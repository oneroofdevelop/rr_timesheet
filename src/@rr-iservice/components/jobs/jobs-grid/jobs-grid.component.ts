import { Component, OnInit } from '@angular/core';
import { AppJobsService } from '@rr-iservice/service/app/app-jobs.service';

@Component({
  selector: 'rr-jobs-grid',
  templateUrl: './jobs-grid.component.html',
  styleUrls: ['./jobs-grid.component.css']
})
export class JobsGridComponent implements OnInit {
  dataSource: any[];
  collapsed = false;
  chooserTitle: any;
  dateList = [];
  constructor(private appJobsService: AppJobsService
  ) {
    this.dataSource = [];
    this.appJobsService.jobsList.subscribe(res => this.dataSource = res);
    this.chooserTitle = 'คอลัมน์';
  }
  ngOnInit(): void {

  }
  isAssign(data: any): boolean {
    return data.isAssigned === 'Y';
  }
}
