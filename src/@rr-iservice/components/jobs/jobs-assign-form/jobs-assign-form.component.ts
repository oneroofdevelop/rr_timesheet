import { Component, OnInit, Inject, Input, Output, EventEmitter } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Employee } from '@rr-iservice/interface/app/app-employee.interface';
import { AppTimerService } from '@rr-iservice/service/app/app-timer.service';
import { AppTaskService } from '@rr-iservice/service/app/app-task.service';
import { AlertService } from '@utilies/service/alert.service';
import List from 'linqts/dist/src/list';
import { AppEmployeeService } from '@rr-iservice/service/app/app-employee.service';
import { IAppAcceptJobTaskRequest, IAppAcceptJobTaskRequest_Team_TeamList } from '@rr-iservice/interface/app/app-jobs.interface';
import { AppJobsService } from '@rr-iservice/service/app/app-jobs.service';
@Component({
    selector: 'rr-jobs-assign-form',
    templateUrl: './jobs-assign-form.component.html',
    styleUrls: ['./jobs-assign-form.component.css']
})
export class JobsAssignFormComponent implements OnInit {
    jobAssignForm: FormGroup;
    employeeList: Employee[];
    userProfile: any;
    dataAcceptFrom: any;
    jobDetail: any;

    @Input() formDataSource: any;
    @Output() formDataSubmit: EventEmitter<any> = new EventEmitter();
    dataFrom: any;
    constructor(
      private appTimerService: AppTimerService,
      private appTaskService: AppTaskService,
      private alertService: AlertService,
      private appEmployeeService: AppEmployeeService,
      private appJobService: AppJobsService,
    ) {
      this.setFormValidate();
      this.loadData();

      this.appEmployeeService.employeeList.subscribe(res => {
        this.employeeList = res;
      });

      this.appJobService.jobDetail.subscribe(res => {
        this.jobDetail = res;
        if (this.jobDetail) {
        }
      });
    }

    setFormValidate(): void {
        this.jobAssignForm = new FormGroup({
            assignList: new FormControl(''),
            dueDate: new FormControl(''),
            priority: new FormControl(''),
            taskJobName: new FormControl(''),
        });
      
    }
    setFromData(dataFrom: any): void {
        if (dataFrom) {
            if(dataFrom.team?.teamRoleList){
              const empNoList = new List<any>(dataFrom.team.teamRoleList).Select(a => a.empNo).ToArray();
              this.jobAssignForm.get('assignList').setValue(empNoList);
            }
        }

    }
    setFormEvent(): void {
    }
    setDefualtEstimation(): void {
    }
    loadData(): void {
    }

    ngOnInit(): void {
        this.setFromData(this.formDataSource);
    }

    onSave(): void {
        const formData = this.jobAssignForm.value;
        if(!this.formDataSource.team.teamRoleList){
            formData.assignList = formData.assignList.map(item => {
                return {
                  empNo: item,
                  isMain:'N',
                  isActive: 'Y'
                };
              });
        }else{
          const activeList = new List<any>(formData.assignList);
          const newList = [];
          formData.assignList.forEach(item => {
            newList.push({
              empNo: item,
              isMain:'N',
              isActive: 'Y'
            });
          });

          this.formDataSource.team.teamRoleList.forEach(element => {
            if (!activeList.Contains(element.empNo)) {
              newList.push({
                empNo: element.empNo,
                isMain:'N',
                isActive: 'N'
              });
            }
          });

          formData.assignList = newList;
        }

        const data = {
          jobTaskCode: this.formDataSource.jobTaskCode,
          compCode: this.jobDetail.compCode,
          projectCode: this.jobDetail.projectCode,
          jobNo: this.formDataSource.jobNo,
          deptCode: this.formDataSource.deptCode,
          acceptStatus: 'Y',
          team: {
            teamCode: this.formDataSource.team?.teamCode || '',
            teamName: '',
            teamRoleList: formData.assignList
          }
        };
        this.appJobService.saveAcceptJob(data).then(res => {
            this.alertService.info({ title: 'Task', message: 'New Jobs Assign saved.' }).then(res2 =>{
                this.formDataSubmit.emit(true);
            });
            this.appTimerService.refreshRecords();
          });
    }

}