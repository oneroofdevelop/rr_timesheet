import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
@Component({
    selector: 'rr-jobs-assign-modal',
    templateUrl: './jobs-assign-modal.component.html',
    styleUrls: ['./jobs-assign-modal.component.css']
})
export class JobsAssignModalComponent implements OnInit {
    constructor(
        public dialogRef: MatDialogRef<JobsAssignModalComponent>,
        @Inject(MAT_DIALOG_DATA)
        public data: any) { }
    ngOnInit(): void {
    }
    onJobAssignFormSubmit(event): void {
        this.dialogRef.close(event);
    }
}


