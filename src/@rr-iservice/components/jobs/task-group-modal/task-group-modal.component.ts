import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';


@Component({
    selector: 'rr-task-group-modal',
    templateUrl: './task-group-modal.component.html',
    styleUrls: ['./task-group-modal.component.css']
  })
export class TaskGroupModalComponent implements OnInit {
  taskGroupList: any[];
  taskGroupModal: FormGroup;

    constructor(
      public dialogRef: MatDialogRef<TaskGroupModalComponent>,
      @Inject(MAT_DIALOG_DATA) public data: any
    ) { 
      
    }
  
    ngOnInit(): void {
  
    }
    isActionAdd(): boolean {
        return true;
    }
    onConfirm(): void {
      this.dialogRef.close();
    }
    onJobTaskFormSubmit(event): void {
      this.dialogRef.close(event);
    }
}
  