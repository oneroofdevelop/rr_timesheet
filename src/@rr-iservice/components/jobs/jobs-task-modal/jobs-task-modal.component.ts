import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'rr-jobs-task-modal',
  templateUrl: './jobs-task-modal.component.html',
  styleUrls: ['./jobs-task-modal.component.css']
})
export class JobsTaskModalComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<JobsTaskModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { 
    
  }

  ngOnInit(): void {

  }

  onJobTaskFormSubmit(event): void {
    console.log(' onJobTaskFormSubmit(event): void {', event);
    this.dialogRef.close(event);
  }
}
