import { Task } from './../../../../app/@core/entitys/task/task.entity';
import { map, take, filter } from 'rxjs/operators';
import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AppDepartmentService } from '@rr-iservice/service/app/master/app-department.service';
import { List } from 'linqts';
import { AlertService } from '@utilies/service/alert.service';
import { AppMasterConfigService } from '@rr-iservice/service/app/master/app-master-config.service';
import { AppJobsService } from '@rr-iservice/service/app/app-jobs.service';
import { AppUserService } from '@rr-iservice/service/app/app-user.service';
import { FormHelperService } from '@utilies/service/form-helper.service';
import { AppEmployeeService } from '@rr-iservice/service/app/app-employee.service';
import _ from 'underscore';
import * as moment from 'moment';
import { AppTaskService } from '@rr-iservice/service/app/app-task.service';
import { JobsDialogTaskGroupComponent } from '../jobs-dialog-task-group/jobs-dialog-task-group.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'rr-jobs-task-form',
  templateUrl: './jobs-task-form.component.html',
  styleUrls: ['./jobs-task-form.component.css']
})
export class JobsTaskFormComponent implements OnInit {
  // #region Component
  jobDetail: any;
  departmentList: any[];
  priorityList: any[];
  jobTaskStatusList: any[];
  employeeList: any[];
  selfEmployeeList: any[];
  userProfile: any;
  taskGroupList: any[];
  taskGroupSelected: string;
  // #endregion

  // #region Task
  jobTaskForm: FormGroup;
  // #endregione
  editPermission: boolean;
  @Input() dataSource: any;
  @Output() formDataSubmit: EventEmitter<any[]> = new EventEmitter();

  constructor(
    private appDepartmentService: AppDepartmentService,
    private appMasterConfigService: AppMasterConfigService,
    private alertService: AlertService,
    private appJobService: AppJobsService,
    private appUserService: AppUserService,
    private formBuilder: FormBuilder,
    private formHelperService: FormHelperService,
    private appEmployeeService: AppEmployeeService,
    private appTaskService: AppTaskService,
    public dialog: MatDialog,
  ) {
    this.setSubscribe();
    this.loadData();
  }

  ngOnInit(): void {

    this.editPermission = true;
    this.setFormValidate();
    this.setFormDefault();
    this.setFormEvent();
    console.log('this.dataSource',this.dataSource);
    if (this.dataSource.task) {
      this.setFormData();
    }
  }

  setFormValidate(): void {
    this.jobTaskForm = this.formBuilder.group({
      jobTaskCode: [''],
      jobTaskName: [''],
      note: ['', Validators.required],
      deptCode: this.isActionAdd() ? [[]] : ['', Validators.required],
      planStartDate: ['', Validators.required],
      planEndDate: ['', Validators.required],
      dueDate: ['', Validators.required],
      priorityCode: [''],
      taskGroupCode: [''],
      jobTaskStatusCode: [''],
      assignList: [[]],
    });

  }
  isCanAssing(): boolean {
    const { task } = this.dataSource;
    return !(task === null);
  }
  setFormDefault(): void {
    const { task } = this.dataSource;

    if (!task) {
      this.jobTaskForm.get('jobTaskStatusCode').disable();
      this.jobTaskForm.get('jobTaskStatusCode').setValue('OP');
      if (this.dataSource.dueDate) {
        this.jobTaskForm.get('dueDate').setValue(this.dataSource.dueDate || '');
        this.jobTaskForm.get('planEndDate').setValue(this.dataSource.dueDate || '');

      }
      if (this.dataSource.briefDate) {
        this.jobTaskForm.get('planStartDate').setValue(this.dataSource.briefDate || '');

      }
      return;
    }

    const { dueDate, jobTaskStatusCode } = task;
    if (dueDate) {
      this.jobTaskForm.get('dueDate').setValue(dueDate || '');
    }
    // jobTaskStatusCode
    this.jobTaskForm.get('jobTaskStatusCode').enable();
    this.jobTaskForm.get('jobTaskStatusCode').setValue(jobTaskStatusCode);


    // assignList
    this.setAssignSourceList();
  }
  onSelectAll(): void{
    const selected = this.departmentList.map(item => item.deptCode);
    console.log('onSelectAll(): void{', selected);
    this.jobTaskForm.get('deptCode').setValue(selected);
  }
  setFormEvent(): void {
    if (this.jobDetail?.jobStatusCode === 'CL') {      
      const assignList = this.jobTaskForm.get('assignList');
      assignList.disable();
    }
  }
  isSetRequired(): string {
    if (this.dataSource.task && this.dataSource.task !== null) {
      return 'required';
    }
    return '';
  }
  setFormData(): void {

    this.jobTaskForm.get('jobTaskCode').setValue(this.dataSource.task.jobTaskCode);
    this.jobTaskForm.get('jobTaskName').setValue(this.dataSource.task.jobTaskName);
    this.jobTaskForm.get('note').setValue(this.dataSource.task.note);
    this.jobTaskForm.get('deptCode').setValue(this.dataSource.task.deptCode);
    this.jobTaskForm.get('planStartDate').setValue(this.dataSource.task.planStartDate || '');
    this.jobTaskForm.get('planEndDate').setValue(this.dataSource.task.planEndDate || '');
    this.jobTaskForm.get('dueDate').setValue(this.dataSource.task.dueDate || '');
    this.jobTaskForm.get('priorityCode').setValue(this.dataSource.task.priorityCode);
    this.jobTaskForm.get('jobTaskStatusCode').setValue(this.dataSource.task.jobTaskStatusCode);

    const empNoList = new List<any>(this.dataSource.task.team?.teamRoleList).Select(a => a.empNo).ToArray();
    this.jobTaskForm.get('assignList').setValue(empNoList);
  }

  setSubscribe(): void {
    this.appUserService.userProfile.subscribe(res => {
      this.userProfile = res;
    });
    this.appJobService.jobDetail.subscribe(res => {
      this.jobDetail = res;
    });
    this.appEmployeeService.employeeAllList.subscribe(res => {
      this.selfEmployeeList = res;      
    });
    
    this.appJobService.jobsGroupList.subscribe(res => {
      this.taskGroupList = res;
    });
  }

  loadData(): void {
    this.appMasterConfigService.getJobTaskStatusList().then(res => {
      this.jobTaskStatusList = res;
    });
    this.appMasterConfigService.getPriorityList().then(res => {
      this.priorityList = res;
    });
    this.appDepartmentService.getDepartmentList().then(res => {
      this.departmentList = res;
    });
    this.appJobService.getTaskGroupList({

    });
  }
  isActionAdd(): boolean{
    if (this.dataSource.actionType === 'add' && !this.jobDetail) {
      return true;
    }
    return false;    
  }
  findInvalid(): any[] {
    const invalid = [];
    const controls = this.jobTaskForm.controls;
    for (const name in controls) {
      if (controls[name].invalid) {
        invalid.push(name, controls[name].value);
      }
    }
    return invalid;
  }
  openDialog(): void {
    const dialogRef = this.dialog.open(JobsDialogTaskGroupComponent, {
      width: '400px',
      data: {}
    });
    dialogRef.afterClosed().subscribe(result => {
      const {taskGroupCode, isCancel} = result;
      if( isCancel ){
        return;
      }
      this.taskGroupSelected = taskGroupCode;
      this.onSaveEdit();
    });
  }  
  checkAssignList(assignList): any {
    let list = [];
    if (!this.dataSource?.task?.team?.teamRoleList) {
      list = assignList.map(item => {
        return {
          empNo: item,
          isMain: 'N',
          isActive: 'Y'
        };
      });
    } else {
      const activeList = new List<any>(assignList);
      const teamRoleList =  new List<any>(this.dataSource.task.team.teamRoleList);
      const newList = [];
      assignList.forEach(item => {
        newList.push({
          empNo: item,
          isMain: 'N',
          isNew: !(_.filter(this.dataSource.task?.team?.teamRoleList, i => { 
            return i.empNo == item }
            ).length > 0),
          isActive: 'Y'
        });
      });

      this.dataSource.task.team.teamRoleList.forEach(element => {
        if (!activeList.Contains(element.empNo)) {
          newList.push({
            empNo: element.empNo,
            isMain: 'N',
            isNew: false,
            isActive: 'N'
          });
        }
      });
      list =  newList;
    }   
    return list;
  }
  onSave(): void {
      if (this.isActionAdd()) {
        this.onSaveAdd();
      }else{
        const formData = this.jobTaskForm.getRawValue();
        formData.assignList = this.checkAssignList(formData.assignList);
        if(_.filter(formData.assignList, item=>{ return item.isNew}).length > 0) {
          this.openDialog();
        }else{
          this.onSaveEdit();
        }
      }
  }
  onSaveEdit(): void {
    const formData = this.jobTaskForm.getRawValue();
    if (this.jobTaskForm.invalid) {
      this.alertService.info({ title: 'Task', message: 'ตรวจสอบข้อมูลให้ครบถ้วนก่อนการบันทึก' });
      this.formHelperService.getValidationErrors(this.jobTaskForm);
      return;
    }
    if (this.dataSource.task === null) {
      this.dataSource.task = {};
    }
    formData.assignList = this.checkAssignList(formData.assignList);
    this.dataSource.task.deptCode = formData.deptCode;
    this.dataSource.task.jobTaskName = formData.jobTaskName;
    this.dataSource.task.taskGroupCode = this.taskGroupSelected; // ใช้เฉพาะกรณีที่เพิ่มคนใหม่เท่านั้น
    this.dataSource.task.note = formData.note;
    this.dataSource.task.deptName = new List(this.departmentList).Where(a => a.deptCode === formData.deptCode).FirstOrDefault().deptName;
    this.dataSource.task.planStartDate = formData.planStartDate;
    this.dataSource.task.planEndDate = formData.planEndDate;
    this.dataSource.task.dueDate = formData.dueDate;
    this.dataSource.task.priorityCode = formData.priorityCode;
    this.dataSource.task.priorityName = new List(this.priorityList).Where(a => a.priorityCode === formData.priorityCode).FirstOrDefault().priorityName;
    this.dataSource.task.estimated = formData.estimated;
    this.dataSource.task.jobTaskStatusCode = formData.jobTaskStatusCode;
    this.dataSource.task.jobTaskStatusName = new List(this.jobTaskStatusList).Where(a => a.jobTaskStatusCode === formData.jobTaskStatusCode).FirstOrDefault().jobTaskStatusName;

    if (this.jobDetail) {
        this.saveTaskAssing();
    } else {
        this.formDataSubmit.emit(this.dataSource.task);
    }
  
  }

  onSaveAdd(): void {
    const formData = this.jobTaskForm.getRawValue();
    console.log('onSave(): void {', formData);
    if (this.jobTaskForm.invalid || formData.deptCode.length === 0 ) {
      this.alertService.info({ title: 'Task', message: 'ตรวจสอบข้อมูลให้ครบถ้วนก่อนการบันทึก' });
      this.formHelperService.getValidationErrors(this.jobTaskForm);
      return;
    }
    if (this.dataSource.task === null) {
      this.dataSource.task = {};
    }
    const data = [];
    formData.deptCode.map(i => {
      data.push({
        deptCode: i,
        jobTaskName: formData.jobTaskName,
        note: formData.note,
        deptName: new List(this.departmentList).Where(a => a.deptCode === i).FirstOrDefault().deptName,
        planStartDate: formData.planStartDate,
        planEndDate: formData.planEndDate,
        dueDate: formData.dueDate,
        priorityCode: formData.priorityCode,
        priorityName: new List(this.priorityList).Where(a => a.priorityCode === formData.priorityCode).FirstOrDefault().priorityName,
        estimated: formData.estimated,
        jobTaskStatusCode: formData.jobTaskStatusCode,
        jobTaskStatusName: new List(this.jobTaskStatusList).Where(a => a.jobTaskStatusCode === formData.jobTaskStatusCode).FirstOrDefault().jobTaskStatusName,
      });

    });
    if (this.jobDetail) {
      this.saveTaskAssing();
    } else {
      this.formDataSubmit.emit(data);
    }
  }

  setDataTaskList(formData: any, newList: any[]): any[] {
    const { compCode, projectCode, jobNo } = this.jobDetail;
    const taskList = _.filter(newList, item => {
      return item.isAddtask === 'Y';
    }).map(item => {
      return {
        compCode: compCode,
        projectCode: projectCode,
        jobNo: jobNo,
        taskCode: '',
        taskName: formData.jobTaskName,
        taskGroupCode: this.taskGroupSelected,
        note: formData.note,
        priorityCode: formData.priorityCode,
        planStartDate: formData.planStartDate,
        planEndDate: formData.planEndDate,
        dueDate: formData.dueDate,
        taskStatusCode: 'OP',
        estimated: 0,
        assignList: [
          {
            empNo: item.empNo,
            taskCode: '',
            isActive: 'Y',
            isSendNotify: 'Y'
          }
        ]
      };
    });

    return taskList;
  }

  newJobsTask(): void {
    const { deptCode, jobNo, compCode } = this.jobDetail;
    const formData = this.jobTaskForm.getRawValue();
    const newList = [];
    formData.assignList.forEach(item => {
      newList.push({
        empNo: item,
        isMain: 'N',
        isSendNotify: 'Y',
        isActive: 'Y',
        isAddtask: 'N',
      });
    });

    const dataTeamList = [{
      teamName: '',
      deptCode: formData.deptCode,
      jobNo: jobNo,
      teamRoleList: [],
    }];

    const payload = {
      jobTaskId: 0,
      jobTaskName: formData.jobTaskName,
      note: formData.note,
      projectCode: this.jobDetail.projectCode,
      jobNo: jobNo,
      compCode: compCode,
      jobTaskCode: '',
      deptCode: formData.deptCode,
      planStartDate: formData.planStartDate,
      planEndDate: formData.planEndDate,
      dueDate: formData.dueDate,
      priorityCode: formData.priorityCode,
      jobTaskStatusCode: formData.jobTaskStatusCode,
      teamList: [],
    };
    const taskList = this.setDataTaskList(formData, newList);
    this.save(payload, taskList);
  }

  updateJobsTask(task: any): void {
    const formData = this.jobTaskForm.getRawValue();
    const { team, deptCode, jobNo, jobTaskId, jobTaskCode } = task;
    let empNoIscancel = [];
    let empNoIsActive = [];
    if (team && team.hasOwnProperty('teamRoleList')) {
      const { teamRoleList } = team;
      empNoIscancel = _.filter(teamRoleList, item => {
        return !_.contains(formData.assignList, item.empNo);
      });

      empNoIsActive = _.filter(teamRoleList, item => {
        return _.contains(formData.assignList, item.empNo);
      });
    }
    const newList = [];
    formData.assignList.forEach(item => {
      newList.push({
        empNo: item,
        isMain: 'N',
        isSendNotify: _.findWhere(empNoIsActive, { empNo: item }) ? empNoIsActive.length === 1 ? 'Y' : 'N' : 'Y',
        isActive: 'Y',
        isAddtask: _.findWhere(empNoIsActive, { empNo: item }) ? empNoIsActive.length === 1 ? 'Y' : 'N' : 'Y',
      });
    });

    empNoIscancel.forEach(item => {
      newList.push({
        empNo: item.empNo,
        isMain: 'N',
        isSendNotify: 'N',
        isActive: 'N',
        isAddtask: 'N',
      });
    });

    const dataTeamList = [{
      teamCode: team?.teamCode || '',
      teamName: '',
      jobTaskCode: jobTaskCode,
      deptCode: deptCode,
      jobNo: this.jobDetail.jobNo,
      teamRoleList: newList
    }];
    const payload = {
      jobTaskId: jobTaskId ? jobTaskId : 0,
      jobTaskName: formData.jobTaskName,
      note: formData.note,
      projectCode: this.jobDetail.projectCode,
      jobNo: this.jobDetail.jobNo,
      compCode: this.jobDetail.compCode,
      deptCode: deptCode,
      jobTaskCode: jobTaskCode,
      planStartDate: formData.planStartDate,
      planEndDate: formData.planEndDate,
      dueDate: formData.dueDate,
      priorityCode: formData.priorityCode,
      jobTaskStatusCode: formData.jobTaskStatusCode,
      teamList: newList.length === 0 ? [] : dataTeamList,
    };
    const taskList = this.setDataTaskList(formData, newList);
    this.save(payload, taskList);
  }
  save(payload: any, taskList: any[]): void {
    this.appJobService.saveJobTask(payload).then(res => {
      this.appTaskService.saveTaskList(taskList).then(r => {
        const { jobNo } = this.jobDetail;
        this.appJobService.getJobDetail(jobNo);
        this.alertService.info({ title: 'Task', message: 'New Jobs Assign saved.' }).then(res2 => {
          this.formDataSubmit.emit(null);
        });
      });
    });
  }

  saveTaskAssing(): void {
    const formData = this.jobTaskForm.getRawValue();
    const { task } = this.dataSource;
    if (task === null || !task) {
      this.newJobsTask();
      return;
    }
    this.updateJobsTask(task);
  }

  setAssignSourceList(): void {
    const assignList = this.jobTaskForm.get('assignList');
    if (this.jobDetail) {
      const positionList = new List<any>(this.userProfile.positionList).Where(a => a).Select(a => a.deptCode).ToList();
      if (positionList.Contains(this.dataSource.task.deptCode)) {
        this.employeeList = this.selfEmployeeList.map(item => {
          item.empName = item.firstName;
          return item;
        });
        this.editPermission = true;
      } else {
        this.employeeList = this.dataSource.task?.team?.teamRoleList || [];

        if (this.jobDetail.issueBy !== this.userProfile.empNo) {
          this.formHelperService.disableForm(this.jobTaskForm);
          this.editPermission = false;
        }
      }
    } else {
      assignList.disable();
    }

  }

  saveAcceptJobTask(formData: any): void {
    const data = {
      jobTaskCode: this.dataSource.task.jobTaskCode,
      compCode: this.jobDetail.compCode,
      projectCode: this.jobDetail.projectCode,
      jobNo: this.dataSource.task.jobNo,
      deptCode: this.dataSource.task.deptCode,
      jobTaskStatusCode: this.dataSource.task.jobTaskStatusCode,
      acceptStatus: 'Y',
      team: {
        teamCode: this.dataSource.task.team?.teamCode || '',
        teamName: '',
        teamRoleList: formData.assignList
      }
    };

    this.appJobService.saveAcceptJob(data).then(res => {
      this.appJobService.getJobDetail(this.dataSource.task.jobNo);
      this.alertService.info({ title: 'Task', message: 'New Jobs Assign saved.' }).then(res2 => {
        this.formDataSubmit.emit(null);
      });
    });
  }
  getPermission(): void {

  }
}
