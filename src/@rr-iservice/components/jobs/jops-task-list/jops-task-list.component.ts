import { Component, OnInit, Input } from '@angular/core';
import { AppJobsService } from '@rr-iservice/service/app/app-jobs.service';
import { log } from 'util';

@Component({
  selector: 'rr-jops-task-list',
  templateUrl: './jops-task-list.component.html',
  styleUrls: ['./jops-task-list.component.css']
})
export class JopsTaskListComponent implements OnInit {

  summaryTaskList: any;
  @Input() dataSource: any[];
  constructor(
    private appJobsService: AppJobsService) {
  
  }

  ngOnInit(): void {

  }
  calculate()
  {
    this.appJobsService.summaryTaskList.subscribe(res=>{
      this.summaryTaskList = res;
    })
  }

}
