import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppJobsService } from '@rr-iservice/service/app/app-jobs.service';
import { AlertService } from '@utilies/service/alert.service';
import { FormHelperService } from '@utilies/service/form-helper.service';

@Component({
    selector: 'rr-jobs-dialog-task-group',
    templateUrl: './jobs-dialog-task-group.component.html',
    styleUrls: ['./jobs-dialog-task-group.component.css']
})

export class JobsDialogTaskGroupComponent implements OnInit {
  taskGroupList: any[];
  
    // #region Task
  selectGroupForm: FormGroup;
  @Output() selectGroupFormDataSubmit: EventEmitter<any> = new EventEmitter();

  constructor(
    private appJobService: AppJobsService,
    public dialogRef: MatDialogRef<JobsDialogTaskGroupComponent>,
    private formBuilder: FormBuilder,
    private alertService: AlertService,
    private formHelperService: FormHelperService,
    @Inject(MAT_DIALOG_DATA) public data: any
    ) {
    this.appJobService.jobsGroupList.subscribe(res => {
      this.taskGroupList = res;
    });
  }
  setFormValidate(): void {
    this.selectGroupForm = this.formBuilder.group({
      taskGroupCode: ['', Validators.required]
    });
  }
  ngOnInit(): void{
    this.setFormValidate();
  }
  onClick(): void {
    if (this.selectGroupForm.invalid) {
      this.alertService.info({ title: 'Task', message: 'ตรวจสอบข้อมูลให้ครบถ้วนก่อนการบันทึก' });
      this.formHelperService.getValidationErrors(this.selectGroupForm);
      return;
    }
    const formData = this.selectGroupForm.getRawValue();
    this.dialogRef.close(formData);
  }
  onClose(): void {
    this.dialogRef.close({isCancel: true});
  }
}
