import { Component, OnInit, Inject, EventEmitter, Output, Input, ViewEncapsulation } from '@angular/core';
import { AppProjectService } from '@rr-iservice/service/app/app-project.service';
import { AppClientService } from '@rr-iservice/service/app/app-client.service';
import { AppJobsService } from '@rr-iservice/service/app/app-jobs.service';
import { AppEmployeeService } from '@rr-iservice/service/app/app-employee.service';
import { AppMasterTimeService } from '@rr-iservice/service/app/master/app-master-time.service';
import { AppMasterPriorityService } from '@rr-iservice/service/app/master/app-master-priority.service';
import { FormControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Observable, BehaviorSubject } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { DateTimePipe } from '@utilies/pipe/date-time.pipe';
import { Employee } from '@rr-iservice/interface/app/app-employee.interface';
import { AppTaskService } from '@rr-iservice/service/app/app-task.service';
import _ from 'underscore';
import { AppUserService } from '@rr-iservice/service/app/app-user.service';
import { DataService } from '@rr-iservice/service/data/data.service';
import { AppTimerService } from '@rr-iservice/service/app/app-timer.service';
import { List } from 'linqts';
import { AppMasterConfigService } from '@rr-iservice/service/app/master/app-master-config.service';
import * as moment from 'moment';

@Component({
  selector: 'app-task-from',
  templateUrl: './task-from.component.html',
  styleUrls: ['./task-from.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class TaskFromComponent implements OnInit {
  projects = [];
  projectList = [];
  clientList = [];
  jobsList = [];
  estimationTimeList = [];
  priorityList = [];
  jobsGroupList = [];
  taskStatusList = [];
  userProfile: any;
  people = [];
  selectAllEmp = [];
  selectedPeople = [];
  taskFrom: FormGroup;
  employeeList: Employee[];
  employeeAllList: Employee[];
  workerList: Employee[];
  filteredOptions: Observable<Employee[]>;
  githubUsers$: Observable<any[]>;
  selectedUsers = [];
  selectedJobProfile: any;
  countDate: Observable<number>;
  taskDetail: any;
  @Input() actionType: string;
  @Output() formGroup: EventEmitter<FormGroup> = new EventEmitter();
  dataFrom: any;
  constructor(
    private appProjectService: AppProjectService,
    private appClientService: AppClientService,
    private appJobsService: AppJobsService,
    private appTaskService: AppTaskService,
    private appEmployeeService: AppEmployeeService,
    private appMasterTimeService: AppMasterTimeService,
    private formBuilder: FormBuilder,
    private appUserService: AppUserService,
    private dataService: DataService,
    private appTimerService: AppTimerService,
    private appMasterConfigService: AppMasterConfigService
  ) {
    this.setFromValidate();
    this.getData();
    this.appTaskService.dataTaskFrom.subscribe(res => {
      this.taskDetail = res;
      this.setFromData(res);
    });
  }

  ngOnDestroy(): void {
    this.jobsList = [];
    this.projectList = [];
    this.clientList = [];
    this.workerList = [];
    this.appJobsService.jobsList.next([]);
  }
  ngOnInit(): void {
    this.setFromValidate();
    this.appUserService.userProfile.subscribe(res => {
      this.taskFrom.get('assignList').setValue([res.empNo]);
    });
    this.appTaskService.dataJobDetail.subscribe(res => {
      if (!res) {
        return;
      }
      this.taskFrom.get('compCode').setValue(res.compCode);
      this.taskFrom.get('projectCode').setValue(res.projectCode);
      this.taskFrom.get('jobNo').setValue(res.jobNo);
    });
    this.appTaskService.dataJobTask.subscribe(res => {
      if (!res) {
        return;
      }
      this.taskFrom.get('note').setValue(res.note);
      this.taskFrom.get('taskName').setValue(res.jobTaskName);
      this.taskFrom.get('planStartDate').setValue(res.planStartDate);
      this.taskFrom.get('planEndDate').setValue(res.planEndDate);
      this.taskFrom.get('dueDate').setValue(res.dueDate);
      this.taskFrom.get('priorityCode').setValue(res.priorityCode);
    });
  }
  setDataFrom(data: any): any {
    if (data !== null) {
      this.taskFrom.get('compCode').setValue(data.compCode);
      this.taskFrom.get('projectCode').setValue(data.projectCode);
      this.taskFrom.get('jobNo').setValue(data.jobNo);
      this.taskFrom.get('note').setValue(data.note);
      this.taskFrom.get('taskName').setValue(data.taskName);
      this.taskFrom.get('taskGroupCode').setValue(data.taskGroupCode);
      this.taskFrom.get('planStartDate').setValue(data.planStartDate);
      this.taskFrom.get('planEndDate').setValue(data.planEndDate);
      this.taskFrom.get('priorityCode').setValue(data.priorityCode);
      this.taskFrom.get('dueDate').setValue(data.dueDate);
      this.taskFrom.get('taskStatusCode').setValue(data.taskStatusCode);
      this.taskFrom.get('assignList').setValue(_.pluck(data.assignList, 'empNo'));
    }
    this.disableInput();
  }
  disableInput(): void {
    if (this.actionType === 'editTask') {
      this.taskFrom.get('taskStatusCode').enable();
      this.taskFrom.get('compCode').disable();
      this.taskFrom.get('projectCode').disable();
      this.taskFrom.get('jobNo').disable();
      this.taskFrom.get('taskName').disable();
      this.taskFrom.get('taskGroupCode').disable();
      this.taskFrom.get('planStartDate').disable();
      this.taskFrom.get('planEndDate').disable();
      this.taskFrom.get('priorityCode').disable();
      this.taskFrom.get('dueDate').disable();
      return;
    }
    this.taskFrom.get('taskStatusCode').disable();

  }
  selectAll(): void {
    this.selectedPeople = this.people.map(x => x.name);
  }

  unselectAll(): void {
    this.selectedPeople = [];
  }

  displayFn(employee: Employee): string {
    return employee && employee.firstName ? employee.firstName : '';
  }

  private _filter(firstName: string): Employee[] {
    const filterValue = firstName.toLowerCase();
    return this.employeeList.filter(option => option.firstName.toLowerCase().indexOf(filterValue) === 0);
  }
  setFromValidate(): void {
    this.taskFrom = this.formBuilder.group({
      taskCode: [''],
      compCode: ['', Validators.required],
      projectCode: ['', Validators.required],
      jobNo: ['', Validators.required],
      taskName: ['', [Validators.required, Validators.maxLength(100)]],
      taskGroupCode: ['', Validators.required],
      note: [''],
      planStartDate: ['', Validators.required],
      planEndDate: ['', Validators.required],
      dueDate: ['', Validators.required],
      priorityCode: ['', Validators.required],
      taskStatusCode: ['', Validators.required],
      estimated: ['', [Validators.required, Validators.pattern('[0-9]+([\.,][0-9]+)?')]],
      assignList: ['', Validators.required]
    });
    this.setSubscribe();
    this.setFromEvent();

    // this.taskFrom.get('taskStatusCode').disable();
    this.taskFrom.get('taskStatusCode').setValue('OP');
    this.taskFrom.get('priorityCode').setValue('MD');

    this.formGroup.emit(this.taskFrom);
  }
  setFromData(dataFrom: any): void {
    if (dataFrom) {
      this.taskFrom.get('taskCode').setValue(dataFrom.taskCode);
      this.taskFrom.get('compCode').setValue(dataFrom.compCode);
      this.taskFrom.get('projectCode').setValue(dataFrom.projectCode);
      this.taskFrom.get('jobNo').setValue(dataFrom.jobNo);
      this.taskFrom.get('taskName').setValue(dataFrom.taskName);
      this.taskFrom.get('taskGroupCode').setValue(dataFrom.taskGroupCode);
      this.taskFrom.get('note').setValue(dataFrom.note);
      this.taskFrom.get('planStartDate').setValue(dataFrom.planStartDate);
      this.taskFrom.get('planEndDate').setValue(dataFrom.planEndDate);
      this.taskFrom.get('dueDate').setValue(dataFrom.dueDate);
      this.taskFrom.get('priorityCode').setValue(dataFrom.priorityCode);
      this.taskFrom.get('taskStatusCode').setValue(dataFrom.taskStatusCode);
      this.taskFrom.get('estimated').setValue(dataFrom.estimated);

      const empNoList = new List<any>(dataFrom.assignList).Select(a => a.empNo).ToArray();
      this.workerList = this.setWorker(dataFrom);
      this.taskFrom.get('assignList').setValue(empNoList);
    }
  }
  setWorker(dataFrom: any): any[] {
    let data = [];
    data = this.employeeList.map(item => {
      return {
        ...item,
        isCanSelected: 'Y',
      };
    });
    if (!dataFrom) {
      return data;
    }
    const { assignList } = dataFrom;
    const workerCantSelected = _.filter(assignList, item => {
      return !_.contains(
        _.pluck(this.employeeList, 'empNo'), item.empNo
      );
    });
    const worker = workerCantSelected.map(item => {
      const employee = _.findWhere(this.employeeAllList, { empNo: item.empNo });
      if (employee) {
        return {
          ...employee,
          isCanSelected: 'Y',
        };
      }
    });
    data = data.concat(worker);    
    return _.filter(data, item => {
      return item;
    });
  }
  onSelectAll(): void{
    const selected = this.workerList.map(item => item.empNo);
    this.taskFrom.get('assignList').patchValue(selected);
  }

  setFromEvent(): void {
    this.taskFrom.get('projectCode').valueChanges.subscribe(res => {
      const fromData = this.taskFrom.getRawValue();
      const req = {
        compCodeList: [fromData.compCode],
        projectCodeList: [fromData.projectCode],
        jobStatusCodeList: [],

      };
      this.appJobsService.getJobsList(req);
    });

    this.taskFrom.get('compCode').valueChanges.subscribe(res => {
      const reqProject = {
        compCodeList: [res]
      };
      this.appProjectService.getProjectList(reqProject).then(res2 => {
        this.projectList = res2;
      });
    });

    this.taskFrom.get('planStartDate').valueChanges.subscribe(res => {
      this.setDefualtEstimation();
    });

    this.taskFrom.get('planEndDate').valueChanges.subscribe(res => {
      this.setDefualtEstimation();
      if (res > moment(this.taskFrom.get('dueDate').value)) {
        this.taskFrom.get('dueDate').setValue(res);
      }
    });

    this.taskFrom.get('jobNo').valueChanges.subscribe(res => {
      this.selectedJobProfile = _.findWhere(this.jobsList, { jobNo: res });
      if (!this.selectedJobProfile || !this.userProfile) {
        return;
      }
      const { trJobTask } = this.selectedJobProfile;
      const { positionList } = this.userProfile;

      const jobtask = trJobTask.map(item => {
        if (_.contains(_.pluck(positionList, 'deptCode'), item.deptCode)) {
          return item;
        }
      });
      const jobtaskFirst = _.first(jobtask);
      if (jobtaskFirst) {
        this.setDataByJobsItem(jobtaskFirst);
      }
    });

    // this.taskFrom.get('dueDate').valueChanges.subscribe(res => {
    //   if(res) {
    //   this.taskFrom.get('planEndDate').setValue(res);
    //   }
    // });

    // this.taskFrom.get('planEndDate').valueChanges.subscribe(res => {
    //   if(res) {
    // this.taskFrom.get('dueDate').setValue(res);
    //   }
    // });

  }
  setDataByJobsItem(jobtaskFist): void {
    const { planStartDate, planEndDate, dueDate } = jobtaskFist;
    this.taskFrom.get('planStartDate').setValue(planStartDate !== null ? planStartDate : Date.now());
    this.taskFrom.get('planEndDate').setValue(planEndDate !== null ? planEndDate : Date.now());
    this.taskFrom.get('dueDate').setValue(dueDate !== null ? dueDate : Date.now());
  }
  setDefualtEstimation(): void {
    if (this.taskDetail) {
      const { estimated } = this.taskDetail;
      this.taskFrom.get('estimated').setValue(estimated);
      return;
    }

    const estimation = this.appTimerService.getWorkEstimation(
      this.taskFrom.get('planStartDate').value,
      this.taskFrom.get('planEndDate').value
    );
    if (Number.isNaN(estimation)) {
      this.taskFrom.get('estimated').setValue(0);
    } else {
      this.taskFrom.get('estimated').setValue(estimation);
    }

  }
  setSubscribe(): void {
    this.dataService.getPeople().subscribe(items => {
      this.people = items;
    });
    this.appUserService.userProfile.subscribe(res => {
      this.userProfile = res;
    });
    this.appClientService.clientList.subscribe(res => {
      this.clientList = res;
    });
    this.appJobsService.jobsList.subscribe(res => {
      this.jobsList = res;
    });
    this.appJobsService.jobsGroupList.subscribe(res => {
      this.jobsGroupList = res;
    });
    this.appEmployeeService.employeeList.subscribe(res => {
      this.employeeList = res;
    });
    this.appEmployeeService.employeeAllList.subscribe(res => {
      this.employeeAllList = res;
      this.workerList = this.setWorker(this.taskDetail);
    });
    this.appMasterTimeService.estimationTimeList.subscribe(res => {
      this.estimationTimeList = res;
    });

  }
  getData(): void {
    this.githubUsers$ = this.dataService.getGithubAccounts();
    this.appClientService.getClientList({});
    this.appJobsService.getTaskGroupList({});
    this.appEmployeeService.getEmployeeList({});
    this.appEmployeeService.getEmployeeAllList({});
    this.appMasterTimeService.getEstimationTimeList({});

    this.appMasterConfigService.getTaskStatusList().then(res => {
      this.taskStatusList = res;
    });
    this.appMasterConfigService.getPriorityList().then(res => {
      this.priorityList = res;
    });
  }

}
