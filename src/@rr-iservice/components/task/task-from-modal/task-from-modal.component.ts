import { Component, OnInit, Inject, ViewEncapsulation, OnDestroy } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormGroup } from '@angular/forms';
import { AlertService } from '@utilies/service/alert.service';
import { FormHelperService } from '@utilies/service/form-helper.service';
import { AppTaskService } from '@rr-iservice/service/app/app-task.service';
import { AppTimerService } from '@rr-iservice/service/app/app-timer.service';
import { List } from 'linqts';
import { AppUserService } from '@rr-iservice/service/app/app-user.service';

@Component({
  selector: 'app-task-from-modal',
  templateUrl: './task-from-modal.component.html',
  styleUrls: ['./task-from-modal.component.css']
})
export class TaskFromModalComponent implements OnInit {
  loading: boolean;
  projects = [];
  projectList = [];
  taskForm: FormGroup;
  dataFrom: any;
  actionType: string;
  jobsDetail: any;
  userProfile: any;
  constructor(
    private alertService: AlertService,
    private formHelperService: FormHelperService,
    private appTaskService: AppTaskService,
    private appTimerService: AppTimerService,
    private appUserService: AppUserService,
    public dialogRef: MatDialogRef<TaskFromModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {
    this.loading = false;
    if (data !== null && data) {
      this.actionType = data?.actionType;
      this.appTaskService.dataJobDetail.next(data.jobsDetail);
      this.appTaskService.dataJobTask.next(data?.task);
    }

    this.appUserService.userProfile.subscribe(res => {
      this.userProfile = res;
    });
  }

  ngOnInit(): void {
    this.setDataFrom(this.data);
  }
  titleText(): string {
    if (this.actionType === 'editAction') {
      return 'Edit Task';
    }
    return 'New Task';
  }

  setDataFrom(data: any): void {
    const dataReq = {
      taskCode: data.taskCode
    };
    this.appTaskService.getTaskDetail(dataReq).then(res => {
      if (res === null) {
        return;
      }
      this.dataFrom = res;
    });
  }
  submit(): void {
    this.loading = true;
    if (this.taskForm.invalid) {
      this.loading = false;
      this.alertService.info({ title: 'Task', message: 'Please check for complete form' });
      return;
    }
    const formData = this.taskForm.getRawValue();
    if (!this.dataFrom) {
      formData.assignList = formData.assignList.map(item => {
        return {
          empNo: item,
          isActive: 'Y',
          isSendNotify: this.userProfile.empNo !== item ? 'Y' : 'N'
        };
      });
    } else {
      const activeList = new List<any>(formData.assignList);
      const newList = [];
      formData.assignList.forEach(item => {
        newList.push({
          empNo: item,
          isActive: 'Y',
          isSendNotify: activeList.Contains(item) && this.userProfile.empNo !== item ? 'Y' : 'N'
        });
      });

      this.dataFrom.assignList.forEach(element => {
        if (!activeList.Contains(element.empNo)) {
          newList.push({
            empNo: element.empNo,
            isActive: 'N',
            isSendNotify: 'N'
          });
        }
      });

      formData.assignList = newList;
    }

    this.appTaskService.saveTask(formData).then(res => {
      this.dialogRef.close(formData);
      this.alertService.info({ title: 'Task', message: 'บันทึกข้อมูลเรียบร้อย' });
      this.loading = false;
      if (this.actionType !== 'formJobs') {
        this.appTimerService.refreshRecords();
      }
    });
  }
  trackTaskForm(event): void {
    this.taskForm = event;
  }

}
