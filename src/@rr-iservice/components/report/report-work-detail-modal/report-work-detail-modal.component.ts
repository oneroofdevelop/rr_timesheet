import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-report-work-detail-modal',
  templateUrl: './report-work-detail-modal.component.html',
  styleUrls: ['./report-work-detail-modal.component.scss']
})
export class ReportWorkDetailModalComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<ReportWorkDetailModalComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit() {
    console.log('ReportWorkDetailModalComponent',this.data);


  }

}
