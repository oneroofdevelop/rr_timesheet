import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'rr-report-work-form',
  templateUrl: './report-work-form.component.html',
  styleUrls: ['./report-work-form.component.scss']
})
export class ReportWorkFormComponent implements OnInit {

  @Input() data : any;

  dataSource : any;
  constructor() { }

  ngOnInit() {
    this.dataSource = this.data.map(item=>{
      const minutes = item.workTime % 60;
      const hours = (item.workTime - minutes) / 60;
      const total = hours + ':' + minutes;
      return {
        ...item,
        workTimeHot:total,
      };
    });
  }

}
