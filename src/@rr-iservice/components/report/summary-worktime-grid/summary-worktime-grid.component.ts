import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import * as moment from 'moment';

@Component({
  selector: 'rr-summary-worktime-grid',
  templateUrl: './summary-worktime-grid.component.html',
  styleUrls: ['./summary-worktime-grid.component.scss']
})
export class SummaryWorktimeGridComponent implements OnInit {
  isPopoverVisible: boolean;
  dataSource: any[];
  collapsed = false;
  dateList = [];
  columns = [];
  summaryTimeGroupList = [];
  userProfile: any;
  @Input() start: any;
  @Input() end: any;
  @Output() saveChangeTimeManual: EventEmitter<boolean> = new EventEmitter(false);
  constructor() { }

  ngOnInit() {
  }

  contentReady = (e) => {
    if (!this.collapsed) {
      this.collapsed = true;
      e.component.expandRow(['EnviroCare']);
    }
  }

  convertDuration(time: any): string {
    if (time != null) {
      return `${parseInt((time / 60).toString(), 10)}:${(time % 60).toString().padStart(2, '0')}`;
    } else {
      return '0:00';
    }

  }

  // isClick(cell: any): boolean {
  //   if (this.isMatchEmpNo(cell) === true && this.isOverDateNew(cell) === true) {
  //     return true;
  //   }
  //   return false;
  // }

  isToday(date: any): boolean {
    return moment(date).format('L') === moment().format('L');
  }

  getDuration(summaryTimeList: any[], cell: any): string {
    // const data = new List(summaryTimeList).Where(a => moment(a.Date).format('L') === moment(cell.column.dataField).format('L')).FirstOrDefault();
    // if (data != null) {
    //   return `${parseInt((data.workTime / 60).toString(), 10)}:${(data.workTime % 60).toString().padStart(2, '0')}`;
    // } else {
    //   return '0:00';
    // }
    return '';
  }

}
