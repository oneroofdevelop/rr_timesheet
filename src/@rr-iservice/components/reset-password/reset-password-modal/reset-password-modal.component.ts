import { Component, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AlertService } from '@utilies/service/alert.service';
import { AppUserService } from '@rr-iservice/service/app/app-user.service';

@Component({
    selector: 'app-reset-password-modal',
    templateUrl: './reset-password-modal.commonent.html',
    styleUrls: ['./reset-password-modal.component.css']
  })
  
  export class ResetPasswordModalComponent implements OnInit {
    resetPasswordForm: FormGroup;
    constructor(
        private formBuilder: FormBuilder,
        private alertService: AlertService,
        private appUserService: AppUserService,
        public dialogRef: MatDialogRef<ResetPasswordModalComponent>,
        ) {
            
    }
    
    ngOnInit(): void {
        this.setForm();
    }
    setForm(): void {
        this.resetPasswordForm = this.formBuilder.group({
            old_password: ['', Validators.required],
            new_password: ['', Validators.required],
            confirm_new_password: ['', Validators.required]
            
          });        
    }
    submit(): void {

        if (this.resetPasswordForm.invalid) {
            this.alertService.info({ title: 'Reset Password', message: 'Please check for complete form' });
            return;
        }

        if (this.resetPasswordForm.get('new_password').value !== this.resetPasswordForm.get('confirm_new_password').value){
            this.alertService.info({ title: 'Reset Password', message: 'รหัสผ่านใหม่ไม่ตรงกันกรุณาลองใหม่อีกครั้ง' });
            return;
        }      
        const payload = {
            currentPassword: this.resetPasswordForm.get('old_password').value,
            newPassword: this.resetPasswordForm.get('new_password').value
        };
        this.appUserService.resetPassword(payload).then(res => {
            if (res !== 'error') {
                this.dialogRef.close(true);
                this.alertService.info({ title: 'Reset Password', message: 'บันทึกข้อมูลเรียบร้อย' });
            }
        });

    }

  }