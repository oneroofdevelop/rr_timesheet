import { Component, OnInit } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { AppNotiService } from '@rr-iservice/service/app/app-noti.service';
import { AppUserService } from '@rr-iservice/service/app/app-user.service';
import { List } from 'linqts';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { HubService } from '@rr-iservice/service/app/hub.service';

@Component({
  selector: 'rr-noti',
  templateUrl: './noti.component.html',
  styleUrls: ['./noti.component.css']
})
export class NotiComponent implements OnInit {
  public data: any[];

  notiList: any[];
  isPopoverVisible: boolean;
  userProfile: any;
  readNotiList: any[];

  constructor(
    private appNotiService: AppNotiService,
    private appUserService: AppUserService,
    private router: Router,
    private hubService: HubService
  ) {
    this.hubService.start();
    this.runSignalR();
    this.isPopoverVisible = false;
    this.readNotiList = [];
    this.appUserService.userProfile.subscribe(res1 => {

      if (res1) {
        this.userProfile = res1;
        this.appNotiService.getNotiList();
      }

    });
    this.appNotiService.notiList.subscribe(res => {
      this.notiList = res;
    });
  }

  ngOnInit(): void {
    if (!this.userProfile) {
      return;
    }


  }
  getUnReadCount(): number {
    return new List<any>(this.notiList).Where(a => a.readStatus === 'N').Count();
  }
  readAndLink(data: any): void {
    if (data.url) {
      this.router.navigate([data.url]);
    }
    if (data.readStatus === 'Y') {
      return;
    }
    this.appNotiService.readNotify({
      notifyMsgCode: data.notifyMsgCode
    });

    this.isPopoverVisible = false;
  }

  readNotifyList(): void {
    //filter ข้อมูลเฉพาะที่ยังไม่ได้อ่าน
    var dataRead = this.notiList.filter(a => {
      return a.readStatus == "N"
    });

    this.readNotiList = dataRead.map(b => {
      return {notifyMsgCode : b.notifyMsgCode}
    });
    console.log('ListReadNoti',this.readNotiList);

    this.appNotiService.readNotifyList({
      notifyMsgCodeList : this.readNotiList
    });
  }

  runSignalR(): void {
    this.hubService.hubConnection.on('onNotify', () => {
      this.appNotiService.getNotiList();
    });
  }
}
