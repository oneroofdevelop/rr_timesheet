import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AppJobsService } from '@rr-iservice/service/app/app-jobs.service';
import { AppNotiService } from '@rr-iservice/service/app/app-noti.service';

@Component({
  selector: 'app-noti-grid',
  templateUrl: './noti-grid.component.html',
  styleUrls: ['./noti-grid.component.scss']
})
export class NotiGridComponent implements OnInit {
  [x: string]: any;
  dataSource: any[];
  collapsed = false; 
  dateList = [];
  notiList: any[];
  selectStatus : any[];
  status : any;
  dataAll: any[];
  readNotiList : any[];
  constructor(
      private appJobsService: AppJobsService,
      private appNotiService: AppNotiService,
      private router: Router,
    ) { 
    this.dataSource = [];
    this.dataAll = [];
    this.readNotiList = [];
    this.selectStatus = [{value:"",desc:"ทั้งหมด"},{value:"N",desc:"ยังไม่ได้อ่าน"},{value:"Y",desc:"อ่านแล้ว"}]
    this.appNotiService.notiList.subscribe(res => {
      res = res.map(a => {
        return {...a,readStatusDesc : (a.readStatus == "Y" ? "อ่านแล้ว" : "ยังไม่อ่าน")}
      });
      this.status = "";
      this.dataSource = res;
      this.dataAll = res;
      console.log('DataListNoti',res)
    });
  }

  ngOnInit() {
  }
  readAndLink(data: any): void {
    if (data.url) {
      this.router.navigate([data.url]);
    }
    if (data.readStatus === 'Y') {
      return;
    }
    this.appNotiService.readNotify({
      notifyMsgCode: data.notifyMsgCode
    });

    this.isPopoverVisible = false;
  }
  
  filterStatus():void{
    var status = (this.status !== "") ? this.status : ""
    if(this.status != "") {
      this.dataSource = this.dataAll.filter(a => {
        return a.readStatus == this.status
      });
    }else {
      this.dataSource = this.dataAll
    }
    console.log('Status : ',status);
    console.log('Data : ',this.dataSource);
  }

  readNotifyList(): void {
    //filter ข้อมูลเฉพาะที่ยังไม่ได้อ่าน
    var dataRead = this.dataAll.filter(a => {
      return a.readStatus == "N"
    });

    this.readNotiList = dataRead.map(b => {
      return {notifyMsgCode : b.notifyMsgCode}
    });
    console.log('ListReadNoti',this.readNotiList);

    this.appNotiService.readNotifyList({
      notifyMsgCodeList : this.readNotiList
    });
  }

  isAssign(data: any): boolean {
    return data.isAssigned === 'Y';
  }

  runSignalR(): void {
    this.hubService.hubConnection.on('onNotify', () => {
      this.appNotiService.getNotiList();
    });
  }
}
