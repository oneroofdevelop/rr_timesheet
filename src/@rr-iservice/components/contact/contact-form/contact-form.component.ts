import { IContactPerson } from './../../../interface/api/api-contact.interface';
import { ContactFormModalComponent } from './../contact-form-modal/contact-form-modal.component';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';
import { AppClientService } from '@rr-iservice/service/app/app-client.service';
import { AppMasterService } from '@rr-iservice/service/app/master/api-master.service';
import { AlertService } from '@utilies/service/alert.service';
import { ISaveContactRequest } from '@rr-iservice/interface/api/api-contact.interface';
import { AppMasterConfigService } from '@rr-iservice/service/app/master/app-master-config.service';

@Component({
  selector: 'app-contact-form',
  templateUrl: './contact-form.component.html',
  styleUrls: ['./contact-form.component.css']
})
export class ContactFormComponent implements OnInit {

  contactForm: FormGroup;
  customerList: any;
  prenameList: any;
  @Input() data: any;
  @Input() formGroup: any;
  @Output() formData: EventEmitter<any> = new EventEmitter();
  constructor(
    public dialog: MatDialog,
    private appClientService: AppClientService,
    private appMasterConfigService: AppMasterConfigService,
    public dialogRef: MatDialogRef<ContactFormModalComponent>,
    private formBuilder: FormBuilder
  ) {
    this.appMasterConfigService.getPreNameList().then(res => this.prenameList = res);
  }

  ngOnInit(): void {
    this.setFormValidate();

    this.getDataDefult();
  }
  getDataDefult(): void {
    this.appClientService.clientList.subscribe(res => {
      this.customerList = res;
    });
  }

  setFormValidate(): void {
    this.contactForm = this.formBuilder.group(
      {
        compCode: [this.data.compCode, Validators.required],
        preNameCode: ['PRE01', Validators.required],
        firstName: ['', [Validators.required, Validators.maxLength(100)]],
        lastName: ['', [Validators.required, Validators.maxLength(100)]],
        mobile: ['', [Validators.required, Validators.maxLength(10)]],
        email: ['', [Validators.email, Validators.maxLength(250)]],
        jobPosition: ['', Validators.maxLength(250) ]

      });
    this.formData.emit(this.contactForm);
  }

  setFormData(): void {
    this.contactForm.get('compCode').setValue(this.formGroup.compCode);
    this.contactForm.get('prefixName').setValue(this.formGroup.preNameCode);
    this.contactForm.get('fistName').setValue(this.formGroup.firstName);
    this.contactForm.get('lastName').setValue(this.formGroup.lastName);
    this.contactForm.get('mobile').setValue(this.formGroup.mobile);
    this.contactForm.get('email').setValue(this.formGroup.email);
    this.contactForm.get('jobPosition').setValue(this.formGroup.jobPosition);
  }
}
