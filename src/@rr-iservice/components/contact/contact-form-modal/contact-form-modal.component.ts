import { AlertService } from './../../../../@utilies/service/alert.service';
import { AppMasterService } from './../../../service/app/master/api-master.service';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Component, OnInit, Input, Inject } from '@angular/core';
import { AppClientService } from '@rr-iservice/service/app/app-client.service';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { IContactPerson, ISaveContactRequest } from '@rr-iservice/interface/api/api-contact.interface';

@Component({
  selector: 'app-contact-form-modal',
  templateUrl: './contact-form-modal.component.html',
  styleUrls: ['./contact-form-modal.component.css']
})
export class ContactFormModalComponent implements OnInit {
  contactForm: FormGroup;
  // dataForm: any;
  constructor(
    public dialogRef: MatDialogRef<ContactFormModalComponent>,
    private alertService: AlertService,
    private appClientService: AppClientService,

    // ถ้าต้องการรับค่าที่ส่งาให้ modal จะต้องรับค่าแบบนี้
    @Inject(MAT_DIALOG_DATA) public data: any
  ) {

  }

  ngOnInit(): void {
    // this.contactForm = new FormGroup();
  }
  onSaveContact(): void {
    if (this.contactForm.invalid) {
      this.alertService.info({ title: 'Form New Contact', message: 'Please complete all information.' });
      return;
    }
    const formData = this.contactForm.getRawValue();

    const dataContact = {
      firstName: formData.firstName,
      lastName: formData.lastName,
      email: formData.email,
      isActive: 'Y',
      mobile: formData.mobile,
      preNameCode: formData.preNameCode,
      preNameName: '',
      citizenId: '',
      lineId: '',
      passport: '',
      personCode: '',
      personId: 0,
      telephone: '',
      contact: {
        compCode: formData.compCode,
        compName: '',
        compPersonId: 0,
        deptCode: '',
        deptName: '',
        contactTypeCode: '',
        contactTypeName: '',
        email: formData.email,
        isActive: 'Y',
        isMain: '',
        jobPositionCode: formData.jobPosition,
        jobPositionName: formData.jobPosition,
        mobile: formData.mobile
      } as IContactPerson
    } as ISaveContactRequest;

    this.appClientService.saveContact(dataContact).then((res) => {
      if (res !== 'error') {
        this.alertService.info({ title: 'Contact', message: 'บันทึกข้อมูลเรียบร้อย' });

        // เมื่อบันทึกข้อมูลเสร็จและ success จะส่งค่า boolean true กลับไปยัง ปุ่มเพื่อไปกระทำบางอย่างต่อ ตามตัวอย่าง code นี้
        // <app-contact-form [data]="data" (formData)="onContactFormSubmit($event)"></app-contact-form>
        this.dialogRef.close(true);
      }
    });
  }

  onContactFormSubmit(event): void {
    // รับค่าจาก form ที่ส่งมาให้ทุก event ของ form
    this.contactForm = event;
    // this.dialogRef.close(event);
  }
}
