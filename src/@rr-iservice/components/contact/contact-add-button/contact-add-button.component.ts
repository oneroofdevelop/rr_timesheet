import { Component, OnInit, Input, OnChanges, SimpleChanges, EventEmitter, Output } from '@angular/core';
import { ContactFormModalComponent } from '../contact-form-modal/contact-form-modal.component';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-contact-add-button',
  templateUrl: './contact-add-button.component.html',
  styleUrls: ['./contact-add-button.component.scss']
})
export class ContactAddButtonComponent implements OnInit, OnChanges {

  // input รับค่ามาจาก การเจาะท่อไว้ [compCode] ใน element ที่ไปแปะใน componant อื่น
  @Input() compCode: any; 

  // output เพื่อส่งค่า boolean กลับไปยัง element ที่ไปแปะใน componant อื่น
  @Output() saveChange: EventEmitter<boolean> = new EventEmitter(false); 
  constructor(public dialog: MatDialog) { }

  ngOnInit(): void {

  }
  ngOnChanges(changes: SimpleChanges): void {
  }

  // สั่งเปิด modal
  openDialog(): void {
    const dialogRef = this.dialog.open(ContactFormModalComponent, {
      width: '500px',

      // ส่งค่าไปให้ component ที่เป็น Modal และใน modal ก็จะต้องเปิดรับ data นี้ด้วย
      data: { titleModal: 'Add new contact', compCode: this.compCode }
    });

    dialogRef.afterClosed().subscribe(result => {
      // ส่งค่ากลับไปยัง element ที่ไปแปะใน componant อื่น เพื่อให้กระทำอะไรบางอย่างหลังจาก ปิด modal ซึง result = true || false
      this.saveChange.emit(result);
    });
  }
}
