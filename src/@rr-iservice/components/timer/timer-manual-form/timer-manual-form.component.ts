import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'rr-timer-manual-form',
  templateUrl: './timer-manual-form.component.html',
  styleUrls: ['./timer-manual-form.component.css']
})
export class TimerManualFormComponent implements OnInit {
  timeManualForm: FormGroup;
  @Input() data: any;
  @Output() formData: EventEmitter<any> = new EventEmitter();
  constructor(
    private formBuilder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.setFormValidate();
  }

  setFormValidate(): void {
    const dataModal = this.data.data;
    this.timeManualForm = this.formBuilder.group(
      {
        compCode: [dataModal.compCode, Validators.required],
        projectCode: [dataModal.projectName, Validators.required],
        jobNo: [dataModal.jobNo, Validators.required],
        taskCode: [dataModal.taskName, Validators.required],
        hours: [0, Validators.required],
        minutes: [0, Validators.required],
      });
    this.formData.emit(this.timeManualForm);
  }
  checkMinutes(event): any {
    // this.value=this.value.replace(/[^0-9]/g,'');
  }
}
