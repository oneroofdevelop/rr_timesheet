import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AppTimerService } from '@rr-iservice/service/app/app-timer.service';


@Component({
    selector: 'rr-timer-history-modal',
    templateUrl: './timer-history-modal.component.html',
    styleUrls: ['./timer-history-modal.component.css']
})
export class TimerHistoryModalComponent implements OnInit {
    constructor(
        private appTimerService: AppTimerService,
        @Inject(MAT_DIALOG_DATA) public data: any
    ) {
    }
    ngOnInit(): void {
        const req = {
            taskCode: this.data.taskCode.toString(),
        };
        this.appTimerService.getTimeSheetHistoryByTaskCode(req);
    }
    onSaveTimerManual(): void {
    }
}