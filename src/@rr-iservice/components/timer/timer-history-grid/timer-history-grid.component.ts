import { Component, OnInit, OnChanges, Input } from '@angular/core';
import { log } from 'util';
import { AppTimerService } from '@rr-iservice/service/app/app-timer.service';
import { threadId } from 'worker_threads';
import { MatDialogRef } from '@angular/material/dialog';
import { TimerManualModalComponent } from '../timer-manual-modal/timer-manual-modal.component';
import { AlertService } from '@utilies/service/alert.service';
import { DialogService } from '@utilies/service/dialog.service';

@Component({
    selector: 'rr-timer-history-grid',
    templateUrl: './timer-history-grid.component.html',
    styleUrls: ['./timer-history-grid.component.css']
})
export class TimerHistoryGridComponent implements OnInit {
    collapsed = false;
    dateList = [];
    dataSource: any[];
    constructor(
        private appTimerService: AppTimerService,
        public dialogRef: MatDialogRef<TimerManualModalComponent>,
        private alertService: AlertService,
        private dialogService: DialogService,
    ) {
    }
    ngOnInit(): void {
        this.gettimeSheetHistory();
    }
    delete(data): void {
        const timeSheetActive = 'C';
        this.dialogRef.close(true);
        this.dialogService.confirm({
            dialogTitile: 'ยืนยันการลบเวลาทำงาน',
            btnConfirmText: '',
            btnCancelText: ''
        }).then(res => {
            if (res) {
                this.appTimerService.saveTiming(data.timeSheetId, data.workTime, data.startDate, data.taskCode, timeSheetActive).then(res => {
                    if (res !== 'error') {
                        const req = {
                            taskCode: data.taskCode.toString(),
                        };
                        this.alertService.info({ title: 'Time manual', message: 'บันทึกข้อมูลเรียบร้อย' });
                        this.dialogRef.close(true);
                        this.appTimerService.getTimeSheetHistoryByTaskCode(req);
                        this.gettimeSheetHistory();
                    }
                });
            }
        });
    }

    gettimeSheetHistory(): void {
        this.appTimerService.timeSheetHistory.subscribe(res => {
            if (res) {
                const resultActive = res.filter((dataSource) => {
                    return dataSource.timeSheetActiveStatusCode !== 'C' && dataSource.timeSheetActiveStatusCode !== 'A';
                });
                this.dataSource = resultActive.map(item => {
                    const minutes = item.workTime % 60;
                    const hours = (item.workTime - minutes) / 60;
                    const total = hours + ':' + minutes;
                    return {
                        ...item,
                        workTimeHot: total,
                    };
                });

            }
        });
    }

}