import { Component, OnInit, Input, OnChanges, SimpleChanges, Output, EventEmitter, ViewEncapsulation } from '@angular/core';
import * as moment from 'moment';
import { AppTimerService } from '@rr-iservice/service/app/app-timer.service';
import { List } from 'linqts';
import { TaskFromModalComponent } from '@rr-iservice/components/task/task-from-modal/task-from-modal.component';
import { MatDialog } from '@angular/material/dialog';
import { TimerManualModalComponent } from '../timer-manual-modal/timer-manual-modal.component';
import { AppUserService } from '@rr-iservice/service/app/app-user.service';
import { TimerHistoryModalComponent } from '../timer-history-modal/timer-history-modal.component';
import _ from 'underscore';

import { from } from 'rxjs';
moment.fn.toJSON = function (): any {
  return this.format();
};
let gbSumTimeGroupList = [];
let gbDateList = [];
@Component({
  selector: 'rr-timer-grid',
  templateUrl: './timer-grid.component.html',
  styleUrls: ['./timer-grid.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TimerGridComponent implements OnInit, OnChanges {
  isPopoverVisible: boolean;
  dataSource: any[];
  lookupIsJobsNo: any[];
  treeDataSource: any[];
  treeBoxValue: any[];
  selectItem: any[];
  collapsed = false;
  dateList = [];
  columns = [];
  summaryTimeGroupList = [];
  userProfile: any;
  chooserTitle: string;
  @Input() start: any;
  @Input() end: any;
  @Output() saveChangeTimeManual: EventEmitter<boolean> = new EventEmitter(false);
  constructor(
    public dialog: MatDialog,
    private appTimerService: AppTimerService,
    private appUserService: AppUserService

  ) {
    this.isPopoverVisible = false;
    this.dataSource = [];
    this.treeBoxValue = [];
    this.lookupIsJobsNo =  ['ใช้งาน', 'ไม่ใช้งาน'];
    this.treeDataSource = [{ID: 1, name: 'Employee', value: 'userFullName'}, {ID: 2, name: 'Client', value: 'compCode'}, {ID: 3, name: 'Job No', value: 'jobNo'}, {ID: 4, name: 'Task', value: 'taskName'}];
    this.chooserTitle = 'คอลัมน์';
    this.appTimerService.records.subscribe(res => {
      this.dataSource = res.filter(item => { 
        if (item.taskStatusCode === 'CL' && item.totalDuration > 0) {
          return item;
        }
        return item.taskStatusCode !== 'CL';
      });

      this.summaryTimeGroupList = [];
      new List(this.dataSource)
        .Select(a => a.summaryTimeList)
        .ForEach(a => {
          a.forEach(element => {
            this.summaryTimeGroupList.push(element);
          });
        });
      gbSumTimeGroupList = this.summaryTimeGroupList;
    });

  }
  ngOnChanges(changes: SimpleChanges): void {
    this.createColumn();
    this.dataSource = [];

  }
  onValueChangedSelection(e): void {
    console.log('onValueChangedSelection(e):', e);
    this.selectItem = e.value;
    // var component = (e && e.component) || (this.treeView && this.treeView.instance);
  }
  treeView_itemSelectionChanged(e): void {
    console.log('treeView_itemSelectionChanged(e):', e);
    this.treeBoxValue = e.component.getSelectedNodeKeys();
  }
  orderHeaderFilter(): any[] {
    return [];
  }
  ngOnInit(): void {
    this.appUserService.userProfile.subscribe(res => {
      this.userProfile = res;
    });

  }
  isDispay(str: string): boolean {
    return _.contains(this.selectItem, str);
  }
  contentReady = (e) => {
    if (!this.collapsed) {
      this.collapsed = true;
      e.component.expandRow(['EnviroCare']);
    }
  }

  customizeTooltip = (pointsInfo) => {
    return { text: parseInt(pointsInfo.originalValue, 32) + '%' };
  }
  onClickDetail(data: any): void {
    const dataRow = {
      ...data,
      actionType: 'editAction',
    };
    const dialogRef = this.dialog.open(TaskFromModalComponent, {
      width: '800px',
      data: dataRow,
    });

    dialogRef.afterClosed().subscribe(() => {
    });
  }

  createColumn(): void {
    this.columns = ['UserFullName', 'compCode', 'jobNo', 'ProjectName'];
    if (!this.start || !this.end) {
      return;
    }
    if (moment(this.start).isAfter(moment(this.end))) {
      return;
    }
    this.dateList = [];
    const dateFirst = this.start.clone();
    const dateLast = this.end.clone();
    this.addDate(dateFirst);
    while (dateFirst.add(1, 'days').diff(dateLast) < 0) {
      this.addDate(dateFirst);
    }
    this.addDate(dateLast);

    gbDateList = this.dateList;
    this.columns.push('Total');
  }

  addDate(date: any): void {
    const isExist = new List(this.dateList).Where(a => moment(a.value).format('L') === moment(date).format('L')).Any();
    if (!isExist) {
      this.columns.push(moment(date.clone().toDate()).format('dd-DD'));

      this.dateList.push({
        name: moment(date.clone().toDate()).format('dd-DD'),
        value: date.clone().toDate()
      });
    }
  }

  getDuration(summaryTimeList: any[], cell: any): string {
    const data = new List(summaryTimeList).Where(a => moment(a.Date).format('L') === moment(cell.column.dataField).format('L')).FirstOrDefault();
    if (data != null) {
      return `${parseInt((data.workTime / 60).toString(), 10)}:${(data.workTime % 60).toString().padStart(2, '0')}`;
    } else {
      return '0:00';
    }

  }

  convertDuration(time: any): string {

    if (time != null) {
      return `${parseInt((time / 60).toString(), 10)}:${(time % 60).toString().padStart(2, '0')}`;
    } else {
      return '0:00';
    }

  }
  totalInRange(summaryTimeList: any[]): number {
    return new List(summaryTimeList).Sum(a => a.workTime);
  }

  calculateSummary(options: any): any {
    if (options.summaryProcess === 'finalize') {
      const date = new List(gbDateList).Where(a => a.name === options.name).Select(a => a.value).FirstOrDefault();
      let total = 0;
      new List(gbSumTimeGroupList)
        .Where(a => moment(a.Date).format('L') === moment(date).format('L'))
        .ForEach(a => {
          total += a.workTime;
        });

      // options.totalValue = (total / 60).toFixed(2);
      options.totalValue = `${parseInt((total / 60).toString(), 10)}:${(total % 60).toString().padStart(2, '0')}`;
      console.log('calculateSummary(options: any)',total, options);
    }
  }
  testClick(data: any): void {
  }
  openWorkTimeManual(cell: any): void {
    if (!this.isClick(cell)) {
      return;
    }
    const dialogRef = this.dialog.open(TimerManualModalComponent, {
      width: '400px',
      data: cell,
    });

    dialogRef.afterClosed().subscribe(result => {
      // alert(result);
      this.saveChangeTimeManual.emit();
    });

  }
  // afterSaveChangeTimeManual(event): void {
  //   if (event) {
  //     alert('test');
  //   }
  // }
  clickHistoryTime(data: any): void {
    console.log('clickHistoryTime(data: any): void {', data);
    const dialogRef = this.dialog.open(TimerHistoryModalComponent, {
      width: '700px',
      data: data,
    });

    dialogRef.afterClosed().subscribe(result => {
      // alert(result);
    });    
  }
  isClick(cell: any): boolean {
    if (this.isMatchEmpNo(cell) === true && this.isOverDateNew(cell) === true) {
      return true;
    }
    return false;
  }
  isToday(date: any): boolean {
    return moment(date).format('L') === moment().format('L');
  }
  isMatchEmpNo(cell: any): boolean {
    return this.userProfile.empNo === cell.data.empNo;
  }
  isOverDateNew(cell: any): boolean {
    return new Date(cell.column.dataField) <= new Date(Date.parse(Date()));
  }
}
