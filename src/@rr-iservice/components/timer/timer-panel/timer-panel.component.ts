import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AppTimerService } from '@rr-iservice/service/app/app-timer.service';
import * as moment from 'moment';


import { BehaviorSubject } from 'rxjs';
import { AppUserService } from '@rr-iservice/service/app/app-user.service';
moment.updateLocale('en', {
  relativeTime: {
    future: 'in %s',
    past: '%s',
    s: (num, withoutSuffix, key, isFuture) => {
      return '00:' + (num < 10 ? '0' : '') + num + ' minutes';
    },
    m: '01:00 minutes',
    mm: (num, withoutSuffix, key, isFuture) => {
      return (num < 10 ? '0' : '') + num + ':00' + ' minutes';
    },
    h: 'an hour',
    hh: '%d',
    d: 'a day',
    dd: '%d days',
    M: 'a month',
    MM: '%d months',
    y: 'a year',
    yy: '%d years'
  }
});

@Component({
  selector: 'rr-timer-panel',
  templateUrl: './timer-panel.component.html',
  styleUrls: ['./timer-panel.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TimerPanelComponent implements OnInit {
  isPopoverVisible = false;
  timing: any;
  timingDuration: BehaviorSubject<string>;

  buttonConfirmConfig = { btnText: 'close', isIcon: true };
  constructor(
    private appTimerService: AppTimerService,
    private appUserService: AppUserService,
  ) {
    this.timingDuration = new BehaviorSubject('');

    this.appUserService.userProfile.subscribe(res => {
      const req = {
        startDate: moment().startOf('week').toDate(),
        endDate: moment().endOf('week').toDate(),
        empNoList: [],
        projectCodeList: []
      };
      localStorage.setItem('rrTimesheetReq', JSON.stringify(req));
      this.appTimerService.getTimingInfo();
    });


  }

  ngOnInit(): void {
    this.appTimerService.getTimingInfo();
    this.appTimerService.timing.subscribe(res => {
      this.timing = res;
      this.getDuration(this.timing?.startDate);
    });
    this.fetchDuration();
  }

  fetchDuration(): any {
    const vm = this;
    setInterval(() => {
      vm.getDuration(this.timing?.startDate);
    }, 1000);
  }

  getDuration(startAt): void {
    const now = moment();
    const expiration = moment(startAt);
    const diff = now.diff(expiration);
    const diffDuration = moment.duration(diff);
    const h = diffDuration.hours().toString().padStart(2, '0');
    const m = diffDuration.minutes().toString().padStart(2, '0');

    const result = `${h}<span class="blink">:</span>${m}`;
    this.timingDuration.next(result);
  }

  saveTiming(): void {
    this.isPopoverVisible = false;
    this.appTimerService.saveTiming(
      this.timing.timeSheetId,
      this.appTimerService.getWorkTime(this.timing.startDate),
      this.timing.startDate,
      this.timing.taskCode,
      'S'
    )
      .then(res => this.appTimerService.refreshRecords());
  }
  confirmDicard(event): void {
    if (event) {
      this.isPopoverVisible = false;
      this.appTimerService.saveTiming(
        this.timing.timeSheetId,
        0,
        this.timing.startDate,
        this.timing.taskCode,
        'C'
      ).then(res => this.appTimerService.refreshRecords());
    }
  }
}
