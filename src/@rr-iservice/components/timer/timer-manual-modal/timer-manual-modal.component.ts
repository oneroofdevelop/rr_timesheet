import { AppTimerService } from '@rr-iservice/service/app/app-timer.service';
import { AlertService } from './../../../../@utilies/service/alert.service';
import { FormGroup } from '@angular/forms';
import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ISaveTimeSheet } from '@rr-iservice/interface/api/api-timer.interface';
import { IDialogConfig } from '@utilies/interface/dialog.interface';
import { DialogConfirmComponent } from '@utilies/component/dialog/dialog-confirm/dialog-confirm.component';
import { MatDialog } from '@angular/material/dialog';
import { DialogService } from '@utilies/service/dialog.service';
import * as moment from 'moment';

@Component({
  selector: 'rr-timer-manual-modal',
  templateUrl: './timer-manual-modal.component.html',
  styleUrls: ['./timer-manual-modal.component.css']
})
export class TimerManualModalComponent implements OnInit {
  timeManualForm: FormGroup;

  constructor(
    public dialogRef: MatDialogRef<TimerManualModalComponent>,
    private dialogService: DialogService,
    private alertService: AlertService,
    private appTimerService: AppTimerService,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  ngOnInit(): void {
  }
  onClose(event: any): void {
    this.dialogRef.close(event);
  }


  onSaveTimerManual(): void {
    const formData = this.timeManualForm.getRawValue();
    const data = {
      endDate: moment(this.data.column.dataField),
      startDate: moment(this.data.column.dataField),
      taskCode: this.data.data.taskCode,
      timeSheetId: 0,
      timeSheetActive: 'M',
      // tslint:disable-next-line: radix
      workTime: (parseInt(formData.hours) * 60) + parseInt(formData.minutes)
    };

    if (this.timeManualForm.invalid) {
      this.alertService.info({ title: 'Timesheet', message: 'ตรวจสอบข้อมูลให้ครบถ้วนก่อนการบันทึก' });
      return;
    }
    this.dialogRef.close(true);
    const calModeMinutes = Number(formData.minutes) % 60;
    const calModeHours = Number(formData.minutes) / 60;
    const hours = Number(formData.hours) + calModeHours;
    this.dialogService.confirm({
      dialogTitile: 'ยืนยันการบันทึกเวลาทำงาน',
      dialogContent: this.getMassage(hours, calModeMinutes),
      btnConfirmText: '',
      btnCancelText: ''
    }).then(res => {
      if (res) {
        this.appTimerService.saveTimeManual(data).then((res) => {
          if (res !== 'error') {
            this.alertService.info({ title: 'Time manual', message: 'บันทึกข้อมูลเรียบร้อย' });
            this.dialogRef.close(true);
          }
        });
      }

    });
  }
  getMassage(hours: any, calModeMinutes: any): string {
    if (Math.floor(calModeMinutes) === 0) {
      return `${Math.floor(hours) ? Math.floor(hours) + ' ชั่วโมง' : ''}`;
    } else {
      return `${Math.floor(hours) ? Math.floor(hours) + ' ชั่วโมง' : ''} ${Math.floor(calModeMinutes)} นาที`;
    }
  }
  onTimeManualSubmit(event): void {
    this.timeManualForm = event;
  }
}
