import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { AppTimerService } from '@rr-iservice/service/app/app-timer.service';
import { AppUserService } from '@rr-iservice/service/app/app-user.service';

@Component({
  selector: 'rr-timer-button',
  templateUrl: './timer-button.component.html',
  styleUrls: ['./timer-button.component.css']
})
export class TimerButtonComponent implements OnInit {
  timing: any;
  userProfile: any;
  @Input() task: any;
  constructor(
    private appTimerService: AppTimerService,
    private appUserService: AppUserService
  ) {

  }

  ngOnInit(): void {
    this.appUserService.userProfile.subscribe(res => {
      this.userProfile = res;
    });
    this.appTimerService.timing.subscribe(res => {
      this.timing = res;
    });
  }
  onTiming(): void {
    if (this.task?.taskCode === this.timing?.taskCode) {
      this.saveTiming();
    } else {
      this.createTiming();
    }
  }
  saveTiming(): void {
    this.appTimerService.saveTiming(
      this.timing.timeSheetId,
      this.appTimerService.getWorkTime(this.timing.startDate),
      this.timing.startDate,
      this.timing.taskCode,
      'S'
    ).then(res => {
      this.appTimerService.refreshRecords();
    });
  }
  createTiming(): void {
    this.appTimerService.createTiming(this.task.taskCode).then(res => {
      this.appTimerService.refreshRecords();
    });
  }
}
