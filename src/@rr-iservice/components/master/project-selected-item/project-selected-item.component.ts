import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AppClientService } from '@rr-iservice/service/app/app-client.service';
import { AppProjectService } from '@rr-iservice/service/app/app-project.service';
import { List } from 'linqts';

@Component({
  selector: 'rr-project-selected-item',
  templateUrl: './project-selected-item.component.html',
  styleUrls: ['./project-selected-item.component.css']
})
export class ProjectSelectedItemComponent implements OnInit, OnChanges {
  projects = new FormControl();
  sourceList = [];
  projectList = [];
  @Input() customer: any;
  @Output() projectChange: EventEmitter<any> = new EventEmitter();
  constructor(
    private appClientService: AppClientService,
    private appProjectService: AppProjectService
  ) {
    this.appProjectService.getProjectList({
      compCodeList: []
    }).then(res => {
      this.sourceList = res;
      this.projectList = res;
    });
    this.appClientService.clientSelected.subscribe(res => {
    });
    this.appProjectService.projectList.subscribe(res => {
      this.projectList = res;
    });
    this.projects.valueChanges.subscribe(res => {
      this.projectChange.emit(res);
    });
  }
  ngOnChanges(changes: SimpleChanges): void {
    this.filterByCustomer();
  }

  ngOnInit(): void {
    this.filterByCustomer();

  }
  filterByCustomer(): void {
    this.projectList = new List<any>(this.sourceList).Where(a => a.compCode === this.customer).ToArray();
  }
}
