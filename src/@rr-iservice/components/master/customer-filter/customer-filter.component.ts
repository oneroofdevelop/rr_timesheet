import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AppClientService } from '@rr-iservice/service/app/app-client.service';

@Component({
  selector: 'rr-customer-filter',
  templateUrl: './customer-filter.component.html',
  styleUrls: ['./customer-filter.component.css']
})
export class CustomerFilterComponent implements OnInit {
  customers = new FormControl();
  customerList = [];
  @Output() customerChange: EventEmitter<any> = new EventEmitter();
  constructor(
    private appClientService: AppClientService
  ) {
    this.appClientService.getClientList(null);
    this.appClientService.clientList.subscribe(res => {
      this.customerList = res;
    });
    this.customers.valueChanges.subscribe(res => {
      this.customerChange.emit(res);
    });
  }

  ngOnInit(): void {

  }

}
