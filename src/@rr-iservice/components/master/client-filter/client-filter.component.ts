import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import { List } from 'linqts';
import { AppClientService } from '@rr-iservice/service/app/app-client.service';
import { AppProjectService } from '@rr-iservice/service/app/app-project.service';
import _ from 'underscore'
@Component({
  selector: 'rr-client-filter',
  templateUrl: './client-filter.component.html',
  styleUrls: ['./client-filter.component.css']
})
export class ClientFilterComponent implements OnInit {
  clientList = [];
  clients = new FormControl();
  @Output() clientChange: EventEmitter<any> = new EventEmitter();
  constructor(
    private appClientService: AppClientService,
    private appProjectService: AppProjectService
  ) {

    this.clients.valueChanges.subscribe(res => {
      this.appProjectService.getProjectList({
        compCodeList: []
      });
      this.clientChange.emit(res);
    });
    this.appClientService.clientList.subscribe(res => {
      this.clientList = res;
    });
    this.appClientService.getClientList({}).then(res => {
      this.setValueFromSession();
    });
  }

  ngOnInit(): void {

  }

  setValueFromSession(): void {
    const req = localStorage.getItem('rrTimesheetReq');
    if (!req) {
      return;
    }
    const data = JSON.parse(req);

    if (data.compCodeList?.length > 0) {
      const codes = new List<string>(data.compCodeList);
      const datas = new List<any>(this.clientList).Where(a => codes.Contains(a.compCode)).ToArray();

      this.clients.setValue(datas);
    }
  }
}
