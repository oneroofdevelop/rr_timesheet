import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormControl } from '@angular/forms';
import { AppClientService } from '@rr-iservice/service/app/app-client.service';
import { AppProjectService } from '@rr-iservice/service/app/app-project.service';
import _ from 'underscore'

@Component({
  selector: 'rr-client-selected-item',
  templateUrl: './client-selected-item.component.html',
  styleUrls: ['./client-selected-item.component.css']
})
export class CustomerSelectedItemComponent implements OnInit {
  customers = new FormControl();
  customerList = [];
  clients = new FormControl();
  @Input() value: any;
  @Output() clientChange: EventEmitter<any> = new EventEmitter();

  constructor(
    private appClientService: AppClientService,
    private appProjectService: AppProjectService,
  ) {

    this.clients.valueChanges.subscribe(res => {
      this.clientChange.emit(res);
      this.appClientService.clientSelected.next(res);
    });

    this.appClientService.clientList.subscribe(res => {
      this.customerList = res;
    });

  }

  ngOnInit(): void {
    this.appClientService.getClientList(null);

    this.clients.setValue(this.value);
  }

}
