import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AppEmployeeService } from '@rr-iservice/service/app/app-employee.service';
import { FormControl } from '@angular/forms';
import { List } from 'linqts';

@Component({
  selector: 'rr-employee-filter',
  templateUrl: './employee-filter.component.html',
  styleUrls: ['./employee-filter.component.css']
})
export class EmployeeFilterComponent implements OnInit {
  employee = new FormControl([]);
  employeeList = [];
  @Output() employeeChange: EventEmitter<any> = new EventEmitter();
  constructor(
    private appEmployeeService: AppEmployeeService
  ) {
    this.employee.valueChanges.subscribe(res => {
      this.employeeChange.emit(res);

    });
    this.appEmployeeService.employeeList.subscribe(res => {
      this.employeeList = res.map((value) => {
        value.fullName = value.title + value.firstName + ' ' + value.lastName;
        return value;
      });

      this.setValueFromSession();
    });
  }

  ngOnInit(): void {
    this.appEmployeeService.getEmployeeList(null);
  }
  setValueFromSession(): void {
    const req = localStorage.getItem('rrTimesheetReq');
    if (!req) {
      return;
    }
    const data = JSON.parse(req);

    if (data.projectCodeList?.length > 0) {
      const codes = new List<string>(data.empNoList);
      const datas = new List<any>(this.employeeList).Where(a => codes.Contains(a.empNo)).ToArray();

      this.employee.setValue(datas);
    }
  }
}
