import { Component, OnInit, Output, EventEmitter, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AppProjectService } from '@rr-iservice/service/app/app-project.service';
import { FormControl } from '@angular/forms';
import { List } from 'linqts';
import _ from 'underscore'
@Component({
  selector: 'rr-project-filter',
  templateUrl: './project-filter.component.html',
  styleUrls: ['./project-filter.component.css']
})
export class ProjectFilterComponent implements OnInit, OnChanges {
  sourceList = [];
  projectList = [];
  projects = new FormControl();
  @Input() customers: any[];
  @Output() projectsChange: EventEmitter<any> = new EventEmitter();
  constructor(
    private appProjectService: AppProjectService
  ) {
    this.projects.valueChanges.subscribe(res => {
      this.projectsChange.emit(res);
    });
    this.appProjectService.projectList.subscribe(res => {
      this.sourceList = res;
      this.projectList = res;
    });
    this.appProjectService.getProjectList({
      compCodeList: []
    }).then(res => {
      if (this.customers?.length > 0) {
        this.filterByCustomer();
      } else {
        this.clearList();
      }
    });
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.customers?.length > 0) {
      this.filterByCustomer();
    } else {
      this.clearList();
    }
  }

  ngOnInit(): void {

  }
  filterByCustomer(): void {

    const codes = new List<any>(this.customers).Select(a => a.compCode);
    this.projectList = new List<any>(this.sourceList).Where(a => codes.Contains(a.compCode)).ToArray();
  }
  clearList(): void {
    this.projectList = [];
    this.projects.setValue([]);
  }
}
