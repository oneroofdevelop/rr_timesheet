export interface IApiAcceptJobTaskRequest {
    jobTaskCode: string,
    compCode: string,
    projectCode: string,
    jobNo: string,
    deptCode: string,
    acceptStatus: string,
    team : IApiAcceptJobTaskRequest_Team
}

export interface IApiAcceptJobTaskRequest_Team {
    teamCode: string,
    teamName: string,
    teamRoleList: Array<IApiAcceptJobTaskRequest_Team_TeamList>
}

export interface IApiAcceptJobTaskRequest_Team_TeamList {
    empNo: string,
    isMain: string,
    isActive: string
}