//#region login
export interface IApiLoginRequest {
    userName: string;
    passWord: string;
    userGroupId: number;
}
export interface IApiLoginResponse {
    userId: number;
    userName: string;
    userGroupId: number;
    userTypeId: number;
    empNo: string;
    email: string;
    isActive: string;
    tokenDateTime: number;
}
//#endregion
//#region menu
export class IApiGetMenuResponse {
    id: number;
    parentId: number;
    name: string;
    icon: string;
    path: string;
    isUse: string;
    sort: number;
    children: Array<IApiGetMenuResponse>;
}
//#endregion

//#region getUserProfile
export class IApiGetUserProfileRequest{

}
export class IApiGetUserProfileResponse{
    name: string;
    avatar: string;
    remark: string;
    userGroupName: string;
    userTypeName: string;
    userId: string;
    userName: string;
    userGroupId: string;
    userTypeId: string;
    empNo: string;
    email: string;
    isActive: string;
    tokenDateTime: string;
}
//#endregion