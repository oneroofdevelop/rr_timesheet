export interface IPerson {
    personId: number;
    personCode: string;
    preNameCode: string;
    preNameName: string;
    firstName: string;
    lastName: string;
    email: string;
    mobile: string;
    telephone: string;
    citizenId: string;
    passport: string;
    isActive: string;
    lineId: string;
}

export interface IContactPerson {
    compPersonId: number;
    compCode: string;
    compName: string;
    jobPositionCode: string;
    jobPositionName: string;
    contactTypeCode: string;
    contactTypeName: string;
    deptCode: string;
    deptName: string;
    email: string;
    mobile: string;
    isMain: string;
    isActive: string;
}

export interface IGetContactListResponse {
    personId: number;
    personCode: string;
    preNameCode: string;
    preNameName: string;
    firstName: string;
    lastName: string;
    email: string;
    mobile: string;
    telephone: string;
    citizenId: string;
    passport: string;
    isActive: string;
    lineId: string;
    contact: Array<IContactPerson>;
}

export interface ISaveContactRequest {
    personId: number;
    personCode: string;
    preNameCode: string;
    preNameName: string;
    firstName: string;
    lastName: string;
    email: string;
    mobile: string;
    telephone: string;
    citizenId: string;
    passport: string;
    isActive: string;
    lineId: string;
    contact: IContactPerson;
}

