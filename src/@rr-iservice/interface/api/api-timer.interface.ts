export interface IGetDataTimeSheet {
    startDate: Date;
    endDate: Date;
    userIdList: number[];
    projectCodeList: number[];
}

export interface ISaveTimeSheet {
    timeSheetId: number;
    taskCode: string;
    startDate: any;
    endDate: any;
    workTime: number;
    timeSheetActive: string;
}
