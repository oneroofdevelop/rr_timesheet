export interface IAppAcceptJobTaskRequest {
    jobTaskCode: string,
    compCode: string,
    projectCode: string,
    jobNo: string,
    deptCode: string,
    acceptStatus: string,
    team : IAppAcceptJobTaskRequest_Team
}

export interface IAppAcceptJobTaskRequest_Team {
    teamCode: string,
    teamName: string,
    teamRoleList: Array<IAppAcceptJobTaskRequest_Team_TeamList>
}

export interface IAppAcceptJobTaskRequest_Team_TeamList {
    empNo: string,
    isMain: string,
    isActive: string
}

  