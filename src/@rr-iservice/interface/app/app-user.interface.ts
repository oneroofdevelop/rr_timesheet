//#region login
export interface IAppLoginRequest {
    userName: string;
    passWord: string;
    userGroupId: number;
}
export interface IAppLoginResponse {
    userId: number;
    userName: string;
    userGroupId: number;
    userTypeId: number;
    empNo: string;
    email: string;
    isActive: string;
    tokenDateTime: number;
}
//#endregion
//#region menu
export class IAppGetMenuResponse {
    id: number;
    parentId: number;
    name: string;
    icon: string;
    path: string;
    isUse: string;
    sort: number;
    children: Array<IAppGetMenuResponse>;
}
//#endregion

//#region getUserProfile
export class IAppGetUserProfileRequest {

}
export class IAppGetUserProfileResponse {
    name: string;
    avatar: string;
    remark: string;
    userGroupName: string;
    userTypeName: string;
    userId: string;
    userName: string;
    userGroupId: string;
    userTypeId: string;
    empNo: string;
    email: string;
    isActive: string;
    tokenDateTime: string;
}
// #endregion
