import { Data } from '@angular/router';

export interface Employee {
    name: string;
    deptCode: string;
    empId: number;
    empNo: string;
    firstName: string;
    lastName: string;
    isActive: string;
    posnCode: string;
    startDate: Data;
    endDate: Date;
    title: string; 
}
