export interface IAppGetNotiListResponse {
    notifyMsgCode: string,
    notifyHeader: string,
    notifyDesc: string,
    imgUrl: string,
    url: string,
    createDate: string,
    readStatus: string,
}